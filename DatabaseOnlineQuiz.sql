create database OnlineQuiz
GO
use OnlineQuiz
GO

create table [Role](
	[RoleId] int primary key,
	[Role] nvarchar(20),
)

create table [User](
	Account nvarchar(50) primary key,
	Email varchar(100) not null,
	[Password] nvarchar(100) not null,
	[RoleId] int REFERENCES [Role](RoleId),
	IsActive bit default 1
)

create table Student(
	Account nvarchar(50) REFERENCES [User](Account) ON DELETE CASCADE,
	MSSV varchar(20) not null,
	StudentName nvarchar(50) not null,
	DOB date,
	Phone varchar(10),
	Major nvarchar(40),
	Primary Key(Account)
)

create table Lecturer(
	Account nvarchar(50) REFERENCES [User](Account) ON DELETE CASCADE,
	LecturerName nvarchar(50) not null,
	Phone varchar(10),
	Primary Key(Account)
)

create table Manager(
	Account nvarchar(50) REFERENCES [User](Account) ON DELETE CASCADE,
	ManagerName nvarchar(50) not null,
	Phone varchar(10),
	Primary Key(Account)
)

create table [Admin](
	Account nvarchar(50) REFERENCES [User](Account) ON DELETE CASCADE,
	AdminName nvarchar(50) not null,
	Phone varchar(10),
	Primary Key(Account)
)

create table [Course](
	CourseId int identity primary key,
	CourseCode varchar(10),
	CourseName nvarchar(100),
	[Description] nvarchar(max)
)

create table Class(
	ClassId int identity primary key,
	ClassName nvarchar(100),
	CourseId int REFERENCES [Course](CourseId) ON DELETE CASCADE,
	LecturerAccount nvarchar(50),
	CoWorker nvarchar(50),
	StartDate Datetime,
	EndDate Datetime,
	
  CONSTRAINT fk_lecturer FOREIGN KEY (LecturerAccount) REFERENCES Lecturer(Account),

  CONSTRAINT fk_coworker FOREIGN KEY (CoWorker) REFERENCES Lecturer(Account)
)

create table Exam(
	ExamId int identity primary key,
	ClassId int REFERENCES [Class](ClassId) ON DELETE CASCADE,
	LecturerAccount nvarchar(50),
	Title nvarchar(50) not null,
	Summary nvarchar(100) not null,
	Score decimal(5,2),
	StartDate Datetime,
	EndDate Datetime,
	Timer int,
	TakingTimes int,
	Permission bit
)

create table QuestionInBank(
	QuesId int identity primary key,
	Content nvarchar(max),
	[Type] bit,
	CourseId int REFERENCES [Course](CourseId) ON DELETE CASCADE
)

create table QuestionExam(
	ExamId int REFERENCES [Exam](ExamId) ON DELETE CASCADE,
	QuesId int identity primary key,
	Content nvarchar(max),
	[Type] bit,
	Score decimal(5,2) default 1
)

create table QuestionExamAnswer(
	AnswerId int identity primary key,
	QuesId int REFERENCES [QuestionExam](QuesId) ON DELETE CASCADE,
	Content nvarchar(max),
	Correct bit,
	[Percent] decimal(5,2),
)

create table QuestionBankAnswer(
	AnswerId int identity primary key,
	QuesId int REFERENCES [QuestionInBank](QuesId) ON DELETE CASCADE,
	Content nvarchar(max),
	Correct bit,
	[Percent] decimal(5,2),
)

create table TakeClass(
	id int identity primary key,
	StudentAccount nvarchar(50) REFERENCES [Student](Account) ON DELETE CASCADE,
	ClassId int REFERENCES [Class](ClassId) ON DELETE CASCADE
)

create table TakeExam(
	TakeExamId int identity primary key,
	StudentAccount nvarchar(50) REFERENCES [Student](Account) ON DELETE CASCADE,
	ExamId int REFERENCES [Exam](ExamId) ON DELETE CASCADE,
	[Status] nvarchar(20),
	Score decimal(5,2),
	StartDate Datetime,
	EndDate Datetime,
)

create table TakeAnswer(
	TakeAnswerId int identity primary key,
	TakeExamId int REFERENCES [TakeExam](TakeExamId) ON DELETE CASCADE,
	QuesId int REFERENCES [QuestionExam](QuesId),
	AnswerId int REFERENCES [QuestionExamAnswer](AnswerId)
)

create table [Notification](
	Account nvarchar(50) REFERENCES [User](Account) ON DELETE CASCADE,
	NotiId int identity primary key,
	Title nvarchar(max),
	Content nvarchar(max),
	SendDate Datetime
)

INSERT INTO [dbo].[Role] ([RoleId], [Role]) VALUES (1,'Admin')
INSERT INTO [dbo].[Role] ([RoleId], [Role]) VALUES (2,'Manager')
INSERT INTO [dbo].[Role] ([RoleId], [Role]) VALUES (3,'Lecturer')
INSERT INTO [dbo].[Role] ([RoleId], [Role]) VALUES (4,'Student')

INSERT INTO [dbo].[User] ([Account], [Email], [Password], [RoleId], [IsActive]) VALUES ('lamvthe160434','lamvthe160434@fpt.edu.vn','$2a$11$0Xc.yqIZNSTEO/oiPM1OVOlTLpIpHgwfVb0IrVx1lUM7sKkOa6kdu',4,1) -- mat khau van nhu cu nhe, t ma hoa mat khau thoi nha ae
INSERT INTO [dbo].[Student] ([Account],[MSSV],[StudentName],[DOB],[Phone],[Major]) VALUES ('lamvthe160434','HE160434','Vuong Tung Lam', '2002-6-14','0394122468','Software Engineering')

INSERT INTO [dbo].[User] ([Account], [Email], [Password], [RoleId], [IsActive]) VALUES ('longdhhe160008','longdhhe160008@fpt.edu.vn','$2a$11$VustiaJM5noLRyM0TIjNeuxZy52IHAiNZcBRWw/0g2MEPTo3Wvu1i',2,1)
INSERT INTO [dbo].[Manager] ([Account],[ManagerName],[Phone]) VALUES ('longdhhe160008','Do Hai Long', '0394122468')

INSERT INTO [dbo].[User] ([Account], [Email], [Password], [RoleId], [IsActive]) VALUES ('duydnhe160373','duydnhe160373@fpt.edu.vn','$2a$11$0362ARrMfcaYnjo6QcC7E.HJ5WZ/m7TdeTiVB83/QTanPvCZmpypK',4,1)
INSERT INTO [dbo].[Student] ([Account],[MSSV],[StudentName],[DOB],[Phone],[Major]) VALUES ('duydnhe160373','HE160373','Do Ngoc Duy', '2002-10-21','0394122468','Software Engineering')

INSERT INTO [dbo].[User] ([Account], [Email], [Password], [RoleId], [IsActive]) VALUES ('anhnlvhe163350','anhnlvhe163350@fpt.edu.vn','$2a$11$xyZMcP4oXGUWL6R8OiYZY.WJb6ETJb0hgPkLfGD4FBOGP53bytHOK',1,1)
INSERT INTO [dbo].[Admin] ([Account],[AdminName],[Phone]) VALUES ('anhnlvhe163350','Nguyen Le Viet Anh','0394122468')

INSERT INTO [dbo].[User] ([Account], [Email], [Password], [RoleId], [IsActive]) VALUES ('longnphe161027','longnphe161027@fpt.edu.vn','123',3,1)
INSERT INTO [dbo].[Lecturer] ([Account],LecturerName,[Phone]) VALUES ('longnphe161027','Nguyen Phi Long','0123456789')

INSERT INTO [dbo].[Course] (CourseCode, CourseName, [Description]) values
('SSL101c', 'Academic Skills for University Success', 'Access, search, filter, manage and organize Information by using a variety of digital tools, from wide variety of source for use in academic study'),
('WED201c', 'Web Design', 'The concepts of HTML, CSS3, JavaScript, Interactivity with JavaScript, Advanced Styling with Responsive Design'),
('PRF192', 'Programming Fundamentals', 'Understand the basic concepts of programming, focus on procedure programming, testing and debugging, unit testing'),
('MAS291', 'Statistics and Probability', 'The fundamental principles of probability and their applications'),
('MAE101', 'Mathematics for Engineering', 'The basic concepts of single variable calculus: limit, derivative, integral.'),
('JPD123', 'Elementary Japanese 1-A1.2', 'The course provides basic knowledge and skills of Japanese at elementary level 1 for students studying Japanese as a second foreign language at the University'),
('CEA201', 'Computer Organization and Architecture', 'This course in an introduction to computer architecture and organization'),
('IOT102', 'Internet of Things', 'The content includes basic concepts and applications of IoT, practical exercises on the learning KIT'),
('CSI104', 'Introduction to Computer Science', 'This course provides an overview of computer Fundamentals'),
('PRJ301', 'Java Web Application Development', 'Basic Web application development applying MVC Design Pattern using Servlet/Filter as Controller'),
('PRO192', 'Object-Oriented Programming', 'This subject introduces the student to object-oriented programming'),
('SWE201c', 'Introduction to Software Engineering', 'SWE201c is for people who are new to software engineering but  wish to gain a deeper understanding of the underlying context and theory of software development practices'),
('MAD101', 'Discrete mathematics', 'Concepts of logical expressions & predicate logic'),
('SWP391', 'Application development project', 'This course focuses on designing, developing, and integrating the basic Web-based system/application using Java Web or .NET technologies'),
('OSG202', 'Operating Systems', 'Background knowledge: The role of operating system, important OS concepts, the mechanism of operating system, and main problems of Operating system'),
('ITE302c', 'Ethics in IT', 'Organizations and governments are seeking out ethics professionals to minimize risk and guide their decision-making about the design of inclusive, responsible, and trusted technology'),
('NWC203c', 'Computer Networking', 'This specialization is developed for seniors and fresh graduate students to understand fundamental network architecture concepts and their impacts on cyber security'),
('SWR302', 'Software Requirement', 'This course is a model-based introduction to RE, providing the conceptual background and terminology on RE, addressing a variety of techniques for requirements development including Analysis and Requirements Elicitation; Requirements Evaluation; Requirements Specification and Documentation; Requirements Quality Assurance'),
('SSG104', 'Communication and In-Group Working Skills', 'This course covers both working in groups and communication skills'),
('SWT301', 'Software Testing', 'General concepts about software testing'),
('JPD113', 'Elementary Japanese 1-A1.1', 'The course provides basic knowledge and skills of Japanese at elementary level 1 for students studying Japanese as a second foreign language at the University'),
('PRN211', 'Basic Cross-Platform Application Programming With .NET', 'Understand the followings: C# language for developing .NET applications; Fundamental concepts of .NET Platform; Basic knowledge of Window Forms in .NET; Basic knowledge of ASP.NET Core MVC; Basic knowledge of RESTful API .NET'),
('CSD201', 'Data Structures and Algorithms', 'Understanding the connection between data structures and their algorithms, including an analysis of algorithms complexity'),
('OJT202', 'On the job training', 'Students get acquainted with the real working environment before completing the study program'),
('DBI202', 'Introduction to Databases', 'Knowledge about database systems has become an essential part of an education in computer science because database management has evolved from a specialized computer application to a central component of a modern computing environment'),
('ENW492c', 'Writing Research Papers', 'The skills taught in this Specialization will empower you to succeed in any college-level course or professional field'),
('LAB211', 'OOP with Java Lab', 'This course focuses on basic problems related to Java programming skills'),
('PRN221', 'Advanced Cross-Platform Application Programming With .NET', 'Understand the followings: Apply C# language for developing Desktop and Web applications; Fundamental concepts of .NET Core Platform; Basic knowledge of Windows Presentation Foundation (WPF) and ASP.NET Core Razor Page application'),
('EXE101','Experiential Entrepreneurship 1', 'This course will provide students with essential knowledge and tips on starting a start-up efficiently and effectively'),
('WDU203c','UI/UX Design', 'Integrate UX Research and UX Design to create great products through understanding user needs, rapidly generating prototypes, and evaluating design concepts'),
('PRM392','Mobile Programming', 'Upon completion of this course students should: understand basic knowledge of mobile programming; get some experienced with all common controls of Android'),
('PRN231','Building Cross-Platform Back-End Application With .NET', 'Understand the followings: Apply C# language for develop ASP.NET WEB API ( RESTful Service applications ); Fundamental concepts of .NET Core Platform; Basic knowledge of ASP.NET WEB API on .NET Core'),
('PRU211m','C# Programming and Unity', 'This course based on the C# Programming for Unity Game Development Specialization at coursera.org/specializations/ programming-unity-game-development'),
('MLN111','Philosophy of Marxism � Leninism', 'Philosophy of Marxism � Leninism studies dialectical materialistic views on nature, society, and the mind, making the worldview of dialectical materialism becomes comprehensive'),
('SWD392','SW Architecture and Design', 'This is a course in concepts and methods for the architectural design of software systems of sufficient size and complexity to require the effort of several people for many months'),
('MLN131','Scientific socialism', 'Scientific socialism is one of the three parts of Marxism-Leninism. Scientific socialism is based on the philosophical methodology of dialectical materialism and historical materialism as well as scientific theoretical foundations of economic laws and economic relations to scientifically explain the advent of scientific socialism socialist revolution, the formation and development of the communist socio-economic form, associated with the historical mission of the working class, to liberate people and society'),
('EXE201','Experiential Entrepreneurship 2', 'In Experiental Entrepreneurship 2, groups of students will develop product/service for their start-up ideas prepared from Experiental Entrepreneurship 1 and deploy sales and find real customers for their group products/services'),
('VNR202','History of Viet Nam Communist Party', 'History of CPV is a major and a division of historical science. President Ho Chi Minh affirmed that the history of the Communist Party of Vietnam is golden pages'),
('MLN122','Political economics of Marxism � Leninism', 'Political economics of Marxism � Leninism is an economic theory and scientific discipline on political economy developed by C. Marx, Engels and later Lenin in a new period, focusing on the capitalist mode of production and the production and economic exchange relations consistent with the capitalist mode of production, thereby clarifying the nature and phenomena of economic processes, laying a foundation to solve matters related to theories of Marxism - Leninism. The core of the Marxist-Leninist political economy is the surplus value theory of Marx'),
('SEP490','SE Capstone Project', 'With the team size of 4-5 members, the output product size are required to be equivalent to 20-25 medium use cases'),
('PMG202c','Project management', 'Project managers play a key role in leading, planning and implementing critical projects to help their organizations succeed'),
('HCM202','Ho Chi Minh Ideology', 'Ho Chi Minh ideology is the crystallization of the thousand-year-old traditions of national construction and defense of the Vietnamese people. On that basis, Ho Chi Minh ideology has collected intellectual values of absorbing Eastern, Western, creatively applied and developed Marxism-Leninism to Vietnamese practices')

---create lecture
INSERT INTO [dbo].[User] ([Account], [Email], [Password], [RoleId]) VALUES ('dattt67','dattt67@fpt.edu.vn','$2a$11$I9Ws2gKnK7Dcwu8qu8XXg.QL9oViygMKsWi4PuII8tFuZJRSWycLa',3)
INSERT INTO [dbo].[Lecturer] ([Account],LecturerName,[Phone]) VALUES ('dattt67','Tran Tien Dat','0123456789')

---create class
Insert into [dbo].[Class] (ClassName, CourseId, LecturerAccount, StartDate, EndDate) values
('SWP391-Summer2023-DatTT67',14,'dattt67','2023-10-01','2023-12-01'),
('SWP391-Summer2023-DatTT67-1',14,'dattt67','2023-10-01','2023-12-01')

---create exam
insert into [dbo].[Exam] (ClassId, LecturerAccount, Title, Summary, StartDate, EndDate, Timer, TakingTimes,Score,Permission) values
(1,'dattt67','Progress test 1','Test kien thuc chuong 1','2023-11-02','2023-11-05',60,5,3,0),
(1,'dattt67','Progress test 2','Test kien thuc chuong 2','2023-10-11','2023-10-20',90,1,0,0),
(1,'dattt67','Progress test 3','Test kien thuc chuong 3','2023-10-11','2023-10-20',120,2,0,0),
(2,'dattt67','Progress test 1','Kiem tra kien thuc chuong 1','2023-10-11','2023-10-20',60,5,0,0),
(2,'dattt67','Progress test 2','Kiem tra kien thuc chuong 2','2023-10-11','2023-10-20',90,1,0,0),
(2,'dattt67','Progress test 3','Kiem tra kien thuc chuong 3','2023-10-11','2023-10-20',120,2,0,0)

----
insert into [dbo].[QuestionExam] (ExamId, Content, Type, Score) values
(1,'What is software modeling?',0,1),
(1,'1 + 1 = ?',1,2)

insert into [dbo].[QuestionExamAnswer] (QuesId, Content, Correct, [Percent]) values
(1,'Developing models of software.',0,0),
(1,'Designing software applications before coding.',1,100),
(1,'Developing software diagrams.',0,0),
(1,'Developing software prototypes.',0,0),

(2,'2',1,50),
(2,'3-1',1,50),
(2,'1',0,0),
(2,'0',0,0)

----------sinh ra data mau~
----user: 1 manager, 2 lecturer, 4 student
INSERT INTO [dbo].[User] ([Account], [Email], [Password], [RoleId], [IsActive]) VALUES ('longdhhe150008','longdhhe150008@fpt.edu.vn','$2a$11$I9Ws2gKnK7Dcwu8qu8XXg.QL9oViygMKsWi4PuII8tFuZJRSWycLa',2,1)
INSERT INTO [dbo].[Manager] ([Account],[ManagerName],[Phone]) VALUES ('longdhhe150008','Do Hai Long', '0394122468')

INSERT INTO [dbo].[User] ([Account], [Email], [Password], [RoleId]) VALUES ('namtt67','namtt67@fpt.edu.vn','$2a$11$I9Ws2gKnK7Dcwu8qu8XXg.QL9oViygMKsWi4PuII8tFuZJRSWycLa',3)
INSERT INTO [dbo].[Lecturer] ([Account],LecturerName,[Phone]) VALUES ('namtt67','Tran Tien Nam','0123456789')
INSERT INTO [dbo].[User] ([Account], [Email], [Password], [RoleId]) VALUES ('lamtt67','lamtt67@fpt.edu.vn','$2a$11$I9Ws2gKnK7Dcwu8qu8XXg.QL9oViygMKsWi4PuII8tFuZJRSWycLa',3)
INSERT INTO [dbo].[Lecturer] ([Account],LecturerName,[Phone]) VALUES ('lamtt67','Tran Tien Lam','0123456789')

INSERT INTO [dbo].[User] ([Account], [Email], [Password], [RoleId], [IsActive]) VALUES ('duydnhe160333','duydnhe160333@fpt.edu.vn','$2a$11$0362ARrMfcaYnjo6QcC7E.HJ5WZ/m7TdeTiVB83/QTanPvCZmpypK',4,1)
INSERT INTO [dbo].[Student] ([Account],[MSSV],[StudentName],[DOB],[Phone],[Major]) VALUES ('duydnhe160333','HE160333','Do Ngoc Duy3', '2002-10-21','0394122468','Software Engineering')
INSERT INTO [dbo].[User] ([Account], [Email], [Password], [RoleId], [IsActive]) VALUES ('duydnhe160343','duydnhe160343@fpt.edu.vn','$2a$11$0362ARrMfcaYnjo6QcC7E.HJ5WZ/m7TdeTiVB83/QTanPvCZmpypK',4,1)
INSERT INTO [dbo].[Student] ([Account],[MSSV],[StudentName],[DOB],[Phone],[Major]) VALUES ('duydnhe160343','HE160343','Do Ngoc Duy4', '2002-10-21','0394122468','Software Engineering')
INSERT INTO [dbo].[User] ([Account], [Email], [Password], [RoleId], [IsActive]) VALUES ('duydnhe160353','duydnhe160353@fpt.edu.vn','$2a$11$0362ARrMfcaYnjo6QcC7E.HJ5WZ/m7TdeTiVB83/QTanPvCZmpypK',4,1)
INSERT INTO [dbo].[Student] ([Account],[MSSV],[StudentName],[DOB],[Phone],[Major]) VALUES ('duydnhe160353','HE160353','Do Ngoc Duy5', '2002-10-21','0394122468','Software Engineering')
INSERT INTO [dbo].[User] ([Account], [Email], [Password], [RoleId], [IsActive]) VALUES ('duydnhe160363','duydnhe160363@fpt.edu.vn','$2a$11$0362ARrMfcaYnjo6QcC7E.HJ5WZ/m7TdeTiVB83/QTanPvCZmpypK',4,1)
INSERT INTO [dbo].[Student] ([Account],[MSSV],[StudentName],[DOB],[Phone],[Major]) VALUES ('duydnhe160363','HE160363','Do Ngoc Duy6', '2002-10-21','0394122468','Software Engineering')
INSERT INTO [dbo].[User] ([Account], [Email], [Password], [RoleId], [IsActive]) VALUES ('thinhnvhe160369','thinhnvhe160369@fpt.edu.vn','$2a$11$0362ARrMfcaYnjo6QcC7E.HJ5WZ/m7TdeTiVB83/QTanPvCZmpypK',4,1)
INSERT INTO [dbo].[Student] ([Account],[MSSV],[StudentName],[DOB],[Phone],[Major]) VALUES ('thinhnvhe160369','HE160369','Nguyen Van Thinh', '2002-10-21','0394122468','Software Engineering')
INSERT INTO [dbo].[User] ([Account], [Email], [Password], [RoleId], [IsActive]) VALUES ('tandmhs160999','tandmhs160999@fpt.edu.vn','$2a$11$0362ARrMfcaYnjo6QcC7E.HJ5WZ/m7TdeTiVB83/QTanPvCZmpypK',4,1)
INSERT INTO [dbo].[Student] ([Account],[MSSV],[StudentName],[DOB],[Phone],[Major]) VALUES ('tandmhs160999','HS160999','Dinh Minh Tan', '2002-10-21','0394122468','Software Engineering')