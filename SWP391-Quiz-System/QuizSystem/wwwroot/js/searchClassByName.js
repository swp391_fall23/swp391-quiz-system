﻿
   function search() {
        // Lấy giá trị tìm kiếm từ ô input
        var input = document.getElementById("search");
        var filter = input.value.toLowerCase();

        // Lấy bảng và các hàng
        var table = document.querySelector("table");
        var rows = table.getElementsByTagName("tr");

        // Lặp qua từng hàng và ẩn hoặc hiển thị dựa trên giá trị tìm kiếm
        for (var i = 1; i < rows.length; i++) { // Bắt đầu từ 1 để tránh xử lý hàng tiêu đề
            var classNameCell = rows[i].getElementsByTagName("td")[0]; 

            if (classNameCell) {
                var textValue = classNameCell.textContent || classNameCell.innerText;
                if (textValue.toLowerCase().indexOf(filter) > -1) {
                    rows[i].style.display = "";
                } else {
                    rows[i].style.display = "none";
                }
            }
        }
    }
