// Get the edit modal
var editmodal = document.getElementById("editModal");

// Get the button that opens the edit modal
var editbtn = document.querySelectorAll('.edit-course');

// Get the close element that closes the modal
var span = document.getElementsByClassName("edit-close")[0];

// Get the cancel button that closes the modal
var editcancel = document.getElementsByClassName("edit-cancel-btn")[0];

//get text of the modal
var txtCourseCodeModal = document.getElementById("text_coursecode_modal");
var txtCourseNameModal = document.getElementById("text_coursename_modal");
var txtCourseDesModal = document.getElementById("text_coursedes_modal");
var txtInitialCourseCode = document.getElementById("initial_course_code");

//get input of edit modal
//var inputSave = document.getElementById("course_id_edit");


// When the user clicks the button, open the modal
//function editClick(btn) {
   // editmodal.style.display = "block";
//};

editbtn.forEach(function (button) {
    button.addEventListener('click', function () {
        editmodal.style.display = "block";
        txtCourseCodeModal.value = button.parentElement.parentElement.childNodes[3].innerHTML;
        txtCourseNameModal.value = button.parentElement.parentElement.childNodes[5].innerHTML;
        txtCourseDesModal.value = button.parentElement.parentElement.childNodes[7].innerHTML;
        txtInitialCourseCode.value = button.parentElement.parentElement.childNodes[3].innerHTML;
        //inputSave.value = button.parentElement.parentElement.childNodes[1].innerHTML;
        
    });
});

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    editmodal.style.display = "none";
}

// When the user clicks on cancel button, close the edit modal
editcancel.onclick = function () {
    editmodal.style.display = "none";
}




