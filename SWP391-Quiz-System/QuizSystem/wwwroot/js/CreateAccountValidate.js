﻿function validateField(field, dataType) {
    if (field === undefined || !document.getElementById(field)?.value || dataType === undefined) {
        return true;
    }
    var el = document.getElementById(field);
    if (typeof (el) === 'undefined' && el === null) {
        return true;
    }
    switch (dataType) {
        case "email":
            var reg = /^[a-zA-Z]+(he|hs|se)\d{6}@fpt[.]edu[.]vn$/;
            if (!reg.test(el.value.trim())) {
                return false;
            }
            break;
        case "emailPlain":
            var reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (!reg.test(el.value.trim())) {
                return false;
            }
            break;
        case "name":
            var reg = /^[a-zA-Z ]+$/;
            if (!reg.test(el.value.trim())) {
                return false;
            }
            break;
        case "major":
            if (el.selectedIndex === 0) {
                return false;
            }
            break;
        case "role":
            if (el.selectedIndex === 0) {
                return false;
            }
            break;
        case "account":
            var reg = /^[a-zA-Z]+[a-zA-Z]{2}\d{6}$/;
            if (!reg.test(el.value.trim())) {
                return false;
            }
            break;
        case "accountPlain":
            var reg = /^[a-zA-Z]{2,}\d{0,}$/;
            if (!reg.test(el.value.trim())) {
                return false;
            }
            break;
        case "mssv":
            var reg = /^[a-zA-Z]{2}\d{6}$/;
            if (!reg.test(el.value.trim())) {
                return false;
            }
            break;
        case "phone":
            var reg = /^0[0-9]{9}$/;
            if (!reg.test(el.value.trim())) {
                return false;
            }
            break;
        case "oldPassword":
            if (el.value.length === 0) {
                return false;
            }
            break;
        case "password":
            var reg = /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})/;
            if (!reg.test(el.value.trim())) {
                return false;
            }
            break;
        case "confirmPassword":
            if (el.value !== document.getElementById("password").value.trim()) {
                return false;
            }
            break;
        case "dob":
            var date = new Date(el.value);
            var currentYear = new Date().getFullYear();
            if (date.getFullYear() < currentYear - 100 || date.getFullYear() > currentYear - 18) {
                return false;
            }
            break;
        default:
            return false;
    }
    return true;
}

function markInvalidData(field) {
    if (!document.getElementById(field + "Req")) return;
    if (!validateField(field, field)) {
        document.getElementById(field + "Req").style.color = "red";
    }
    else {
        document.getElementById(field + "Req").style.color = "";
    }

    if (field === "password") {
        if (!validateField("confirmPassword", "confirmPassword")) {
            document.getElementById("confirmPasswordReq").style.color = "red";
        }
        else {
            document.getElementById("confirmPasswordReq").style.color = "";
        }
    }
}
function markInvalidDataRoleDependent(field) {
    var s = document.getElementById("role");
    var role = s.options[s.selectedIndex].text;
    var validateType = field;
    if (role != "Student") validateType += "Plain";

    if (!document.getElementById(field + "Req")) return;
    if (!validateField(field, validateType)) {
        document.getElementById(field + "Req").style.color = "red";
    }
    else {
        document.getElementById(field + "Req").style.color = "";
    }

    if (field === "password") {
        if (!validateField("confirmPassword", "confirmPassword")) {
            document.getElementById("confirmPasswordReq").style.color = "red";
        }
        else {
            document.getElementById("confirmPasswordReq").style.color = "";
        }
    }
}

function validateData(...arr) {
    if (arr === null) arr = ["name", "email", "account", "major", "password", "confirmPassword", "phone", "dob"];
    var isValid = true;
    for (let a of arr) {
        markInvalidData(a);
        if (!validateField(a, a)) {
            console.log(a);
            isValid = false;
            if (a === "email") {
                alert("E-mail must be of '@FPT.EDU.VN' domain. If you are a Lecturer or a Manager, please contact an Admin to register your account instead!");
            }
        }
    }

    if (!isValid) {
        alert("Please double check your submission for any mistakes (Requirements are highlighted in red)!");
    }
    console.log(isValid);
    return isValid;
}
function changeTemplate() {
    var s = document.getElementById("role");
    var role = s.options[s.selectedIndex].text;
    var name = "";
    var phone = "";
    for (r of s.options) {
        if (document.getElementById("UserData_" + r.text + "_" + r.text + "Name") !== null
            && document.getElementById("UserData_" + r.text + "_" + r.text + "Name").value.length > 0) {
            name = document.getElementById("UserData_" + r.text + "_" + r.text + "Name").value;
        }
        if (document.getElementById("UserData_" + r.text + "_Phone") !== null
            && document.getElementById("UserData_" + r.text + "_Phone").value.length > 0) {
            phone = document.getElementById("UserData_" + r.text + "_Phone").value;
        }
    }

    markInvalidData("role");
    markInvalidDataRoleDependent("account");
    markInvalidDataRoleDependent("email");
    var templates = document.getElementsByClassName("InfoTemplate");
    hideAllTemplates();
    switch (role) {
        case "Student":
        case "Lecturer":
        case "Admin":
        case "Manager":
            var t = document.getElementById("info" + role);
            t.classList.remove("d-none");

            if (name.length !== 0 || phone.length !== 0) {
                if (document.getElementById("UserData_" + role + "_" + role + "Name") != null
                    && document.getElementById("UserData_" + role + "_Phone") != null
                    && (document.getElementById("UserData_" + role + "_" + role + "Name").value.length === 0
                        && document.getElementById("UserData_" + role + "_Phone").value.length === 0)
                ) {
                    document.getElementById("UserData_" + role + "_" + role + "Name").value = name;
                    document.getElementById("UserData_" + role + "_Phone").value = phone;
                }
            }
            break;
        default:
            break;
    }
}
function hideAllTemplates() {
    for (r of document.getElementById("role").options) {
        if (r.value === "") continue;
        var t = document.getElementById("info" + r.text);
        if (t.classList !== null && !t.classList.contains("d-none")) t.classList.add("d-none");
    }
}
function submitPwdReset(account) {
    if (confirm("Reset " + account + "'s password ? ")) {
        document.getElementById('pwdForm').submit();
    }
}
function ValidateUpdateUser() {
    var s = document.getElementById("role");
    var role = s.options[s.selectedIndex].value;
    var isValid = true;
    var error = "Check following fields: ";

    if (role === undefined) {
        alert("Select a role for the new account!");
        return false;
    }

    var userPrefix = "UserData_";
    var rolePrefix = userPrefix + role + "_";

    if (!validateField(userPrefix + "Email", "emailPlain")) { isValid = false; error += "Email; " }

    switch (role) {
        case "Student":
            if (!validateField(rolePrefix + "Mssv", "mssv")) { isValid = false; error += "Mssv; " }
            if (!validateField(rolePrefix + "Dob", "dob")) { isValid = false; error += "Dob; " }
            if (!validateField(rolePrefix + "Major", "major")) { isValid = false; error += "Major; " }
        case "Admin":
        case "Lecturer":
        case "Manager":
            if (!validateField(rolePrefix + role + "Name", "name")) { isValid = false; error += "Name; " }
            if (!validateField(rolePrefix + "Phone", "phone")) { isValid = false; error += "Phone; " }
            break;
        default:
            break;
    }

    if (!isValid) { alert(error); }
    return isValid;
}
function ValidateCreateUser() {
    var s = document.getElementById("role");
    var role = s.options[s.selectedIndex].value;
    var isValid = true;
    var error = "Check following fields: ";

    if (role === undefined) {
        alert("Select a role for the new account!");
        return false;
    }


    var validateType = "";
    if (role != "Student") validateType += "Plain";

    if (!validateField("email", "email" + validateType)) { isValid = false; error += "Email; " }
    if (!validateField("account", "account" + validateType)) { isValid = false; error += "Account; " }
    if (!validateField("name", "name")) { isValid = false; error += "Name; " }
    if (!validateField("phone", "phone")) { isValid = false; error += "Phone; " }

    switch (role) {
        case "Student":
            if (!validateField("mssv", "mssv")) { isValid = false; error += "Mssv; " }
            if (!validateField("dob", "dob")) { isValid = false; error += "Dob; " }
            if (!validateField("major", "major")) { isValid = false; error += "Major; " }
        case "Admin":
        case "Lecturer":
        case "Manager":
            break;
        default:
            break;
    }

    if (!isValid) { alert(error); }
    return isValid;
}