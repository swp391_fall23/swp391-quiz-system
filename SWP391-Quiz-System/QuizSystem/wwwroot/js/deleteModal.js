﻿// Get the modal
var modal = document.getElementById("deleteModal");


// Get the button that opens the modal
var btn = document.querySelectorAll('.delete-course');

// Get the close element that closes the modal
var span = document.getElementsByClassName("close")[0];

// Get the cancel button that closes the modal
var cancel = document.getElementsByClassName("cancel-btn")[0];

// When the user clicks the button, open the modal
function deleteClick(btn) {
    modal.style.display = "block";
};

btn.forEach(function (button) {
    button.addEventListener('click', deleteClick);
});


// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
}

// When the user clicks on cancel button, close the modal
cancel.onclick = function () {
    modal.style.display = "none";
}


// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
    if (event.target == editmodal) {
        editmodal.style.display = "none";
    }
}