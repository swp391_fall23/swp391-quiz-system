﻿var edit_modal = document.getElementById("edit-question-modal");
var delete_modal = document.getElementById("delete-question-modal");
var create_modal = document.getElementById("create-question-modal");
var modal_overlay = document.getElementById("modal-overlay");

var inputQuesIDDelete = document.querySelector("input[name='quesID']");
var editFormAnswers = document.getElementById("answers-container-edit");

function showDeleteModal(quesID) {
    edit_modal.style.display = "none";
    delete_modal.style.display = "block";
    create_modal.style.display = "none";
    modal_overlay.style.display = "block";

    inputQuesIDDelete.value = quesID;
}


var save_ques_mes = document.getElementById("save-ques-mes");

//can sua
function showEditModal(btn, quesID,
     question1,  typeSelectEdit,  score1) {
    edit_modal.style.display = "block";
    delete_modal.style.display = "none";
    create_modal.style.display = "none";
    modal_overlay.style.display = "block";
    //truyen du lieu cua ques
    document.editQuesForm.quesID.value = quesID;
    document.editQuesForm.question1.value = question1;
    document.editQuesForm.typeSelect.value = (typeSelectEdit == "False" ? "0" : "1");
    document.editQuesForm.score1.value = Number(score1.replace(',', '.'));

    var answersElm = btn.parentElement.nextElementSibling;

    var answer_content = [].slice.call(answersElm.getElementsByClassName("answer"));
    var answer_percent = [].slice.call(answersElm.getElementsByClassName("answer-percent"));

    //truyen du lieu cho answer
    for (var i = 0; i < answer_content.length; i++) {
        var div = document.createElement("div");
        var divouterHTML;
        editFormAnswers.appendChild(div);

        //div.classList.add('answer-new-ques');
        divouterHTML = "<div class='answer-new-ques'><div class='answer-content'>" +
            "Answer: <input name='' type = 'text' class='answer-content-input' value='" + answer_content[i].innerHTML + "' required />" +
            "<i class='fa fa-trash-o answer-trash' aria-hidden='true'></i>" +
            "<i class='fa fa-chevron-down answer-arrow' aria-hidden='true'> </i>" +
            "</div>" +
            "<div class='answer-info'>" +
            "<div class='answer-correct'>";

        if (answer_content[i].classList.contains("answer-true-content")) {
            divouterHTML += "<input type = 'radio' name = '' class='add-ans-input' value = true checked > True &nbsp&nbsp&nbsp&nbsp" +
                "<input type = 'radio' name = '' class='add-ans-input' value = false > False" +
                "</div>";
        } else {
            divouterHTML += "<input type = 'radio' name = '' class='add-ans-input' value = true > True &nbsp&nbsp&nbsp&nbsp" +
                "<input type = 'radio' name = '' class='add-ans-input' value = false checked> False" +
                "</div>";
        }

        if (typeSelectEdit == "False") {
            divouterHTML += "<div class='answer-percent' >" +
                "Answer weight(%):" +
                "<input name = '' type = 'number' min = '1' max = '100' class='answer-weight-input' required disabled />" +
                "</div>" +
                "</div></div>";
        } else {
            if (answer_content[i].classList.contains("answer-true-content")) {
                var percentOfAns = answer_percent[i].textContent.trim();
                divouterHTML += "<div class='answer-percent' >" +
                    "Answer weight(%):" +
                    "<input name = '' type = 'number' min = '1' max = '100' class='answer-weight-input' value='" + parseInt(percentOfAns.slice(1, percentOfAns.length - 2)) + "' required />" +
                    "</div>" +
                    "</div></div>";
            } else {
                divouterHTML += "<div class='answer-percent' >" +
                    "Answer weight(%):" +
                    "<input name = '' type = 'number' min = '1' max = '100' class='answer-weight-input' required disabled />" +
                    "</div>" +
                    "</div></div>";
            }
        }

        div.outerHTML += divouterHTML;
        Reset1();
    }

    //cho arrow va trash bin event listener
    var answer_arrow_list = [].slice.call(document.editQuesForm.getElementsByClassName("answer-arrow"));
    var answer_trash_list = [].slice.call(document.editQuesForm.getElementsByClassName("answer-trash"));
    //arrow
    answer_arrow_list.forEach(function (curVal) {
        curVal.onclick = function () {
            if (curVal.parentElement.nextElementSibling.style.display == "none") {
                curVal.parentElement.nextElementSibling.style.display = "block";
            } else if (curVal.parentElement.nextElementSibling.style.display == "block") {
                curVal.parentElement.nextElementSibling.style.display = "none";
            } else {
                curVal.parentElement.nextElementSibling.style.display = "none";
            }
        }
    });
    //trash-bin
    answer_trash_list.forEach(function (curVal) {
        curVal.onclick = function () {
            curVal.parentElement.parentElement.remove();
            selectOnChange1();
            checkNumberOfAnswer1();
        }
    });

    //on change for typeselect
    document.getElementById("typeSelectEdit").addEventListener("change", selectOnChange1);
    //on change for input weight
    var inputs_weight = [].slice.call(document.editQuesForm.getElementsByClassName("answer-weight-input"));
    inputs_weight.forEach(function (curVal) {
        curVal.addEventListener('change', function () {
            var inputWeight = [].slice.call(document.editQuesForm.getElementsByClassName("answer-weight-input"));
            var z = 0;
            inputWeight.forEach(function (curVal) {
                if (curVal.disabled == false) {
                    z += Number(curVal.value);
                }
            });
            if (z == 100) {
                enableEdit();
                save_ques_mes.innerHTML = "";
            } else {
                disableEdit();
                save_ques_mes.innerHTML = "Total of answer's weight is not 100%! Total now: " + z + "%";
            }
        });
    });
    //on change for radio btn
    //radio on change
    var radioList = [].slice.call(document.editQuesForm.getElementsByClassName("add-ans-input"));

    radioList.forEach(function (curVal) {
        curVal.addEventListener('change', selectOnChange1);
    })

    //check number of answer
    checkNumberOfAnswer1();
}

function checkNumberOfAnswer1() {
    var answerList = [].slice.call(document.editQuesForm.getElementsByClassName('answer-new-ques'));
    if (answerList.length < 2) {
        disableEdit();
        save_ques_mes.innerHTML = "Please add 2 or more answers to the question!!";
    } else {
        enableEdit();
        save_ques_mes.innerHTML = "";
        selectOnChange1();
    }
}

function selectOnChange1() {
    var x = document.getElementById("typeSelectEdit").value;
    var inputWeight = [].slice.call(document.editQuesForm.getElementsByClassName("answer-weight-input"));
    var inputCorrect = [].slice.call(document.editQuesForm.getElementsByClassName("add-ans-input"));

    var y = 0;
    var z = 0;

    inputCorrect.forEach(function (curVal, index) {
        if (curVal.checked) {
            if (index % 2 == 0) {
                y++;
            }
        }
    });

    if (x == '0') {
        inputWeight.forEach(function (curVal) {
            curVal.disabled = true;
        });
        if (y != 1) {
            disableEdit();
            save_ques_mes.innerHTML = "Please choose 1 true answer!"
        } else {
            enableEdit();
            save_ques_mes.innerHTML = "";
        }

    } else {
        inputCorrect.forEach(function (curVal, index) {
            if (curVal.checked) {
                if (index % 2 == 0) {
                    inputWeight[index / 2].disabled = false;
                } else {
                    inputWeight[(index - 1) / 2].disabled = true;
                }
            }
        });

        inputWeight.forEach(function (curVal) {
            if (curVal.disabled == false) {
                z += Number(curVal.value);
            }
        });

        if (y == 0) {
            disableEdit();
            save_ques_mes.innerHTML = "There is 0 true answer!"
        } else {
            if (z == 100) {
                enableEdit();
                save_ques_mes.innerHTML = "";
            } else {
                disableEdit();
                save_ques_mes.innerHTML = "Total of answer's weight is not 100%! Total now: " + z + "%";
            }
        }
    }
};

function addAnswer1() {
    var div = document.createElement("div");
    var typeSelectEdit = document.editQuesForm.typeSelect.value;
    console.log(typeSelectEdit);
    editFormAnswers.appendChild(div);

    //div.classList.add('answer-new-ques');
    var divouterHTML = "<div class='answer-new-ques'><div class='answer-content'>" +
        "Answer: <input name='' type = 'text' class='answer-content-input' required />" +
        "<i class='fa fa-trash-o answer-trash' aria-hidden='true'></i>" +
        "<i class='fa fa-chevron-down answer-arrow' aria-hidden='true'> </i>" +
        "</div>" +
        "<div class='answer-info'>" +
        "<div class='answer-correct'>" +
        "<input type = 'radio' name = '' class='add-ans-input' value = true checked > True &nbsp&nbsp&nbsp&nbsp" +
        "<input type = 'radio' name = '' class='add-ans-input' value = false > False" +
        "</div>";

    if (typeSelectEdit == "False") {
        divouterHTML += "<div class='answer-percent' >" +
            "Answer weight(%):" +
            "<input name = '' type = 'number' min = '1' max = '100' class='answer-weight-input' required disabled />" +
            "</div>" +
            "</div></div>";
    } else
    {
        divouterHTML += "<div class='answer-percent' >" +
            "Answer weight(%):" +
            "<input name = '' type = 'number' min = '1' max = '100' class='answer-weight-input' value=1 required />" +
            "</div>" +
            "</div></div>";
    }


    div.outerHTML = divouterHTML;

    Reset1();

    //cho arrow va trash bin event listener
    var answer_arrow_list = [].slice.call(document.editQuesForm.getElementsByClassName("answer-arrow"));
    var answer_trash_list = [].slice.call(document.editQuesForm.getElementsByClassName("answer-trash"));
    //arrow
    answer_arrow_list.forEach(function (curVal) {
        curVal.onclick = function () {
            if (curVal.parentElement.nextElementSibling.style.display == "none") {
                curVal.parentElement.nextElementSibling.style.display = "block";
            } else if (curVal.parentElement.nextElementSibling.style.display == "block") {
                curVal.parentElement.nextElementSibling.style.display = "none";
            } else {
                curVal.parentElement.nextElementSibling.style.display = "none";
            }
        }
    });
    //trash-bin
    answer_trash_list.forEach(function (curVal) {
        curVal.onclick = function () {
            curVal.parentElement.parentElement.remove();
            Reset1();
            selectOnChange1();
            checkNumberOfAnswer1();
        }
    });

    //on change for typeselect
    document.getElementById("typeSelectEdit").addEventListener("change", selectOnChange1);
    //on change for input weight
    var inputs_weight = [].slice.call(document.editQuesForm.getElementsByClassName("answer-weight-input"));
    inputs_weight.forEach(function (curVal) {
        curVal.addEventListener('change', function () {
            var inputWeight = [].slice.call(document.editQuesForm.getElementsByClassName("answer-weight-input"));
            var z = 0;
            inputWeight.forEach(function (curVal) {
                if (curVal.disabled == false) {
                    z += Number(curVal.value);
                }
            });
            if (z == 100) {
                enableEdit();
                save_ques_mes.innerHTML = "";
            } else {
                disableEdit();
                save_ques_mes.innerHTML = "Total of answer's weight is not 100%! Total now: " + z + "%";
            }
        });
    });
    //on change for radio btn
    //radio on change
    var radioList = [].slice.call(document.editQuesForm.getElementsByClassName("add-ans-input"));

    radioList.forEach(function (curVal) {
        curVal.addEventListener('change', selectOnChange1);
    })

    //check number of answer
    checkNumberOfAnswer1();
}

//reset thu tu cua input trong add form
function Reset1() {
    var listAnswer = [].slice.call(document.editQuesForm.getElementsByClassName("answer-new-ques"));
    var listAnswerOfAdd;
    listAnswer.forEach(function (curVal, index) {
        listAnswerOfAdd = [].slice.call(curVal.querySelectorAll("input"));
        listAnswerOfAdd[0].setAttribute('name', '[' + index + '].Content');
        listAnswerOfAdd[1].setAttribute('name', '[' + index + '].Correct');
        listAnswerOfAdd[2].setAttribute('name', '[' + index + '].Correct');
        listAnswerOfAdd[3].setAttribute('name', '[' + index + '].Percent');
    });
}

//function disable btn save cau hoi
var saveBtn = document.querySelector("input.save-question-btn");
function disableEdit() {
    saveBtn.disabled = true;
}

function enableEdit() {
    saveBtn.disabled = false;
} 

function showCreateModal() {
    edit_modal.style.display = "none";
    delete_modal.style.display = "none";
    create_modal.style.display = "block";
    modal_overlay.style.display = "block";
}

function hideModal() {
    edit_modal.style.display = "none";
    delete_modal.style.display = "none";
    create_modal.style.display = "none";
    modal_overlay.style.display = "none";
    editFormAnswers.innerHTML = "";
}


///////////////////
var answer_container = document.getElementById("answers-container");
var add_ques_mes = document.getElementById("add-ques-mes");

function addAnswer() {
    var div = document.createElement("div");
    answer_container.appendChild(div);

    //div.classList.add('answer-new-ques');
    div.outerHTML = "<div class='answer-new-ques'><div class='answer-content'>" +
        "Answer: <input name='' type = 'text' class='answer-content-input' required />" +
        "<i class='fa fa-trash-o answer-trash' aria-hidden='true'></i>" +
        "<i class='fa fa-chevron-down answer-arrow' aria-hidden='true'> </i>" +
        "</div>" +
        "<div class='answer-info'>" +
        "<div class='answer-correct'>" +
        "<input type = 'radio' name = '' class='add-ans-input' value = true checked > True &nbsp&nbsp&nbsp&nbsp" +
        "<input type = 'radio' name = '' class='add-ans-input' value = false > False" +
        "</div>" +
    "<div class='answer-percent' >"+
    "Answer weight(%):" +
        "<input name = '' type = 'number' min = '1' max = '100' class='answer-weight-input' value=1 required disabled />" +
        "</div>" +
        "</div></div>";

    var answer_arrow_list = [].slice.call(document.addQuesForm.getElementsByClassName("answer-arrow"));
    var answer_trash_list = [].slice.call(document.addQuesForm.getElementsByClassName("answer-trash"));

    answer_arrow_list.forEach(function (curVal) {
        curVal.onclick = function () {
            if (curVal.parentElement.nextElementSibling.style.display == "none") {
                curVal.parentElement.nextElementSibling.style.display = "block";
            } else if (curVal.parentElement.nextElementSibling.style.display == "block") {
                curVal.parentElement.nextElementSibling.style.display = "none";
            } else {
                curVal.parentElement.nextElementSibling.style.display = "none";
            }
        }
    });

    answer_trash_list.forEach(function (curVal) {
        curVal.onclick = function () {
            curVal.parentElement.parentElement.remove();
            //reset thu tu cua cac input trong answer
            Reset();
            selectOnChange();
            checkNumberOfAnswer();
        }
    });

    document.getElementById("typeSelect").addEventListener("change", selectOnChange);

    //input weight on change
    var inputs_weight = [].slice.call(document.addQuesForm.getElementsByClassName("answer-weight-input"));
    inputs_weight.forEach(function (curVal) {
        curVal.addEventListener('change', function () {
            var inputWeight = [].slice.call(document.addQuesForm.getElementsByClassName("answer-weight-input"));
            var z = 0;
            inputWeight.forEach(function (curVal) {
                if (curVal.disabled == false) {
                    z += Number(curVal.value);
                }
            });
            if (z == 100) {
                enableAdd();
                add_ques_mes.innerHTML = "";
            } else {
                disableAdd();
                add_ques_mes.innerHTML = "Total of answer's weight is not 100%! Total now: " + z + "%";
            }
        });
    });


    //radio on change
    var radioList = [].slice.call(document.addQuesForm.getElementsByClassName("add-ans-input"));

    radioList.forEach(function (curVal) {
        curVal.addEventListener('change', selectOnChange);
    })

    //check number of answer
    checkNumberOfAnswer();

    Reset();
}

function checkNumberOfAnswer() {
    var answerList = [].slice.call(document.addQuesForm.getElementsByClassName('answer-new-ques'));
    if (answerList.length < 2) {
        disableAdd();
        add_ques_mes.innerHTML = "Please add 2 or more answers to the question!!";
    } else {
        enableEdit();
        add_ques_mes.innerHTML = "";
        selectOnChange();
    }
}

//select on change
function selectOnChange() {
    var x = document.getElementById("typeSelect").value;
    var inputWeight = [].slice.call(document.addQuesForm.getElementsByClassName("answer-weight-input"));
    var inputCorrect = [].slice.call(document.addQuesForm.getElementsByClassName("add-ans-input"));

    var y = 0;
    var z = 0;

    inputCorrect.forEach(function (curVal, index) {
        if (curVal.checked) {
            if (index % 2 == 0) {
                y++;
            }
        }
    });

    if (x == '0') {
        inputWeight.forEach(function (curVal) {
            curVal.disabled = true;
        });
        if (y != 1) {
            disableAdd();
            add_ques_mes.innerHTML = "Please choose 1 true answer!"
        } else {
            enableAdd();
            add_ques_mes.innerHTML = "";
        }

    } else {
        inputCorrect.forEach(function (curVal, index) {
            if (curVal.checked) {
                if (index % 2 == 0) {
                    inputWeight[index / 2].disabled = false;
                } else {
                    inputWeight[(index - 1) / 2].disabled = true;
                }
            }
        });

        inputWeight.forEach(function (curVal) {
            if (curVal.disabled == false) {
                z += Number(curVal.value);
            }
        });

        if (y == 0) {
            disableAdd();
            add_ques_mes.innerHTML = "There is 0 true answer!"
        } else {
            if (z == 100) {
                enableAdd();
                add_ques_mes.innerHTML = "";
            } else {
                disableAdd();
                add_ques_mes.innerHTML = "Total of answer's weight is not 100%! Total now: " + z +"%";
            }
        }
    }
}
//reset thu tu cua input trong add form
function Reset() {
    var listAnswer = [].slice.call(document.addQuesForm.getElementsByClassName("answer-new-ques"));
    var listAnswerOfAdd;
    listAnswer.forEach(function (curVal, index) {
        listAnswerOfAdd = [].slice.call(curVal.querySelectorAll("input"));
        listAnswerOfAdd[0].setAttribute('name', '[' + index + '].Content');
        listAnswerOfAdd[1].setAttribute('name', '[' + index + '].Correct');
        listAnswerOfAdd[2].setAttribute('name', '[' + index + '].Correct');
        listAnswerOfAdd[3].setAttribute('name', '[' + index + '].Percent');
    });
}

//function disable btn add cau hoi
var addBtn = document.querySelector("input.add-question-btn");
function disableAdd() {
    addBtn.disabled = true;
}

function enableAdd() {
    addBtn.disabled = false;
}

//check input content of the question
var quesInput = document.addQuesForm.question;
var quesInput1 = document.editQuesForm.question1;
var alertQuesContent = document.getElementById("alert-ques-content");
var alertQuesContent1 = document.getElementById("alert-ques-content1");

quesInput.addEventListener("change", checkInputTxt);
quesInput1.addEventListener("change", checkInputTxt1);

function checkInputTxt() {
    if (quesInput.value.trim() === '') {
        alertQuesContent.innerHTML = "Please do not leave blank or just enter spaces";
        disableAdd();
    } else {
        alertQuesContent.innerHTML = "";
        enableAdd();
    }
}

function checkInputTxt1() {
    if (quesInput1.value.trim() === '') {
        alertQuesContent1.innerHTML = "Please do not leave blank or just enter spaces";
        disableEdit();
    } else {
        alertQuesContent1.innerHTML = "";
        enableEdit();
    }
}

////
var alertAnswer_Add = document.getElementById("alert-answer-add");

document.getElementById("add-more-answer").addEventListener("click", checkAnswerInputForPlus);

function checkAnswerInputForPlus() {
    var answerList = [].slice.call(document.addQuesForm.getElementsByClassName("answer-content-input"));
    answerList.forEach(function (cur, index) {
        cur.addEventListener("change", checkAnswerInput);
    });
    checkAnswerInput();
}

function checkAnswerInput() {
    var answerList = [].slice.call(document.addQuesForm.getElementsByClassName("answer-content-input"));
    var isOK = true;

    answerList.forEach(function (cur, index) {
        if (cur.value.trim() === "") {
            isOK = false;
        }
    });

    alertAnswer_Add.innerHTML = isOK ? "" : "Please do not leave blank or just enter spaces in the field answer!";
}

////
var alertAnswer_Edit = document.getElementById("alert-answer-edit");

document.getElementById("add-more-answer-1").addEventListener("click", checkAnswerInputForPlus1);

function checkAnswerInputForPlus1() {
    var answerList = [].slice.call(document.editQuesForm.getElementsByClassName("answer-content-input"));
    answerList.forEach(function (cur, index) {
        cur.addEventListener("change", checkAnswerInput1);
    });
    checkAnswerInput1();
}

function checkAnswerInput1() {
    var answerList = [].slice.call(document.editQuesForm.getElementsByClassName("answer-content-input"));
    var isOK = true;

    answerList.forEach(function (cur, index) {
        if (cur.value.trim() === "") {
            isOK = false;
        }
    });

    alertAnswer_Edit.innerHTML = isOK ? "" : "Please do not leave blank or just enter spaces in the field answer!";
}