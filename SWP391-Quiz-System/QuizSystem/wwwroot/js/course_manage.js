﻿var courseList = [].slice.call( document.getElementsByClassName("course"));
var searchInput = document.getElementById("search-name");
var searchBtn = document.getElementById("search-name-btn");

function search() {
    var searchString = searchInput.value.toLowerCase(); // Chuyển searchString thành chữ thường
    var hideArr = Array.prototype.filter.call(courseList, function (val, index) {
        return !val.childNodes[3].innerHTML.toLowerCase().includes(searchString);
    });

    var notHideArr = Array.prototype.filter.call(courseList, function (val, index) {
        return val.childNodes[3].innerHTML.toLowerCase().includes(searchString);
    });

    hideArr.forEach(function (curVal) {
        curVal.style.display = 'none';
    });

    notHideArr.forEach(function (curVal) {
        curVal.style.display = 'flex';
    });
}

