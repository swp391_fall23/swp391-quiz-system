﻿var edit_modal = document.getElementById("edit-exam-modal");
var delete_modal = document.getElementById("delete-exam-modal");
var modal_overlay = document.getElementById("modal-overlay");

var startDate = document.getElementById("start-date");
var endDate = document.getElementById("end-date");
var duration = document.editExamForm.dur;
var examID_input = document.editExamForm.examID;
var title_input = document.editExamForm.title;
var sum_input = document.editExamForm.summary;
var atm_input = document.editExamForm.numOfAttempts;
var submitBtn = document.getElementById("submit-btn");
var alert = document.getElementById("alert-date");
var alert1 = document.getElementById("alert-dur");
var alert2 = document.getElementById("alert-atp");

function showDeleteModal() {
    edit_modal.style.display = "none";
    delete_modal.style.display = "block";
    modal_overlay.style.display = "block";
}

function showEditModal(examID, title, sum, start,end,dur, attempts) {
    edit_modal.style.display = "block";
    delete_modal.style.display = "none";
    modal_overlay.style.display = "block";

    var start2 = start.slice(3, 6) + start.slice(0, 3) + start.slice(6, start.length);
    var end2 = end.slice(3, 6) + end.slice(0, 3) + end.slice(6, end.length);

    var start1 = new Date(start2);
    var end1 = new Date(end2);
    start1.setMinutes(start1.getMinutes() - start1.getTimezoneOffset());
    end1.setMinutes(end1.getMinutes() - end1.getTimezoneOffset());

    examID_input.value = examID;
    title_input.value = title;
    sum_input.value = sum;
    startDate.value = start1.toISOString().slice(0, 16);
    endDate.value = end1.toISOString().slice(0, 16);
    duration.value = dur;
    atm_input.value = attempts;
}

function hideModal() {
    edit_modal.style.display = "none";
    delete_modal.style.display = "none";
    modal_overlay.style.display = "none";
}
//
var timeDisplay = document.getElementById("time-display");
function refreshTime() {
    var dateString = new Date().toLocaleString();
    var formattedString = dateString.replace(", ", " - ");
    timeDisplay.innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
        "Now: " + formattedString;
}
setInterval(refreshTime, 1000);

//
startDate.addEventListener("change", checkAll);
endDate.addEventListener("change", checkAll);
duration.addEventListener("change", checkAll);

function checkDate() {
    var start = new Date(startDate.value);
    var end = new Date(endDate.value);
    var dur = parseInt(duration.value);
    var q = new Date();

    if (endDate.value == '' && startDate.value == '') {
        if (dur <= 0) {
            //submitBtn.disabled = true;
            alert1.innerHTML = 'Duration must be greater than 0!';
            return false;
        } else {
            //submitBtn.disabled = false;
            alert.innerHTML = '';
            alert1.innerHTML = '';
            return true;
        }
    } else if (endDate.value != '' && startDate.value != '') {
        if (start < q) {
            //submitBtn.disabled = true;
            alert.innerHTML = 'StartDate must be beyond/equal to now!';
            return false;
        } else {
            if (start >= end) {
                //submitBtn.disabled = true;
                alert.innerHTML = 'EndDate must be beyond StartDate!';
                return false;
            } else {
                if (duration.value == '') {
                    //submitBtn.disabled = false;
                    alert.innerHTML = '';
                    alert1.innerHTML = '';
                    return true;
                } else {
                    if (dur <= 0) {
                        //submitBtn.disabled = true;
                        alert.innerHTML = '';
                        alert1.innerHTML = 'Duration must be greater than 0!';
                        return false;
                    } else if (dateDiff(start, end) < dur) {
                        //submitBtn.disabled = true;
                        alert1.innerHTML = 'Duration between StartDate and EndDate must be >= Duration!';
                        return false;
                    } else {
                        //submitBtn.disabled = false;
                        alert.innerHTML = '';
                        alert1.innerHTML = '';
                        return true;
                    }
                }
            }
        }
    } else {
        //submitBtn.disabled = true;
        alert.innerHTML = 'Please input both of the date!';
        return false;
    }
}

//date diff => seconds
function dateDiff(date1, date2) {
    var diff = Math.abs(date2 - date1);
    return Math.ceil(diff / (1000));
}

/////check input title vs summary
var titleInput = document.editExamForm.title;
var sumInput = document.editExamForm.summary;
var alertTitle = document.getElementById("alert-title");
var alertSum = document.getElementById("alert-sum");

titleInput.addEventListener("change", checkAll);
sumInput.addEventListener("change", checkAll);

function checkInputTxt() {
    //check title
    if (titleInput.value.trim() === '') {
        alertTitle.innerHTML = "Please do not leave blank or just enter spaces";
        //submitBtn.disabled = true;
    } else {
        alertTitle.innerHTML = "";
        //submitBtn.disabled = false;
    }
    //check summary
    if (sumInput.value.trim() === '') {
        alertSum.innerHTML = "Please do not leave blank or just enter spaces";
        //submitBtn.disabled = true;
    } else {
        alertSum.innerHTML = "";
        //submitBtn.disabled = false;
    }

    if (titleInput.value.trim() === '' || sumInput.value.trim() === '') {
        return false;
    } else {
        return true;
    }
}

function checkAll() {
    if (checkDate() == true && checkInputTxt() == true) {
        submitBtn.disabled = false;
    } else {
        submitBtn.disabled = true;
    }
}