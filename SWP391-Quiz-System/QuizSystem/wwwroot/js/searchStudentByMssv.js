﻿var studentList = [].slice.call(document.getElementsByClassName("student"));
var searchInput = document.getElementById("search-name");
var searchBtn = document.getElementById("search-name-btn");

function search() {
    var searchString = searchInput.value.toLowerCase(); 
    var hideArr = Array.prototype.filter.call(studentList, function (val, index) {
        return !val.childNodes[3].innerHTML.toLowerCase().includes(searchString);
    });

    var notHideArr = Array.prototype.filter.call(studentList, function (val, index) {
        return val.childNodes[3].innerHTML.toLowerCase().includes(searchString);
    });

    hideArr.forEach(function (curVal) {
        curVal.style.display = 'none';
    });

    notHideArr.forEach(function (curVal) {
        curVal.style.display = 'flex';
    });
}
