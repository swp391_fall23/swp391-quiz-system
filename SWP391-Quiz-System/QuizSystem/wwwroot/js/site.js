﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

var usernav = document.querySelectorAll(".user-nav ul li.logined");
var userchoice = document.querySelector(".user-nav-choice");

for (var j = 0; j < usernav.length; j++) {
    usernav[j].addEventListener("click", function () {
        var a = false;
        for (var i = 0; i < userchoice.classList.length; i++) {
            if (userchoice.classList[i] == "d-none") {
                a = true; break;
            }
        }
        if (!a) {
            userchoice.classList.add("d-none");
        } else {
            userchoice.classList.remove("d-none");
        }
    })
}