// Get the modal
var modal = document.getElementById("deleteModal");


// Get the button that opens the modal
var btn = document.querySelectorAll('.delete-course');

// Get the close element that closes the modal
var span = document.getElementsByClassName("close")[0];

// Get the cancel button that closes the modal
var cancel = document.getElementsByClassName("cancel-btn")[0];

//get text of the modal
var txtModal = document.getElementById("text_delete_modal");
//get input of delete modal
var inputDelete = document.getElementById("course_code_delete");

// When the user clicks the button, open the modal
//function deleteClick(btn) {
//    modal.style.display = "block";
//};

btn.forEach(function (button) {
    button.addEventListener('click', function () {
        modal.style.display = "block";
        txtModal.innerHTML = "Are you sure you want to delete " + button.parentElement.parentElement.childNodes[3].innerHTML + "?";
        inputDelete.value = button.parentElement.parentElement.childNodes[3].innerHTML;
    });
});


// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
}

// When the user clicks on cancel button, close the modal
cancel.onclick = function () {
    modal.style.display = "none";
}


// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
    if (event.target == editmodal) {
        editmodal.style.display = "none";
    }
    if (event.target == addmodal) {
        addmodal.style.display = "none";
    }
    if (event.target == detailmodal) {
        detailmodal.style.display = "none";
    }
}

