// Get the edit modal
var editmodal = document.getElementById("editModal");

// Get the button that opens the edit modal
var editbtn = document.querySelectorAll('.edit-course');

// Get the close element that closes the modal
var span = document.getElementsByClassName("edit-close")[0];

// Get the cancel button that closes the modal
var editcancel = document.getElementsByClassName("edit-cancel-btn")[0];

// When the user clicks the button, open the modal
function editClick(btn) {
    editmodal.style.display = "block";
};

editbtn.forEach(function (button) {
    button.addEventListener('click', editClick);
});

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    editmodal.style.display = "none";
}

// When the user clicks on cancel button, close the edit modal
editcancel.onclick = function () {
    editmodal.style.display = "none";
}




