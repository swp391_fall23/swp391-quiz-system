﻿function parseDate(dateString) {
    return moment(dateString, "DD/MM/YYYY");
}

function filterClassesByDate() {
    // Lấy giá trị chọn từ combo box
    var dateFilter = document.getElementById("filterDate").value;

    // Lấy bảng và các hàng
    var table = document.querySelector("table");
    var rows = table.querySelectorAll("tr");
    var today = moment();

    // Lặp qua từng hàng và ẩn hoặc hiển thị dựa trên giá trị chọn
    for (var i = 1; i < rows.length; i++) { // Bắt đầu từ 1 để tránh xử lý hàng tiêu đề
        var startDateCell = rows[i].querySelector("td:nth-child(2)"); 
        var endDateCell = rows[i].querySelector("td:nth-child(3)");
       
        if (startDateCell && endDateCell) {
            var startDate = parseDate(startDateCell.textContent);
            var endDate = parseDate(endDateCell.textContent);
          
          
            if (dateFilter === "past" && endDate < today) {
                rows[i].style.display = "";
            } else if (dateFilter === "ongoing" && startDate <= today && endDate >= today) {
                rows[i].style.display = "";
            } else if (dateFilter === "upcoming" && startDate > today) {
                rows[i].style.display = "";
            } else if (dateFilter === "all") {
                rows[i].style.display = "";
            } else {
                rows[i].style.display = "none";
            }
        }
    }
}

// Lắng nghe sự kiện thay đổi trong combo box
document.getElementById("filterDate").addEventListener("change", filterClassesByDate);

// Gọi hàm lọc ban đầu để hiển thị tất cả các lớp
filterClassesByDate();