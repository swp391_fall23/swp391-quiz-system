function prevQ(current) {
    gotoQuestion(current - 1);
}
function nextQ(current) {
    gotoQuestion(current + 1);
}
function gotoQuestion(current) {
    var el = document.getElementById("q" + current);
    var b = document.getElementById("currentBadge" + current);
    if (el === null) return;
    hideAllQuestions();

    el.classList.remove("d-none");
    b.classList.remove("d-none");
}
function toggleFlag(current) {
    var el = document.getElementById("flag" + current);
    if (el === null) return;

    var isFlagged = false;
    if (el.style.visibility != "hidden") isFlagged = true;
    if (!isFlagged) el.style.visibility = "visible";
    else el.style.visibility = "hidden";
}
function hideAllQuestions() {
    var questionList = document.getElementsByClassName("question");
    for (var q of questionList) {
        if (!q.classList.contains("d-none")) {
            q.classList.add("d-none");
        }
    }
    var questionList = document.getElementsByClassName("current-badge");
    for (var q of questionList) {
        if (!q.classList.contains("d-none")) {
            q.classList.add("d-none");
        }
    }
}
function hideAllFlags() {
    var flags = document.querySelectorAll("sup");
    for (var q of flags) {
        if (!q.classList.contains("d-none")) {
            q.classList.add("d-none");
        }
    }
}
function confirmSubmit() {

}
