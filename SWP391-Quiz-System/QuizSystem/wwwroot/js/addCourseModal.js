// Get the edit modal
var addmodal = document.getElementById("addModal");

// Get the button that opens the edit modal
var addbtn = document.getElementById("add-course-button");

// Get the close element that closes the modal
var span = document.getElementsByClassName("add-close")[0];

// Get the cancel button that closes the modal
var addcancel = document.getElementsByClassName("add-cancel-btn")[0];

// When the user clicks the button, open the modal
addbtn.onclick = function () {
    addmodal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    addmodal.style.display = "none";
}

// When the user clicks on cancel button, close the edit modal
addcancel.onclick = function () {
    addmodal.style.display = "none";
}




