﻿var create_modal = document.getElementById("create-modal");
var modal_overlay = document.getElementById("modal-overlay");

function showModal() {
    create_modal.style.display = "block";
    modal_overlay.style.display = "block";
}

function hideModal() {
    create_modal.style.display = "none";
    modal_overlay.style.display = "none";
}
//

var timeDisplay = document.getElementById("time-display");
function refreshTime() {
    var dateString = new Date().toLocaleString();
    var formattedString = dateString.replace(", ", " - ");
    timeDisplay.innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
    "Now: " + formattedString;
}
setInterval(refreshTime, 1000);

//
var startDate = document.getElementById("start-date");
var endDate = document.getElementById("end-date");
var duration = document.getElementById("duration");
var submitBtn = document.getElementById("submit-btn");
var atm_input = document.createExamForm.numOfAttempts;
var alert = document.getElementById("alert-date");
var alert1 = document.getElementById("alert-dur");
var alert2 = document.getElementById("alert-atp");

function checkAtp() {
    var atm = parseInt(atm_input.value);

    if (atm <= 0) {
        //submitBtn.disabled = true;
        alert2.innerHTML = 'Attempt must be greater than 0!';
        return false;
    } else {
        //submitBtn.disabled = false;
        alert2.innerHTML = '';
        return true;
    }
}

function checkDate() {
    var start = new Date(startDate.value);
    var end = new Date(endDate.value);
    var dur = parseInt(duration.value);
    var q = new Date();
    console.log(dur > 0)
    if (endDate.value == '' && startDate.value == '') {
        if (dur <= 0) {
            //submitBtn.disabled = true;
            alert1.innerHTML = 'Duration must be greater than 0!';
            return false;
        } else {
            //submitBtn.disabled = false;
            alert.innerHTML = '';
            alert1.innerHTML = '';
            return true;
        }
    } else if (endDate.value != '' && startDate.value != '') {
        if (start < q) {
            //submitBtn.disabled = true;
            alert.innerHTML = 'StartDate must be beyond/equal to now!';
            return false;
        } else {
            if (start >= end) {
                //submitBtn.disabled = true;
                alert.innerHTML = 'EndDate must be beyond StartDate!';
                return false;
            } else {
                if (duration.value == '') {
                    //submitBtn.disabled = false;
                    alert.innerHTML = '';
                    alert1.innerHTML = '';
                    return true;
                } else {
                    if (dur <= 0) {
                        //submitBtn.disabled = true;
                        alert.innerHTML = '';
                        alert1.innerHTML = 'Duration must be greater than 0!';
                        return false;
                    } else if (dateDiff(start, end) < dur) {
                        //submitBtn.disabled = true;
                        alert1.innerHTML = 'Duration between StartDate and EndDate must be >= Duration!';
                        return false;
                    } else {
                        //submitBtn.disabled = false;
                        alert.innerHTML = '';
                        alert1.innerHTML = '';
                        return true;
                    }
                }
            }
        }
    } else {
        //submitBtn.disabled = true;
        alert.innerHTML = 'Please input both of the date!';
        return false;
    }
}

//date diff => seconds
function dateDiff(date1, date2) {
    var diff = Math.abs(date2 - date1);
    return Math.ceil(diff / (1000));
}


/////check input text title vs summary cua exam
var titleInput = document.createExamForm.title;
var sumInput = document.createExamForm.summary;
var alertTitle = document.getElementById("alert-title");
var alertSum = document.getElementById("alert-sum");

titleInput.addEventListener("change", checkAll);
sumInput.addEventListener("change", checkAll);

function checkInputTxt() {
    //check title
    if (titleInput.value.trim() === '') {
        alertTitle.innerHTML = "Please do not leave blank or just enter spaces";
        //submitBtn.disabled = true;
    } else {
        alertTitle.innerHTML = "";
        //submitBtn.disabled = false;
    }
    //check summary
    if (sumInput.value.trim() === '') {
        alertSum.innerHTML = "Please do not leave blank or just enter spaces";
        //submitBtn.disabled = true;
    } else {
        alertSum.innerHTML = "";
        //submitBtn.disabled = false;
    }

    if (titleInput.value.trim() === '' || sumInput.value.trim() === '') {
        return false;
    } else {
        return true;
    }
}

function checkAll() {
    if (checkDate() == true && checkInputTxt() == true) {
        submitBtn.disabled = false;
    } else {
        submitBtn.disabled = true;
    }
}

startDate.addEventListener("change", checkAll);
endDate.addEventListener("change", checkAll);
duration.addEventListener("change", checkAll);

/////
var curClassName = document.querySelector(".current-class-name");
var listClass = document.querySelector(".current-list-class");
curClassName.addEventListener('click', function () {
    if (listClass.style.display == "none") {
        listClass.style.display = "block";
    } else if (listClass.style.display == "block") {
        listClass.style.display = "none";
    } else {
        listClass.style.display = "block";
    }
})