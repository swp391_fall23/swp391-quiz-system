﻿using Microsoft.AspNetCore.Authorization;
using QuizSystem.Models;
using QuizSystem.Repositories;

namespace QuizSystem.Policies {
    public class RoleBasedAuthorizationHandler : AuthorizationHandler<RoleAuthorizeRequirement> {
        UserRepository userRepository;
        HttpContext HttpContext;
        public RoleBasedAuthorizationHandler(IHttpContextAccessor _httpContext, OnlineQuizContext _context) {
            userRepository = new UserRepository(_context);
            HttpContext = _httpContext?.HttpContext ?? throw new Exception();
        }

        protected override async Task<Task> HandleRequirementAsync(AuthorizationHandlerContext context, RoleAuthorizeRequirement requirement) {
            if (!context.User.Identities.Any()) {
                context.Fail();
                return Task.CompletedTask;
            }

            try {
                string? a = HttpContext.Session.GetString("account");

                User? u = null;
                if (a != null) {
                    u = await userRepository.Find(a);
                }

                if (!u?.IsActive ?? false) throw new Exception();

                if (!requirement.Roles.Any(e => e.Equals(u?.Role?.Role1 ?? "", StringComparison.InvariantCultureIgnoreCase))) throw new Exception();
            }
            catch {
                context.Fail();
                return Task.CompletedTask;
            }

            context.Succeed(requirement);
            return Task.CompletedTask;
        }
    }
    public class RoleAuthorizeRequirement : IAuthorizationRequirement {
        public RoleAuthorizeRequirement(params string[] roles) {
            Roles = roles.ToList();
        }
        public List<string> Roles { get; }
    }
}
