using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using QuizSystem.Models;

namespace QuizSystem.Pages.Courses.Lecturer
{
    public class InChargeCoursesModel : PageModel
    {
        OnlineQuizContext onlineQuizContext = new OnlineQuizContext();
        public List<Course> courseList { get; set; }
        public String account { get; set; }

        private void startUp()
        {
            account = HttpContext.Session.GetString("account");

            //lay ra tat ca class ma lecturer dong vai tro la lecturer hoac coworker
            List<Class> classList = onlineQuizContext.Classes.Where(c => c.LecturerAccount == account || c.CoWorker == account).ToList();
            List<int> courseIDListDup = new List<int>();
            List<int> courseIDList = new List<int>();
            courseList = new List<Course>();

            //TH tim thay class => ko co class nao thi courseList ko co phan tu nao
            if(classList != null && classList.Count > 0)
            {
                foreach (Class c in classList)
                {
                    courseIDListDup.Add((int)c.CourseId);
                }

                courseIDList = courseIDListDup.Distinct().ToList();

                foreach (int courseID in courseIDList)
                {
                    courseList.Add(onlineQuizContext.Courses.SingleOrDefault(c => c.CourseId == courseID));
                }
            }
        }
        public IActionResult OnGet()
        {
            if (!User.Identity.IsAuthenticated || HttpContext.Session.GetString("role") != "3")
            {
                return Redirect("~/Login/Login");
            }
            else
            {
                startUp();
                return Page();
            }
        }

        public IActionResult OnPostSearch(string code)
        {
            startUp();
            if (!(code==null || code.Length == 0) && courseList != null && courseList.Count > 0)
            {
                for (int i = 0; i < courseList.Count; i++)
                {
                    if (!courseList[i].CourseCode.ToLower().Contains(code.ToLower()))
                    {
                        courseList.Remove(courseList[i]);
                        i--;
                    }
                }
            }
            
            return Page();
        }
    }
}
