using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;

namespace QuizSystem.Pages.Courses
{
    public class ManageCoursesModel : PageModel
    {

        [BindProperty]
        public int CourseId { get; set; }

        [BindProperty]
        public string CourseCode { get; set; }

        [BindProperty]
        public string CourseName { get; set; }

        [BindProperty]
        public string Description { get; set; }


        OnlineQuizContext onlineQuizContext = new OnlineQuizContext();
        [BindProperty]
        public List<Course> courses { get; set; }

        [BindProperty]
        public Course Course { get; set; }


        public void OnGet()
        {
            courses = onlineQuizContext.Courses.ToList();
        }


        private bool IsValidated(string input)
        {
            return !string.IsNullOrEmpty(input) && Regex.IsMatch(input, @"^[A-Za-z0-9.-]+$");
        }

        public void OnPostCreate(String coursecode, String coursename, String coursedes)
        {
            // check existed course code
            Course c = onlineQuizContext.Courses.SingleOrDefault(c => c.CourseCode == coursecode);
            Course newcourse = new Course();
            if (!IsValidated(coursecode))
            {
                ViewData["ErrorMessage"] = "Course code must be only letters, numbers and (.), (-) characters";
            }
            else
            if (c == null)
            {

                newcourse.CourseCode = coursecode;
                newcourse.CourseName = coursename;
                newcourse.Description = coursedes;
                onlineQuizContext.Courses.Add(newcourse);
                onlineQuizContext.SaveChanges();
                ViewData["myRedirect"] = "window.location = '" + "/Courses/ManageCourses" + "'";

            }
            else
            {
                ViewData["ErrorMessage"] = "Course code already exists.";
            }

        }

        public void OnPostDelete(String coursecode)
        {
            //code delete
            Course c = onlineQuizContext.Courses.SingleOrDefault(c => c.CourseCode == coursecode);
            if (c != null)
            {
                onlineQuizContext.Courses.Remove(c);
                onlineQuizContext.SaveChanges();

            }
            ViewData["myRedirect"] = "window.location = '" + "/Courses/ManageCourses" + "'";
        }


        public void OnPostEdit(String initialCourseCode, String coursecode, String coursename, String coursedes)
        {
            
            Course c = onlineQuizContext.Courses.FirstOrDefault(c => c.CourseCode.Equals(initialCourseCode));
                if (c != null)
                {
                    c.CourseCode = coursecode;
                    c.CourseName = coursename;
                    c.Description = coursedes;
                    onlineQuizContext.SaveChanges();
                    
                    ViewData["myRedirect"] = "window.location = '" + "/Courses/ManageCourses" + "'";
                }
            
            
        }

    }
}
