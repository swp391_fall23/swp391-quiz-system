using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;

namespace QuizSystem.Pages.Exams
{
    public class StudentQuestionLogModel : PageModel
    {
        OnlineQuizContext OnlineQuizContext = new OnlineQuizContext();
        [BindProperty]
        public QuestionExam question { get; set; }
        [BindProperty]
        public TakeExam attempt { get; set; }
        [BindProperty]
        public List<QuestionExamAnswer> answers { get; set; }
        [BindProperty]
        public List<TakeAnswer> takeAnswers { get; set; }
        [BindProperty]
        public decimal score { get; set; }

        public ActionResult OnGet(int recordId, int quesId)
        {
            score = 0;
            question = OnlineQuizContext.QuestionExams.SingleOrDefault(q => q.QuesId == quesId);
            attempt = OnlineQuizContext.TakeExams.SingleOrDefault(t => t.TakeExamId == recordId);

            string account = HttpContext.Session.GetString("account");
            string role = HttpContext.Session.GetString("role");
            if(question == null)
            {
                return RedirectToPage("/Courses/Lecturer/InChargeCourses");
            }
            Exam exam = OnlineQuizContext.Exams.Include(e=>e.Class).SingleOrDefault(e => e.ExamId == question.ExamId);
            if (account == null || role != "3")
            {
                return RedirectToPage("/Login/Login");
            }

            if (exam == null || (exam.Class.LecturerAccount != account && exam.Class.CoWorker != account))
            {
                return RedirectToPage("/Courses/Lecturer/InChargeCourses");
            }
            
            if(question !=null && attempt != null)
            {
                answers = OnlineQuizContext.QuestionExamAnswers.Where(a => a.QuesId == question.QuesId).ToList();
                takeAnswers = OnlineQuizContext.TakeAnswers.Include(t=>t.Answer).Where(t => t.TakeExamId == attempt.TakeExamId && t.QuesId == question.QuesId).ToList();
                if(takeAnswers != null && takeAnswers.Count > 0)
                {
                    foreach(TakeAnswer a in takeAnswers)
                    {
                        if(question.Type == true)
                        {
                            if((bool)a.Answer.Correct)
                            {
                                score += (decimal)(a.Answer.Percent * question.Score) / 100;
                            }
                            else
                            {
                                score = 0;
                                break;
                            }
                        }
                        else
                        {
                            score = (bool)a.Answer.Correct? (decimal)question.Score : 0;
                        }
                    }
                }
                return Page();
            }
            else
            {
                return RedirectToPage("/Courses/Lecturer/InChargeCourses");
            }
        }
    }
}
