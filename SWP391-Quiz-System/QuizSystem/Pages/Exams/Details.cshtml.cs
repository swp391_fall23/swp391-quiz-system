using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;

namespace QuizSystem.Pages.Exams
{
    public class DetailsModel : PageModel
    {
        OnlineQuizContext OnlineQuizContext = new OnlineQuizContext();
        [BindProperty]
        public Class Class { get;set; }
        //[BindProperty]
        //public Course course { get; set; }
        [BindProperty]
        public Exam exam { get;set; }
        [BindProperty]
        public String account { get; set; }
        public String message = "";
        public int numOfTaken = 0;
        public List<QuestionExam> questionList { get;set; }
        public List<List<QuestionExamAnswer>> answerList { get; set; }

        private bool checkLoginLecturer()
        {
            account = HttpContext.Session.GetString("account");
            string role = HttpContext.Session.GetString("role");
            //TH chua login
            if (account == null)
            {
                return false;
            }
            else
            {
                //TH login nhung ko phai tk lecturer
                if (role == null || role != "3")
                {
                    return false;
                }
            }
            return true;
        }
        private ActionResult startUp(int examID, string msg)
        {
            if (!checkLoginLecturer())
            {
                //redirect sang trang login
                return RedirectToPage("/Login/Login");
            }


            if (msg == "notAllowed") { message = "You are not allowed to edit/delete this exam!!"; }
            account = HttpContext.Session.GetString("account");
            numOfTaken = (OnlineQuizContext.TakeExams.Where(t => t.ExamId == examID).ToList()).Count;
            exam = OnlineQuizContext.Exams.Include(e=>e.Class).SingleOrDefault(e => e.ExamId == examID);
            if(exam != null && (exam.Class.LecturerAccount == account || exam.Class.CoWorker == account))
            {
                Class = OnlineQuizContext.Classes.Include(x => x.Course).SingleOrDefault(c => c.ClassId == exam.ClassId);
                //course = OnlineQuizContext.Courses.SingleOrDefault(c => c.CourseId == Class.Course.CourseId);
                questionList = OnlineQuizContext.QuestionExams.Where(q => q.ExamId == examID).ToList();

                answerList = new List<List<QuestionExamAnswer>>();

                if(questionList != null && questionList.Count > 0)
                {
                    foreach (QuestionExam q in questionList)
                    {
                        answerList.Add(OnlineQuizContext.QuestionExamAnswers.Where(ans => ans.QuesId == q.QuesId).ToList());
                    }
                }

                return Page();
            }
            else
            {
                //redirect sang incharge
                return RedirectToPage("/Courses/Lecturer/InChargeCourses");
            }
        }
        public ActionResult OnGet(int examID, string? msg)
        {
            return startUp(examID, msg==null? "":msg); 
        }

        public ActionResult OnPostDelete(string examID_raw, string classID)
        {
            int examID = Convert.ToInt32(examID_raw);

            Exam e = OnlineQuizContext.Exams.Include(e => e.Class).SingleOrDefault(e => e.ExamId == examID);

            if (HttpContext.Session.GetString("account") != e.LecturerAccount)
            {
                return startUp(examID, "notAllowed");
            }
            else
            {
                if (e != null)
                {
                    int count  = OnlineQuizContext.TakeExams.Where(t=>t.ExamId == e.ExamId).Count();
                    if (count >0)
                    {
                        return startUp(examID, "notAllowed");
                    }
                    else
                    {
                        try
                        {
                            OnlineQuizContext.Exams.Remove(e);
                            OnlineQuizContext.SaveChanges();
                            ViewData["myRedirect"] = "window.location = '" + "/Exams/Exams?courseID=" + e.Class.CourseId + "&&classID=" + Int32.Parse(classID) + "&&delMsg=success" + "'";
                            List<Exam> examsList = OnlineQuizContext.Exams.ToList();
                            if(examsList == null || examsList.Count == 0)
                            {
                                return startUp(examID, "");
                            }
                            else
                            {
                                return startUp(examsList[0].ExamId, "");
                            }
                        }
                        catch(Exception exception)
                        {
                            ViewData["myRedirect"] = "window.location = '" + "/Exams/Exams?courseID=" + e.Class.CourseId + "&&classID=" + Int32.Parse(classID) + "&&delMsg=error" + "'";
                            return startUp(examID, "");
                        }
                    }
                }
                else
                {
                    return RedirectToPage("/Courses/Lecturer/InChargeCourses");
                }
            }
        }

        public void OnPostEdit(string examID, string title, string summary, 
            DateTime startDate, DateTime endDate, int dur, int numOfAttempts)
        {
            try
            {
                int id = Int32.Parse(examID);
                Exam e = OnlineQuizContext.Exams.SingleOrDefault(e => e.ExamId == id);

                if (HttpContext.Session.GetString("account") != e.LecturerAccount)
                {
                    startUp(Int32.Parse(examID), "notAllowed");
                }
                else
                {
                    if (e != null)
                    {
                        e.Title = title == null ? title : title.Trim();
                        e.Summary = summary == null ? summary : summary.Trim();
                        e.StartDate = startDate;
                        e.EndDate = endDate;
                        e.Timer = dur;
                        e.TakingTimes = numOfAttempts;

                        OnlineQuizContext.Entry<Exam>(e).State =
                            Microsoft.EntityFrameworkCore.EntityState.Modified;
                        OnlineQuizContext.SaveChanges();
                    }
                    TempData["editSuccess"] = "Edit exam information successful!";
                    startUp(Int32.Parse(examID), "");
                }
            }
            catch(Exception e)
            {
                TempData["editError"] = "Edit exam information failed!";
                startUp(Int32.Parse(examID), "");
            }
        }

        public void OnPostAllow(string examID)
        {
            int id = Int32.Parse(examID);
            Exam e = OnlineQuizContext.Exams.SingleOrDefault(e => e.ExamId == id);

            if(e.Permission == true)
            {
                e.Permission = false;
            }
            else { e.Permission = true; }

            OnlineQuizContext.Entry<Exam>(e).State =
                        Microsoft.EntityFrameworkCore.EntityState.Modified;
            OnlineQuizContext.SaveChanges();

            startUp(Int32.Parse(examID), "");
        }
    }
}
