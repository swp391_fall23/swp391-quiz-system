using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;

namespace QuizSystem.Pages.Exams
{
    public class ExamsModel : PageModel
    {
        OnlineQuizContext onlineQuizContext = new OnlineQuizContext();
        [BindProperty]
        public Course course { get; set; }
        [BindProperty]
        public List<Exam> Exams { get; set; }
        [BindProperty]
        public List<Class> Classes { get; set; }
        [BindProperty]
        public Class Class { get; set; }
        [BindProperty]
        public String account { get; set; }
        [BindProperty]
        public int numOfStu { get; set; }

        private ActionResult startUp(int courseID, int classID)
        {
            numOfStu = 0;
            account = HttpContext.Session.GetString("account");
            string role = HttpContext.Session.GetString("role");
            //TH chua login
            if (account == null)
            {
                return RedirectToPage("/Login/Login");
            }
            else
            {
                //TH login nhung ko phai tk lecturer
                if (role == null || role != "3")
                {
                    return RedirectToPage("/Login/Login");
                }
            }

            Classes = onlineQuizContext.Classes.
                Where(c => c.CourseId == courseID && (c.LecturerAccount == account || c.CoWorker == account))
                .ToList();

            //TH lecturer ko co class nao thuoc course nay hoac course ID truyen vao ko tim thay
            if (Classes == null || Classes.Count == 0)
            {
                return RedirectToPage("/Courses/Lecturer/InChargeCourses");
            }
            else
            {
                if (classID == 0)
                {
                    Class = Classes[0];
                }
                else
                {
                    Class = onlineQuizContext.Classes.SingleOrDefault(c => c.ClassId == classID && c.CourseId == courseID);
                }

                //TH ClassID truyen vao sai => chon class dau tien 
                if(Class == null)
                {
                    return RedirectToPage("/Exams/Exams", new {courseID = courseID, classID = Classes[0].ClassId });
                }

                course = onlineQuizContext.Courses.
                    SingleOrDefault(c => c.CourseId == courseID && (Class.LecturerAccount == account || Class.CoWorker == account));
                Exams = onlineQuizContext.Exams.Where(e => e.ClassId == Class.ClassId).ToList();

                numOfStu = (onlineQuizContext.TakeClasses.Where(tc => tc.ClassId == Class.ClassId).ToList()).Count;
                return Page();
            }
        }
        public ActionResult OnGet(int courseID, int? classID, string? delMsg)
        {
            if (delMsg != null)
            {
                if(delMsg == "error")
                {
                    TempData["delError"] = "Delete exam failed!";
                }

                if (delMsg == "success")
                {
                    TempData["delSuccess"] = "Delete exam successful!";
                }
            }

            if (classID == null || classID == 0)
            {
                return startUp(courseID, 0);
            }
            else
            {
                return startUp(courseID, (int)classID);
            }
        }

        public ActionResult OnPost(string lecturer,string courseID,string classID,
            string title, string summary, DateTime startDate, DateTime endDate, int dur, int numOfAttempts)
        {
            try
            {
                Exam e = new Exam();
                e.LecturerAccount = lecturer;
                e.Title = title == null ? title : title.Trim();
                e.Summary = summary == null? summary : summary.Trim();
                e.StartDate = startDate;
                e.EndDate = endDate;
                e.Timer = dur;
                e.TakingTimes = numOfAttempts;
                e.ClassId = Int32.Parse(classID);
                e.Score = 0;
                e.Permission = false;

                onlineQuizContext.Exams.Add(e);
                onlineQuizContext.SaveChanges();

                TempData["success"] = "Create exam successful!";
            }
            catch(Exception e)
            {
                Console.Out.WriteLine(e.Message);
                TempData["error"] = "Create exam failed!";
                //throw new Exception(e.Message);
                return startUp(Int32.Parse(courseID), Int32.Parse(classID));
            }

            return startUp(Int32.Parse(courseID), Int32.Parse(classID));
            //ViewData["myRedirect"] = "window.location = '" + "/Exams/Exams?courseID=" + Int32.Parse(courseID) + "&&" +"classID=" + Int32.Parse(classID) + "'";
        }
    }
}
