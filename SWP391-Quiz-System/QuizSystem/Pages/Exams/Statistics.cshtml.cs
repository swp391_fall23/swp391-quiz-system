using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Primitives;
using QuizSystem.Models;

namespace QuizSystem.Pages.Exams
{
    public class StatisticsModel : PageModel
    {
        OnlineQuizContext OnlineQuizContext = new OnlineQuizContext();
        [BindProperty]
        public Exam exam { get; set; }
        [BindProperty]
        public Class Class { get; set; }
        [BindProperty]
        public List<QuestionExam> questionList { get; set; }
        [BindProperty]
        public List<List<TakeExam>> AttemptList { get; set; }
        [BindProperty]
        public List<List<List<decimal>>> scoreList { get; set; }
        [BindProperty]
        public List<int> scoreStatistics { get; set; }


        private ActionResult startUp(int examID)
        {
            exam = OnlineQuizContext.Exams.Include(e=>e.Class).SingleOrDefault(e => e.ExamId == examID);

            string account = HttpContext.Session.GetString("account");
            string role = HttpContext.Session.GetString("role");
            if (account == null || role != "3")
            {
                return RedirectToPage("/Login/Login");
            }

            if(exam == null || (exam.Class.LecturerAccount != account && exam.Class.CoWorker != account))
            {
                return RedirectToPage("/Courses/Lecturer/InChargeCourses");
            }
            questionList = OnlineQuizContext.QuestionExams.Where(qe => qe.ExamId == exam.ExamId).ToList();

            Class = OnlineQuizContext.Classes.Include(c => c.Course).SingleOrDefault(c => c.ClassId == exam.ClassId);

            if (questionList == null || questionList.Count == 0)
            {
                return Page();
            }

            List<string> studentAcc = new List<string>();
            List<TakeExam> takeExamList = OnlineQuizContext.TakeExams.Where(t => t.ExamId == exam.ExamId).ToList();
            if(takeExamList != null && takeExamList.Count > 0)
            {
                foreach (TakeExam takeExam in takeExamList)
                {
                    studentAcc.Add(takeExam.StudentAccount);
                }

				studentAcc = studentAcc.Distinct().ToList();
				studentAcc.Sort();

				AttemptList = new List<List<TakeExam>>();

                foreach (string stuAcc in studentAcc)
                {
                    List<TakeExam> attempts = OnlineQuizContext.TakeExams.Include("TakeAnswers").Where(t => t.StudentAccount == stuAcc && t.ExamId == exam.ExamId).ToList();
                    TakeExam highestAttempt = attempts.MaxBy(a => a.Score);
                    int index = attempts.IndexOf(highestAttempt);
                    attempts.RemoveAt(index);
                    attempts.Insert(0, highestAttempt);

					AttemptList.Add(attempts);
				}

                //tinh diem
                decimal questionScore = 0;
                List<TakeAnswer> tempList;
                List<QuestionExam> questionList = OnlineQuizContext.QuestionExams.Where(q => q.ExamId == exam.ExamId).ToList();
                int questionTrcDoID = questionList[0].QuesId;
                QuestionExam curQuestion;
                QuestionExamAnswer curQuestionAnswer;
                bool tinhDiemOrNot = true;
                List<int> quesIDList = new List<int>(questionList.Count);
                Dictionary<int, decimal> dict2;
                List<decimal> list4;
                foreach (QuestionExam questionExam in questionList)
                {
                    quesIDList.Add(questionExam.QuesId);
                }
                List<int> quesIDList1;

				scoreList = new List<List<List<decimal>>>();
				for (int i = 0; i < AttemptList.Count; i++) {
					List<List<decimal>> attemptsOfOne = new List<List<decimal>>();
					for (int j = 0; j < AttemptList[i].Count; j++) {
						List<decimal> oneAttemptOfOne = new List<decimal>();
						quesIDList1 = new List<int>();
						for (int k = 0; k < AttemptList[i][j].TakeAnswers.Count; k++) {
							//cham diem: lay ra take answer => lay ra answer theo answer ID(percent) => lay ra score cua question => tinh ra diem
							tempList = AttemptList[i][j].TakeAnswers.ToList();
							curQuestion = OnlineQuizContext.QuestionExams.SingleOrDefault(q => q.QuesId == tempList[k].QuesId);
							curQuestionAnswer = OnlineQuizContext.QuestionExamAnswers.SingleOrDefault(a => a.AnswerId == tempList[k].AnswerId);
							questionScore = (decimal)curQuestion.Score;
							//add diem vao list
							if (curQuestion.Type == true) {
								if (k != 0) {
									if (questionTrcDoID != curQuestion.QuesId) {
										tinhDiemOrNot = true;
									}
									if (curQuestionAnswer.Percent == 0) {
										tinhDiemOrNot = false;
									}

                                    if (questionTrcDoID != curQuestion.QuesId)
                                    {
                                        oneAttemptOfOne.Add((tinhDiemOrNot ? Math.Round((decimal)(questionScore * curQuestionAnswer.Percent / 100), 2) : 0));
                                    }
                                    else
                                    {
                                        oneAttemptOfOne[oneAttemptOfOne.Count - 1] = tinhDiemOrNot ?
                                            (oneAttemptOfOne[oneAttemptOfOne.Count - 1] + Math.Round((decimal)(questionScore * curQuestionAnswer.Percent / 100), 2)) : 0;
                                    }
                                }
                                else
                                {
                                    if (curQuestionAnswer.Percent == 0)
                                    {
                                        tinhDiemOrNot = false;
                                    }
                                    else
                                    {
                                        tinhDiemOrNot = true;
                                    }
                                    oneAttemptOfOne.Add((tinhDiemOrNot ? Math.Round((decimal)(questionScore * curQuestionAnswer.Percent / 100), 2) : 0));
                                }
                            }
                            else
                            {
                                if ((bool)curQuestionAnswer.Correct)
                                {
                                    oneAttemptOfOne.Add(Math.Round((decimal)(questionScore), 2));
                                }
                                else
                                {
                                    oneAttemptOfOne.Add(0);
                                }
                            }
                            quesIDList1.Add(curQuestion.QuesId);
                            questionTrcDoID = curQuestion.QuesId;
                        }

						quesIDList1 = quesIDList1.Distinct().ToList();
						list4 = new List<decimal>();

						if (quesIDList.Count > quesIDList1.Count) {
							dict2 = new Dictionary<int, decimal>();
							for (int a = 0; a < quesIDList1.Count; a++) {
								dict2[quesIDList1[a]] = oneAttemptOfOne[a];
							}

							foreach (int item in quesIDList) {
								if (dict2.ContainsKey(item)) {
									list4.Add(dict2[item]);
								}
								else {
									list4.Add(0);
								}
							}
							attemptsOfOne.Add(list4);
						}
						else {
							attemptsOfOne.Add(oneAttemptOfOne);
						}
					}
					scoreList.Add(attemptsOfOne);
				}

                // tinh ra highest attempt cua tung thang student roi tinh tren he so 10
                // sau khi tinh ra thi sap xep vao cac list tuong ung voi so diem [0-2), [2-4), [4-6), [6-8), [8-10]
                scoreStatistics = new List<int>(5) { 0, 0, 0, 0, 0 };
                decimal z;
                foreach (List<TakeExam> t in AttemptList)
                {
                    z = (decimal)(t[0].Score * 10 / exam.Score);
                    if (z >= 0 && z < 2) { scoreStatistics[0] += 1; }
                    else if (z >= 2 && z < 4) { scoreStatistics[1] += 1; }
                    else if (z >= 4 && z < 6) { scoreStatistics[2] += 1; }
                    else if (z >= 5 && z < 8) { scoreStatistics[3] += 1; }
                    else if (z >= 8 && z <= 10) { scoreStatistics[4] += 1; }
                }
            }

            return Page();
        }

		public IActionResult OnGet(int examID) {
			try {
				return startUp(examID);
			}
			catch {
				var lastPage = "../";
				var referer = Request.Headers["Referer"];
				if (!StringValues.IsNullOrEmpty(referer)) lastPage = referer.Last() ?? "../";

				return Redirect(lastPage);
			}
			return Page();
		}
	}
}
