using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;

namespace QuizSystem.Pages.Dashboard
{
    public class StudentDashboardModel : PageModel
    {

        public IList<TakeClass> ClassList { get; set; } = default!;

        public IActionResult OnGet()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToPage("/Login/Login");
            }
            string studentAcc = HttpContext.Session.GetString("account");
            var mySystem = new OnlineQuizContext();
            Student s = mySystem.Students.FirstOrDefault(x => x.Account == studentAcc);
            List<Class> Classes = new List<Class>();
            List<TakeClass> takeClasses = new List<TakeClass>();
            ClassList = mySystem.TakeClasses.Include(x=>x.Class.Course).Where(x=>x.StudentAccount == studentAcc && x.Class.StartDate <= DateTime.Today && x.Class.EndDate >= DateTime.Today).ToList();

            return Page();
        }
    }
}
