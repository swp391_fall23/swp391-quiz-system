using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;
using QuizSystem.Models.ExamQ;

namespace QuizSystem.Pages.Questions
{
    public class ImportQuestionFromBankModel : PageModel
    {
        OnlineQuizContext OnlineQuizContext = new OnlineQuizContext();
        [BindProperty]
        public Exam exam { get; set; }
        [BindProperty]
        public Course course { get; set; }
        [BindProperty]
        public List<QuestionInBank> quesList { get; set; }
        [BindProperty]
        public IList<SelectListItem> quesListSelect { get; set; }

        private ActionResult startUp(int examID)
        {
            exam = OnlineQuizContext.Exams.Include(e => e.Class).SingleOrDefault(e => e.ExamId == examID);

            string account = HttpContext.Session.GetString("account");
            string role = HttpContext.Session.GetString("role");

            if (account == null || role != "3")
            {
                return RedirectToPage("/Login/Login");
            }

            if (exam == null)
            {
                return RedirectToPage("/Courses/Lecturer/InChargeCourses");
            }

            if (exam == null || (exam.Class.LecturerAccount != account && exam.Class.CoWorker != account))
            {
                return RedirectToPage("/Courses/Lecturer/InChargeCourses");
            }
            course = OnlineQuizContext.Courses.SingleOrDefault(c => c.CourseId == exam.Class.CourseId);

            quesList = OnlineQuizContext.QuestionInBanks.Include(q => q.QuestionBankAnswers).Where(q => q.CourseId == course.CourseId).ToList();
            quesListSelect = quesList.ToList<QuestionInBank>().Select(m => new SelectListItem { Text = m.Content, Value = m.QuesId.ToString() }).ToList<SelectListItem>();
            return Page();
        }
        public ActionResult OnGet(int examID)
        {
           return startUp(examID);
        }

        public void OnPostSearch(string examID, string filter)
        {
            exam = OnlineQuizContext.Exams.Include(e => e.Class).SingleOrDefault(e => e.ExamId == Int32.Parse(examID));
            course = OnlineQuizContext.Courses.SingleOrDefault(c => c.CourseId == exam.Class.CourseId);
            if (filter == "0")
            {
                quesList = OnlineQuizContext.QuestionInBanks.Include(q => q.QuestionBankAnswers).Where(q => q.CourseId == course.CourseId).ToList();
            } else if (filter == "1") {

                quesList = OnlineQuizContext.QuestionInBanks.Include(q => q.QuestionBankAnswers).Where(q => q.CourseId == course.CourseId).ToList();
                for(int i = 0; i < quesList.Count; i++)
                {
                    if (quesList[i].Type == false)
                    {
                        quesList.RemoveAt(i);
                        i--;
                    }
                }
            }
            else if(filter == "2") {
                quesList = OnlineQuizContext.QuestionInBanks.Include(q => q.QuestionBankAnswers).Where(q => q.CourseId == course.CourseId).ToList();
                for (int i = 0; i < quesList.Count; i++)
                {
                    if (quesList[i].Type == true)
                    {
                        quesList.RemoveAt(i);
                        i--;
                    }
                }
            }
          
            quesListSelect = quesList.ToList<QuestionInBank>().Select(m => new SelectListItem { Text = m.Content, Value = m.QuesId.ToString() }).ToList <SelectListItem>();

        }

        public void OnPost(string __examID)
        {
            Exam target_exam = OnlineQuizContext.Exams.Include(e => e.Class).SingleOrDefault(e => e.ExamId == Int32.Parse(__examID));

            foreach (SelectListItem q in quesListSelect)
            {
                if (q.Selected)
                {
                    QuestionExam q0 = new QuestionExam();
                    QuestionInBank q1 = OnlineQuizContext.QuestionInBanks.
                        Include(ques => ques.QuestionBankAnswers).
                        SingleOrDefault(ques => ques.QuesId == Int32.Parse(q.Value));

                    q0.Content = q1.Content;
                    q0.Type = q1.Type;
                    q0.ExamId = Int32.Parse(__examID);
                    q0.Score = 1;

                    target_exam.Score += 1;
                    OnlineQuizContext.Entry<Exam>(target_exam).State =
                       Microsoft.EntityFrameworkCore.EntityState.Modified;

                    OnlineQuizContext.QuestionExams.Add(q0);
                    OnlineQuizContext.SaveChanges();

                    int q0ID = (int)OnlineQuizContext.QuestionExams.Where(q => q.ExamId == Int32.Parse(__examID)).ToList().LastOrDefault().QuesId;

                    foreach (QuestionBankAnswer qba in q1.QuestionBankAnswers)
                    {
                        QuestionExamAnswer ans = new QuestionExamAnswer();
                        ans.QuesId = q0ID;
                        ans.Content = qba.Content;
                        ans.Correct = qba.Correct;
                        ans.Percent = qba.Percent;
                        OnlineQuizContext.QuestionExamAnswers.Add(ans);
                        OnlineQuizContext.SaveChanges();
                    }
                }
            }
            startUp(target_exam.ExamId);
            ViewData["myRedirect"] = "window.location = '" + "/Questions/Questions?examID=" + Int32.Parse(__examID) + "'";
        }
    }
}
