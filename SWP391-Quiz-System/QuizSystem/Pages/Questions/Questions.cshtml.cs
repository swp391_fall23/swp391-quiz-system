﻿using Humanizer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using OfficeOpenXml;
using QuizSystem.Models;
using QuizSystem.Models.ExamQ;
using System;

namespace QuizSystem.Pages.Questions
{
    public class QuestionsModel : PageModel
    {
        OnlineQuizContext OnlineQuizContext = new OnlineQuizContext();
        [BindProperty]
        public Exam exam { get; set; }
        [BindProperty]
        public Class Class { get; set; }
        public List<QuestionExam> questionList { get; set; }
        public List<List<QuestionExamAnswer>> answerList { get; set; }
        public QuestionExam newQuestion { get; set; }
        public string Alert { get; set; }
        public string Notice { get; set; }

        private ActionResult startUp(int examID)
        {
            exam = OnlineQuizContext.Exams.SingleOrDefault(e => e.ExamId == examID);
            if(exam == null)
            {
                return RedirectToPage("/Courses/Lecturer/InChargeCourses");
            }
            questionList = OnlineQuizContext.QuestionExams.Where(q => q.ExamId == examID).ToList();
            Class = OnlineQuizContext.Classes.Include(c => c.Course).SingleOrDefault(c => c.ClassId == exam.ClassId);

            answerList = new List<List<QuestionExamAnswer>>();

            if(questionList != null)
            {
                foreach (QuestionExam q in questionList)
                {
                    answerList.Add(OnlineQuizContext.QuestionExamAnswers.Where(ans => ans.QuesId == q.QuesId).ToList());
                }
            }

            int numOfTaken = (OnlineQuizContext.TakeExams.Where(t => t.ExamId == examID).ToList()).Count;

            if (exam.LecturerAccount != HttpContext.Session.GetString("account") || numOfTaken > 0)
            {
                ViewData["myRedirect"] = "window.location = '" + "/Exams/Details?examID=" + examID +
                    "&&msg=" + "notAllowed" + "'";
            }

            return Page();
        }
        public ActionResult OnGet(int examID)
        { 
            return startUp(examID);
        }

        public void OnPostCreate(string examID,
            string question, string typeSelect, int score, string addToBank,
            List<QuestionExamAnswer> a)
        {
            try
            {
                if (question == null || question.Trim() == "")
                {
                    TempData["CQuestionNull"] = "Create new question failed! Check content of the question!";
                    startUp(Int32.Parse(examID));
                    return;
                }

                QuestionExam q = new QuestionExam();
                q.ExamId = Int32.Parse(examID);
                q.Content = question == null ? question : question.Trim();
                q.Type = typeSelect == "0" ? false : true;
                q.Score = score;

                Exam e = OnlineQuizContext.Exams.SingleOrDefault(x => x.ExamId == Int32.Parse(examID));
                e.Score += score;

                OnlineQuizContext.Entry<Exam>(e).State =
                           Microsoft.EntityFrameworkCore.EntityState.Modified;
                OnlineQuizContext.QuestionExams.Add(q);
                OnlineQuizContext.SaveChanges();

                int quesID = OnlineQuizContext.QuestionExams.Where(q => q.ExamId == Int32.Parse(examID)).ToList().LastOrDefault().QuesId;
                QuestionExam newQuestion = OnlineQuizContext.QuestionExams.SingleOrDefault(q => q.QuesId == quesID);
                for (int i = 0; i < a.Count; i++)
                {
                    QuestionExamAnswer qa = new QuestionExamAnswer();
                    qa.QuesId = quesID;
                    if (a[i].Content == null || a[i].Content.Trim() == "")
                    {
                        OnlineQuizContext.QuestionExams.Remove(newQuestion);
                        OnlineQuizContext.SaveChanges();
                        TempData["CAnswerNull"] = "Create new question failed! Check content of the answers!";
                        startUp(Int32.Parse(examID));
                        return;
                    }
                    qa.Content = a[i].Content == null ? a[i].Content : a[i].Content.Trim();
                    qa.Correct = a[i].Correct;
                    qa.Percent = (bool)q.Type ? (((bool)qa.Correct ? a[i].Percent : 0)) : (((bool)qa.Correct ? 100 : 0));

                    OnlineQuizContext.QuestionExamAnswers.Add(qa);
                    OnlineQuizContext.SaveChanges();
                }

                if (addToBank == "Yes")
                {
                    QuestionInBank questionInBank = new QuestionInBank();
                    questionInBank.Content = newQuestion.Content;
                    questionInBank.Type = newQuestion.Type;
                    questionInBank.CourseId = OnlineQuizContext.Exams.Include(e => e.Class).
                        SingleOrDefault(e => e.ExamId == Int32.Parse(examID)).Class.CourseId;

                    OnlineQuizContext.QuestionInBanks.Add(questionInBank);
                    OnlineQuizContext.SaveChanges();

                    int newQuesInBankID = OnlineQuizContext.QuestionInBanks.Where(q => q.CourseId == questionInBank.CourseId).ToList().LastOrDefault().QuesId;
                    for (int i = 0; i < a.Count; i++)
                    {
                        QuestionBankAnswer qba = new QuestionBankAnswer();
                        qba.QuesId = newQuesInBankID;
                        qba.Content = a[i].Content;
                        qba.Correct = a[i].Correct;
                        qba.Percent = (bool)q.Type ? (((bool)qba.Correct ? a[i].Percent : 0)) : 100;

                        OnlineQuizContext.QuestionBankAnswers.Add(qba);
                        OnlineQuizContext.SaveChanges();
                    }
                }
                TempData["createQSuccess"] = "Create new question successful!";
                startUp(Int32.Parse(examID));
            }
            catch(Exception ex)
            {
                TempData["createQError"] = "Create new question failed! " + ex.Message;
                startUp(Int32.Parse(examID));
            }
        }

        public void OnPostDelete(string quesID, string examID)
        {
            try
            {
                QuestionExam q = OnlineQuizContext.QuestionExams.SingleOrDefault(q => q.QuesId == Int32.Parse(quesID));
                Exam e = OnlineQuizContext.Exams.SingleOrDefault(x => x.ExamId == Int32.Parse(examID));
                e.Score -= q.Score;
                OnlineQuizContext.Entry<Exam>(e).State =
                           Microsoft.EntityFrameworkCore.EntityState.Modified;
                OnlineQuizContext.SaveChanges();

                if (q != null)
                {
                    OnlineQuizContext.QuestionExams.Remove(q);
                    OnlineQuizContext.SaveChanges();
                }
                TempData["delQSuccess"] = "Delete question successful!";
                startUp(Int32.Parse(examID));
            }
            catch(Exception ex)
            {
                TempData["delQError"] = "Delete question failed! " + ex.Message;
                startUp(Int32.Parse(examID));
            }
        }

        public void OnPostEdit(string examID, string quesID,
            string question1, string typeSelect, int score1,
            List<QuestionExamAnswer> ansList)
        {
            try
            {
                if (question1 == null || question1.Trim() == "")
                {
                    TempData["EQuestionNull"] = "Edit question failed! Check content of the question!";
                    startUp(Int32.Parse(examID));
                    return;
                }

                QuestionExam q = OnlineQuizContext.QuestionExams.SingleOrDefault(q => q.QuesId == Int32.Parse(quesID));

                Exam e = OnlineQuizContext.Exams.SingleOrDefault(x => x.ExamId == Int32.Parse(examID));
                e.Score += score1 - q.Score;
                //edit question
                q.Content = question1 == null ? question1 : question1.Trim();
                q.Type = typeSelect == "0" ? false : true;
                q.Score = score1;

                OnlineQuizContext.Entry<Exam>(e).State =
                           Microsoft.EntityFrameworkCore.EntityState.Modified;
                OnlineQuizContext.Entry<QuestionExam>(q).State =
                           Microsoft.EntityFrameworkCore.EntityState.Modified;
                OnlineQuizContext.SaveChanges();

                for (int i = 0; i < ansList.Count; i++)
                {
                    if (ansList[i].Content == null || ansList[i].Content.Trim() == "")
                    {
                        TempData["EAnswerNull"] = "Edit question failed! Check content of the answers!";
                        startUp(Int32.Parse(examID));
                        return;
                    }
                }

                //delete old question answer of question
                OnlineQuizContext.QuestionExamAnswers.RemoveRange(OnlineQuizContext.QuestionExamAnswers.Where(a => a.QuesId == q.QuesId));
                //update new question answer of question
                for (int i = 0; i < ansList.Count; i++)
                {
                    QuestionExamAnswer qea = new QuestionExamAnswer();
                    qea.QuesId = q.QuesId;
                    qea.Content = ansList[i].Content == null ? ansList[i].Content : ansList[i].Content.Trim();
                    qea.Correct = ansList[i].Correct;
                    qea.Percent = (bool)q.Type ? (((bool)qea.Correct ? ansList[i].Percent : 0)) : 100;

                    OnlineQuizContext.QuestionExamAnswers.Add(qea);
                }

                OnlineQuizContext.SaveChanges();
                TempData["editQSuccess"] = "Edit question successful!";
                startUp(Int32.Parse(examID));
            }
            catch(Exception ex)
            {
                TempData["editQError"] = "Edit question failed! " + ex.Message;
                startUp(Int32.Parse(examID));
            }
        }

        public void OnPostImport(IFormFile file, string examID)
        {
            exam = OnlineQuizContext.Exams.SingleOrDefault(e => e.ExamId == Int32.Parse(examID));
            
            if (file != null && file.Length > 0 && IsExcelFileValid(file))
            {
                System.Diagnostics.Contracts.Contract.ContractFailed += (sender, e) => e.SetHandled();

                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

                using (var package = new ExcelPackage(file.OpenReadStream()))
                {
                    var worksheet = package.Workbook.Worksheets[0]; 

                    for (int row = 2; row <= worksheet.Dimension.End.Row; row++)
                    {
                        questionList = OnlineQuizContext.QuestionExams.Where(q => q.ExamId == Int32.Parse(examID)).ToList();
                        var content = worksheet.Cells[row, 1].Text.Trim();
                        bool isDuplicate = false;
                        if (questionList.Count > 0)
                        {
                            foreach (var item in questionList)
                            {
                                if (content.Equals(item.Content))
                                {
                                    isDuplicate = true;
                                }
                            }
                        }
                        if (isDuplicate == false)
                        {
                            var typeText = worksheet.Cells[row, 2].Text;
                            bool type = false;

                            type = Convert.ToBoolean(typeText.Trim());
                            var score = decimal.Parse(worksheet.Cells[row, 3].Text.Trim());

                            var question = new QuestionExam { Content = content, Type = type, Score = score, ExamId = Int32.Parse(examID) };
                            OnlineQuizContext.QuestionExams.Add(question);
                            OnlineQuizContext.SaveChanges();
                            exam.Score += question.Score;
                            OnlineQuizContext.Entry<Exam>(exam).State =
                       Microsoft.EntityFrameworkCore.EntityState.Modified;
                            OnlineQuizContext.SaveChanges();
                            int endColumnForRow = FindLastNonEmptyColumn(worksheet, row);
                            for (int answerIndex = 4; answerIndex <= endColumnForRow; answerIndex++)
                            {
                                var answerContent = worksheet.Cells[row, answerIndex].Text.Trim();
                                var isCorrectText = worksheet.Cells[row, answerIndex + 1].Text;
                                bool isCorrect = false;
                                isCorrect = Convert.ToBoolean(isCorrectText.Trim());

                                var percent = decimal.Parse(worksheet.Cells[row, answerIndex + 2].Text.Trim());

                                var answer = new QuestionExamAnswer
                                {
                                    QuesId = question.QuesId,
                                    Content = answerContent,
                                    Correct = isCorrect,
                                    Percent = percent
                                };

                                OnlineQuizContext.QuestionExamAnswers.Add(answer);

                                answerIndex += 2;
                            }
                        }
                    }
                }
            }
            if (Alert == null && file != null && file.Length > 0)
            {
                OnlineQuizContext.SaveChanges();
                Notice = "Import Successfully!";
            }

            startUp(Int32.Parse(examID));
        }
        bool IsExcelFileValid(IFormFile file)
        {
            string fileName = file.FileName;
            string fileExtension = Path.GetExtension(fileName);

            if (IsExcelFile(fileExtension))
            {
                System.Diagnostics.Contracts.Contract.ContractFailed += (sender, e) => e.SetHandled();

                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

                using (var package = new ExcelPackage(file.OpenReadStream()))
                {
                    var worksheet = package.Workbook.Worksheets[0];

                    for (int row = 2; row <= worksheet.Dimension.End.Row; row++)
                    {
                        decimal totalPercent = 0;
                        int countCorrectAnswer = 0;
                        if (String.IsNullOrEmpty(worksheet.Cells[row, 1].Text) || String.IsNullOrEmpty(worksheet.Cells[row, 2].Text) || String.IsNullOrEmpty(worksheet.Cells[row, 2].Text))
                        {
                            Alert = "\nCheck question " + (row - 1) + ", make sure you input a valid String.";
                        }
                        else
                        {
                            if (!IsPositiveNumber(worksheet.Cells[row, 3].Text))
                            {
                                Alert = "\nCheck question " + (row - 1) + ", make sure you input Score is a positive number.";
                                return false;
                            }
                            var content = worksheet.Cells[row, 1].Text;
                            var typeText = worksheet.Cells[row, 2].Text;
                            if (!(typeText.Trim().ToLower().Equals("false")) && !(typeText.Trim().ToLower().Equals("true")))
                            {
                                Alert += "\nCheck question " + (row - 1) + ", make sure 'IsCorrect' field is fill with 'True' or 'False' as a String.";
                            }
                            var score = decimal.Parse(worksheet.Cells[row, 3].Text);
                            int endColumnForRow = FindLastNonEmptyColumn(worksheet, row);
                            for (int answerIndex = 4; answerIndex <= endColumnForRow; answerIndex++)
                            {
                                if (String.IsNullOrEmpty(worksheet.Cells[row, answerIndex].Text) || String.IsNullOrEmpty(worksheet.Cells[row, answerIndex + 1].Text) || String.IsNullOrEmpty(worksheet.Cells[row, answerIndex + 2].Text))
                                {
                                    Alert = "\nCheck question " + (row - 1) + ", make sure you input a valid String.";
                                }
                                else
                                {
                                    var answerContent = worksheet.Cells[row, answerIndex].Text;
                                    var isCorrectText = worksheet.Cells[row, answerIndex + 1].Text;
                                    if (!(isCorrectText.Trim().ToLower().Equals("false")) && !(isCorrectText.Trim().ToLower().Equals("true")))
                                    {
                                        Alert += "\nCheck question " + (row - 1) + ", make sure 'IsMultipleChoice' field is fill with 'True' or 'False' as a String.";
                                    }
                                    if (!IsNonNegativeNumber(worksheet.Cells[row, answerIndex + 2].Text))
                                    {
                                        Alert = "\nCheck question " + (row - 1) + ", make sure you input Percent is a non negative number.";
                                        return false;
                                    }
                                    var percent = decimal.Parse(worksheet.Cells[row, answerIndex + 2].Text);
                                    if (isCorrectText.Trim().ToLower().Equals("true"))
                                    {
                                        countCorrectAnswer++;
                                        totalPercent += percent;
                                    }
                                    answerIndex += 2;
                                }
                            }
                            if (typeText.Trim().ToLower().Equals("true"))
                            {
                                if (countCorrectAnswer <= 1)
                                {
                                    Alert += "\nCheck question " + (row - 1) + ", make sure Multiple Choice Question has more than 1 correct answer";
                                }
                                if (totalPercent != 100)
                                {
                                    Alert += "\nCheck Question " + (row - 1) + ", make sure total percent of multiple choice question is 100.";
                                }
                            }
                            else
                            {
                                if (countCorrectAnswer < 1)
                                {
                                    Alert += "\nCheck question " + (row - 1) + ", make sure Select One Question has just 1 correct answer.";
                                }
                                if (countCorrectAnswer > 1)
                                {
                                    Alert += "\nCheck question " + (row - 1) + ", make sure Select One Question has just 1 correct answer.";
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                Alert = " File import is not an excel file.";
            }
            
            if (Alert != null)
            {
                return false;
                Notice = null;
            }
            return true;
        }
        public bool IsPositiveNumber(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return false; 
            }

            if (decimal.TryParse(value, out decimal number))
            {
                if (number > 0)
                {
                    return true; 
                }
            }

            return false; 
        }

        public bool IsNonNegativeNumber(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return false;
            }

            if (decimal.TryParse(value, out decimal number))
            {
                if (number >= 0)
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsExcelFile(string fileName)
        {
            string fileExtension = Path.GetExtension(fileName);

            if (fileExtension.Equals(".xlsx", StringComparison.OrdinalIgnoreCase) || fileExtension.Equals(".xls", StringComparison.OrdinalIgnoreCase))
            {
                return true; 
            }

            return false;
        }

        private int FindLastNonEmptyColumn(ExcelWorksheet worksheet, int row)
        {
            int lastColumn = worksheet.Dimension.End.Column;

            for (int col = lastColumn; col >= 1; col--)
            {
                var cellValue = worksheet.Cells[row, col].Text.Trim();

                if (!string.IsNullOrEmpty(cellValue))
                {
                    return col;
                }
            }
            return 1; 
        }
    }
}
