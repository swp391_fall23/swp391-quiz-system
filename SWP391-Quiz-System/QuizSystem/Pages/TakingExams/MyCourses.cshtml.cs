using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;

namespace QuizSystem.Pages.TakingExams
{
    public class MyCoursesModel : PageModel
    {
        public IList<TakeClass> ClassList { get; set; } = default!;

        public IActionResult OnGet()
        {
            if (HttpContext.Session.GetString("role") == null)
            {
                return RedirectToPage("/Login/Login");
            }
            if (HttpContext.Session.GetString("role") != "4")
            {
                return RedirectToPage("Index");
            }
            string studentAcc = HttpContext.Session.GetString("account");
            var mySystem = new OnlineQuizContext();
            Student s = mySystem.Students.FirstOrDefault(x => x.Account == studentAcc);
            List<Class> Classes = new List<Class>();
            List<TakeClass> takeClasses = new List<TakeClass>();
            ClassList = mySystem.TakeClasses.Include(x => x.Class.Course).Where(x => x.StudentAccount == studentAcc && x.Class.StartDate <= DateTime.Today).OrderByDescending(x=>x.Class.EndDate).ToList();

            return Page();
        }

        public IActionResult OnPostSearch(string keyword)
        {
            string studentAcc = HttpContext.Session.GetString("account");
            var mySystem = new OnlineQuizContext();
            Student s = mySystem.Students.FirstOrDefault(x => x.Account == studentAcc);
            List<Class> Classes = new List<Class>();
            List<TakeClass> takeClasses = new List<TakeClass>();
            if (String.IsNullOrEmpty(keyword))
            {
                ClassList = mySystem.TakeClasses.Include(x => x.Class.Course).Where(x => x.StudentAccount == studentAcc && x.Class.StartDate <= DateTime.Today).OrderByDescending(x => x.Class.EndDate).ToList();
            }
            else
            {
                ClassList = mySystem.TakeClasses.Include(x => x.Class.Course)
                .Where(x => x.StudentAccount == studentAcc && x.Class.StartDate <= DateTime.Today && (x.Class.ClassName.Contains(keyword.Trim()) || x.Class.Course.CourseCode.Contains(keyword.Trim())))
                .OrderByDescending(x => x.Class.EndDate).ToList();
            }
            return Page();
        }
    }
}
