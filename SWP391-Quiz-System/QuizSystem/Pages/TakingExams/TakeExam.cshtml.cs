using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;
using QuizSystem.Models.ExamQ;
using QuizSystem.Repositories;
using QuizSystem.Services.Encryption;
using QuizSystem.Services.ExamGradingService;

namespace QuizSystem.Pages.TakingExams {
    public class TakeExamModel : PageModel {
        public int ExamId { get; set; }
        public string? TakeExamId { get; set; }
        public string? ExamTitle { get; set; }
        public long Timer { get; set; }
        public List<EQuestion> Questions { get; set; } = new List<EQuestion>();
        //public Timer Timer { get; set; } = new Timer()
        GradeExam Grade { get; set; }
        TakeExamRepository takeExamRepository { get; set; }
        UserRepository userRepository { get; set; }
        ExamRepository examRepository { get; set; }
        public TakeExamModel(OnlineQuizContext _context) {
            Grade = new GradeExam(_context);
            userRepository = new UserRepository(_context);
            examRepository = new ExamRepository(_context);
            takeExamRepository = new TakeExamRepository(_context);
        }

        public async Task<IActionResult> OnPostAsync(int examID) {
            TempData["Color"] = "danger";
            TempData["Message"] = "Cannot initiate exam";
            //Register new take exam
            var claims = User.Identities.FirstOrDefault()?.Claims.Select(claim => new {
                claim.Issuer,
                claim.OriginalIssuer,
                claim.Type,
                claim.Value
            });
            string? email = claims?.ElementAt(4).Value;

            if (email == null) return RedirectToPage("ExamDetails", new { examId = examID });
            User? u = await userRepository.FindUserByEmail((string)email);
            if (u == null) return RedirectToPage("ExamDetails", new { examId = examID });

            TakeExam tExam = new TakeExam();
            tExam.ExamId = examID;
            tExam.StudentAccount = u.Account;

            //Check if user can create new take exam
            var exam = await examRepository.Find(examID);
            if (exam == null) return RedirectToPage("ExamDetails", new { examId = examID });
            if (await takeExamRepository.UserTakeExamCount(tExam.StudentAccount, examID) >= exam.TakingTimes) {
                TempData["Message"] = "You have ran out of retries for this exam!";
                return RedirectToPage("ExamDetails", new { examId = examID });
            }
            ExamTitle = exam.Title;

            //Get exam information
            tExam.StartDate = DateTime.Now;
            tExam.Status = "In progress";
            await takeExamRepository.Add(tExam);
            var tExamId = (await takeExamRepository.GetStudentLatestOnGoingExam(tExam.StudentAccount, tExam.ExamId))?.TakeExamId;

            if (tExamId == null) return RedirectToPage("ExamDetails", new { examId = examID });

            List<QuestionExam> Ques = exam.QuestionExams.ToList();
            foreach (var question in Ques) {
                var q = new EQuestion();
                q.QuestionId = question.QuesId;
                q.Content = question.Content;
                q.IsSingleChoice = !(bool)question.Type;

                List<EAnswer> answer = new List<EAnswer>();
                foreach (var _answer in question.QuestionExamAnswers) {
                    var a = new EAnswer();
                    a.AnswerId = _answer.AnswerId;
                    a.Content = _answer.Content;
                    answer.Add(a);
                }
                q.Answers = answer;
                Questions.Add(q);

            }

            ExamId = examID;
            Timer = (exam.Timer ?? 0)  * 1000;
            TakeExamId = tExamId.ToString().Encrypt();
            TempData["Color"] = null;
            TempData["Message"] = null;
            return Page();
        }

    }
}
