using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Primitives;
using QuizSystem.Models;

namespace QuizSystem.Pages.TakingExams
{
    public class Quiz_LogModel : PageModel
    {
        public TakeExam TakeExam { get; set; } = default!;
        public Exam Exam { get; set; } = default!;
        public List<QuestionExam> QuestionExams { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id) {
			TempData["Message"] = "Cannot get user data";
			TempData["Color"] = "danger";

			var lastPage = "../";
			var referer = Request.Headers["Referer"];
			if (!StringValues.IsNullOrEmpty(referer)) lastPage = referer.Last() ?? "../";

			var mySystem = new OnlineQuizContext();
            TakeExam? takeExam = mySystem.TakeExams.Include(c=>c.Exam).Include(c=>c.TakeAnswers).FirstOrDefault(c => c.TakeExamId == id);
			if (takeExam == null) return Redirect(lastPage);
			Exam? exam = mySystem.Exams.Include(c=>c.QuestionExams).FirstOrDefault(c => c.ExamId == takeExam.ExamId);
			if (exam == null) return Redirect(lastPage);
			QuestionExams = mySystem.QuestionExams.Include(c => c.QuestionExamAnswers).Where(c => c.ExamId == exam.ExamId).ToList();

            TakeExam = takeExam;
            Exam = exam;

			TempData["Message"] = null;
			TempData["Color"] = null;
			return Page();
        }
    }
}
