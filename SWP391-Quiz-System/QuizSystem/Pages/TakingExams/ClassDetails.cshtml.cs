using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;

namespace QuizSystem.Pages.TakingExams
{
    public class ClassDetailsModel : PageModel
    {
        public Class Class { get; set; } = default!;
        public IList<Exam> Exams { get; set; } = default!;
        public TakeClass TakeClasses { get; set; }

        public ActionResult OnGet(int? id)
        {
            return startUp(id);

        }
        private ActionResult startUp(int? id)
        {
            var mySystem = new OnlineQuizContext();
            Class = mySystem.Classes.Include(x => x.Course)
                .Include(x => x.LecturerAccountNavigation).SingleOrDefault(x => x.ClassId == id);
            string account = HttpContext.Session.GetString("account");
            TakeClasses = mySystem.TakeClasses.FirstOrDefault(q => q.ClassId == id && q.StudentAccount == account);
            if (Class != null && TakeClasses != null)
            {
                Exams = mySystem.Exams.Where(x => x.ClassId == id).ToList();

                return Page();
            }
            else
            {
                return RedirectToPage("./MyCourses");
            }

        }

    }
}
