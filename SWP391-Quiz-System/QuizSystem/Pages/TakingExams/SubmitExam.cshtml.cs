using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using QuizSystem.Models.ExamQ;
using QuizSystem.Models;
using QuizSystem.Repositories;
using QuizSystem.Services.Encryption;
using System.Diagnostics;
using QuizSystem.Services.ExamGradingService;
using QuizSystem.Services.ExtensionMethods;
using Microsoft.Extensions.Primitives;

namespace QuizSystem.Pages.TakingExams
{
    public class SubmitExamModel : PageModel {
        GradeExam Grade { get; set; }
        TakeExamRepository takeExamRepository { get; set; }
        ExamRepository examRepository { get; set; }
        public SubmitExamModel(OnlineQuizContext _context) {
            Grade = new GradeExam(_context);
            takeExamRepository = new TakeExamRepository(_context);
            examRepository = new ExamRepository(_context);
        }

        public IActionResult OnGet() {
            var lastPage = "../";
            var referer = Request.Headers["Referer"];
			if (!StringValues.IsNullOrEmpty(referer)) lastPage = referer.Last() ?? "../";


			return Redirect(lastPage);
        }

        public async Task<IActionResult> OnPostAsync(int __examId, string? __takeExamId) {
            TempData["Color"] = "danger";
            TempData["Message"] = "Cannot retrieve exam data";

            var exam = await examRepository.Find(__examId);
            if (__takeExamId?.ToString().Decrypt() == null) return RedirectToPage("ExamDetails", new { examId = __examId });
            if (!int.TryParse(__takeExamId?.ToString().Decrypt(), out var takeExamId)) return RedirectToPage("ExamDetails", new { examId = __examId });
            if (exam == null) return RedirectToPage("ExamDetails", new { examId = __examId });

            List<EQuestion> questions = new List<EQuestion>();
            foreach (var a in Request.Form.Where(e => !e.Key.StartsWith("__"))) {
                EQuestion question = new EQuestion();
                question.QuestionId = int.Parse(a.Key);
                foreach (var b in a.Value) {
                    question.Answers.Add(new EAnswer() { AnswerId = int.Parse(b.ToString()) });
                }
                questions.Add(question);
            }

            (decimal score, decimal total) score = (0, 0);

            if (int.TryParse(Request.Form["__examId"].FirstOrDefault(), out var result)) {
                if (questions.Count != 0) {
                    score = (await Grade.Grade(questions, result)).ToExamGrade(exam.Score);
                }
                else score.total = await Grade.GetTotalScore(result);
            }
            else return RedirectToPage("ExamDetails", new { examId = __examId });
            TakeExam? tExam = await takeExamRepository.Find(takeExamId);
            if (tExam == null || tExam.ExamId != __examId) return RedirectToPage("ExamDetails", new { examId = __examId });

            tExam.EndDate = DateTime.Now;
            tExam.Score = score.score;
            tExam.Status = "Finished";

            if (tExam.TakeAnswers == null) tExam.TakeAnswers = new List<TakeAnswer>();
            foreach (var q in questions) {
                foreach (var answer in q.Answers) {
                    TakeAnswer a = new TakeAnswer();
                    a.TakeExamId = tExam.TakeExamId;
                    a.QuesId = q.QuestionId;
                    a.AnswerId = answer.AnswerId;

                    tExam.TakeAnswers.Add(a);
                }
            }
            bool permission = exam?.Permission ?? false;
            await takeExamRepository.Update(tExam);

            TempData["Color"] = null;
            TempData["Message"] = null;
            TempData["ExamComplete"] = takeExamId;
            TempData["ExamCompleteView"] = permission;
            TempData["ExamCompleteScore"] = score.score + " out of " + score.total;
            return RedirectToPage("ExamDetails", new { examId = __examId });
        }
    }
}
