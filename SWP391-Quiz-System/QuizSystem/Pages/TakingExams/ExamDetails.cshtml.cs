using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;

namespace QuizSystem.Pages.TakingExams
{
    public class ExamDetailsModel : PageModel
    {
        OnlineQuizContext OnlineQuizContext = new OnlineQuizContext();
        public Class Class { get; set; }
        public Exam exam { get; set; }
        public List<QuestionExam> questionList { get; set; }
        public List<List<QuestionExamAnswer>> answerList { get; set; }
        public List<TakeExam> TakeExams { get; set; }
        public TakeClass TakeClasses { get; set; }

        public ActionResult OnGet(int? examID)
        {
            return startUp(examID);

        }
        private ActionResult startUp(int? examID)
        {
            exam = OnlineQuizContext.Exams.SingleOrDefault(e => e.ExamId == examID);
            
            if (exam != null)
            {
                Class = OnlineQuizContext.Classes.Include(c => c.Course).SingleOrDefault(c => c.ClassId == exam.ClassId);
                string account = HttpContext.Session.GetString("account");
                TakeClasses = OnlineQuizContext.TakeClasses.FirstOrDefault(q => q.ClassId == Class.ClassId && q.StudentAccount == account);
                if (TakeClasses != null)
                {
                    questionList = OnlineQuizContext.QuestionExams.Where(q => q.ExamId == examID).ToList();
                    TakeExams = OnlineQuizContext.TakeExams.Where(q => q.ExamId == examID && q.StudentAccount == account).ToList();
                    answerList = new List<List<QuestionExamAnswer>>();

                    foreach (QuestionExam q in questionList)
                    {
                        answerList.Add(OnlineQuizContext.QuestionExamAnswers.Where(ans => ans.QuesId == q.QuesId).ToList());
                    }

                    return Page();
                }
                else
                {
                    return RedirectToPage("./MyCourses");
                }
                
            }
            else
            {
                return RedirectToPage("./MyCourses");
            }

        }
    }
}
