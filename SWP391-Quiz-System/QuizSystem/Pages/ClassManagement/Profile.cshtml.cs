using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using QuizSystem.Models;
using QuizSystem.Repositories;
using Microsoft.Extensions.Primitives;

namespace QuizSystem.Pages.ClassManagement {
    [Authorize(Policy = "Student and Lecturer")]
    public class ProfileModel : PageModel {
        public User? UserData { get; set; }
        public List<Class> ClassList { get; set; }
        public bool IsStudent { get; set; }
        UserRepository userRepository;
        StudentRepository studentRepository;
        LecturerRepository lecturerRepository;

        public ProfileModel(OnlineQuizContext _context) {
            userRepository = new UserRepository(_context);
            studentRepository = new StudentRepository(_context);
            lecturerRepository = new LecturerRepository(_context);
        }

        public async Task<IActionResult> OnGetAsync(string? u, string? id) {
            TempData["Message"] = "Cannot retrieve user data.";
            TempData["Color"] = "danger";
            var lastPage = "Index";
            var referer = Request.Headers["Referer"];
            if (!StringValues.IsNullOrEmpty(referer)) lastPage = referer.Last() ?? "Index";




            string? a = HttpContext.Session.GetString("account");

            User? getter = null;
            if (a != null) {
                getter = await userRepository.Find(a);
            }
            User? profiler = await userRepository.Find(u);
            if (getter == null || profiler == null) return Redirect(lastPage);

            if (getter.RoleId == 3) {
                if (profiler.RoleId != 4) return Redirect(lastPage);
                
                var classList = await lecturerRepository.GetClassListAsync(getter.Account);
                profiler.Student = await studentRepository.Find(profiler.Account);

                if (profiler.Student == null) return Redirect(lastPage);
                if (!getCommonClasses(classList, await studentRepository.GetClassListAsync(profiler.Account)).Any()) return Redirect(lastPage);

                IsStudent = true;
                ClassList = classList;
            }
            else {
                if (profiler.RoleId != 3) return Redirect(lastPage);

                var classList = await studentRepository.GetClassListAsync(getter.Account);
                profiler.Lecturer = await lecturerRepository.Find(profiler.Account);

                if (profiler.Lecturer == null) return Redirect(lastPage);
                if (!getCommonClasses(classList, await lecturerRepository.GetClassListAsync(profiler.Account)).Any()) return Redirect(lastPage);

                IsStudent = false;
                ClassList = classList;
            }
            UserData = profiler;

            TempData["Message"] = null;
            TempData["Color"] = null;
            return Page();

            List<Class> getCommonClasses(List<Class> list1, List<Class> list2) {
                List<Class> ret = new List<Class>();

                foreach (var c in list1) {
                    if (list2.Contains(c)) ret.Add(c);
                }

                return ret;
            }
        }
    }
}
