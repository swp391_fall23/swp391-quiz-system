﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;

namespace QuizSystem.Pages.ClassManagement
{
    public class DetailsModel : PageModel
    {
        private readonly QuizSystem.Models.OnlineQuizContext _context;

        public DetailsModel(QuizSystem.Models.OnlineQuizContext context)
        {
            _context = context;
        }

      public Class Class { get; set; } = default!;
        public IList<TakeClass> TakeClass { get; set; } = default!;

        public ActionResult OnGet(int? id)
        {
            return startUp(id);

        }
        private ActionResult startUp(int? id)
        {
            Class = _context.Classes.Include(x => x.Course)
                .Include(x => x.LecturerAccountNavigation).Include(x => x.CoWorkerNavigation).SingleOrDefault(x => x.ClassId == id);
            if (Class != null)
            {
                TakeClass = _context.TakeClasses.Include(x => x.StudentAccountNavigation).Where(x => x.ClassId == id).ToList();

                return Page();
            }
            else
            {
                return RedirectToPage("/Courses/ManageCourses");
            }

        }
    }
}
