﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;

namespace QuizSystem.Pages.ClassManagement
{
    public class EditModel : PageModel
    {
        private readonly QuizSystem.Models.OnlineQuizContext _context;

        public EditModel(QuizSystem.Models.OnlineQuizContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Class Class { get; set; } = default!;
        public IList<Lecturer> Lecturers { get; set; } = default!;
        public IList<Course> Courses { get; set; } = default!;
        public IList<TakeClass> TakeClass { get; set; } = default!;
        public IList<Student> Students { get; set; } = default!;

        public IList<Student> StudentsNotAttend { get; set; } = default!;

        public ActionResult OnGet(int? id)
        {
            return startUp(id);
            /*Lecturers = await _context.Lecturers
                .Include(x => x.AccountNavigation).Where(c=>c.Account != Class.LecturerAccount && c.Account != Class.CoWorker).ToListAsync();
            Courses = await _context.Courses.ToListAsync();
            Students = await _context.Students
                .Include(x => x.AccountNavigation).ToListAsync();
            TakeClass = mySystem.TakeClasses.Include(x => x.StudentAccountNavigation).Where(x => x.ClassId == id).ToList();

            List<Class> listClass = mySystem.Classes.Include(x => x.Course).Where(x=>x.EndDate >= DateTime.Today).ToList();
            List<Student> s = new List<Student>();
            foreach (var item in Students)
            {
                using (var context = new OnlineQuizContext())
                {
                    int count = 0;
                    TakeClass t1 = new TakeClass();
                    foreach (var c in listClass)
                    {
                        t1 = mySystem.TakeClasses.Include(x => x.StudentAccountNavigation).SingleOrDefault(x => x.StudentAccount == item.Account && x.ClassId == c.ClassId && x.Class.CourseId == Class.CourseId);
                        if(t1 != null)
                        {
                            count++;
                        }
                    }
                    if (count == 0)
                    {
                        s.Add(item);
                    }
                }
            }
            StudentsNotAttend = s;*/
        }
        public async Task<IActionResult> OnPostAsync(string id, string name, string startdate, string enddate )
        {
            var mySystem = new OnlineQuizContext();
            var _c = mySystem.Classes.SingleOrDefault(x => x.ClassName == name && x.ClassId != Int32.Parse(id));
            if (String.IsNullOrEmpty(name))
            {
                TempData["FailMessage"] = "Class name is only contains alphabet, numbers and hyphen(ABC-123).";
            }
            else if (!IsAlphaNumericWithHyphen(name))
            {
                TempData["FailMessage"] = "Class name is only contains alphabet, numbers and hyphen(ABC-123).";
            }
            else if (DateTime.Parse(startdate) > DateTime.Parse(enddate))
            {
                TempData["FailMessage"] = "Start date cannot be earlier than end date.";
            }
            else if (_c != null)
            {
                TempData["FailMessage"] = "ClassName already exist!";
            }
            else if (_c == null)
            {
                using (var context = new OnlineQuizContext())
                {
                    Class c = context.Classes.Include(x => x.Course)
                        .Include(x => x.LecturerAccountNavigation).SingleOrDefault(x => x.ClassId == Int32.Parse(id));
                    c.ClassName = name;
                    c.StartDate = DateTime.Parse(startdate);
                    c.EndDate = DateTime.Parse(enddate);
                    context.Entry<Class>(c).State =
                        Microsoft.EntityFrameworkCore.EntityState.Modified;
                    context.SaveChanges();
                }
                TempData["SuccessMessage"] = "Update Class Information Successfully!";
                return RedirectToPage("./Edit", new { id = id });
            }
            return RedirectToPage("./Edit", new { id = id });
        }

        public IActionResult OnPostAssignLecturer(string lecturer, string id)
        {
            using (var context = new OnlineQuizContext())
            {
                Class c = context.Classes.Include(x => x.Course)
                    .Include(x => x.LecturerAccountNavigation).SingleOrDefault(x => x.ClassId == Int32.Parse(id));
                c.LecturerAccount = lecturer;
                context.Entry<Class>(c).State =
                    Microsoft.EntityFrameworkCore.EntityState.Modified;
                context.SaveChanges();
                TempData["SuccessMessage"] = "Assign Lecturer Successfully!";
            }
            return RedirectToPage("./Edit", new { id = id });
        }

        public IActionResult OnPostAssignCoworker(string lecturer, string id)
        {
            using (var context = new OnlineQuizContext())
            {
                Class c = context.Classes.Include(x => x.Course)
                    .Include(x => x.CoWorkerNavigation).SingleOrDefault(x => x.ClassId == Int32.Parse(id));
                c.CoWorker = lecturer;
                context.Entry<Class>(c).State =
                    Microsoft.EntityFrameworkCore.EntityState.Modified;
                context.SaveChanges();
                TempData["SuccessMessage"] = "Assign Co-Worker Successfully!";
            }
            return RedirectToPage("./Edit", new { id = id });
        }

        public IActionResult OnPostAssignStudent(string[] student, string id)
        {
            using (var context = new OnlineQuizContext())
            {
                foreach (var item in student)
                {
                    var t = new TakeClass
                    {
                        ClassId = Int32.Parse(id),
                        StudentAccount = item
                    };
                    context.Add(t);
                }
                context.SaveChanges();
                TempData["SuccessMessage"] = "Assign Students Successfully!";
            }

            return RedirectToPage("./Edit", new { id = id });
        }

        public IActionResult OnPostDelete(int id)
        {
            var c = _context.TakeClasses.Find(id);
            _context.TakeClasses.Remove(c);
            _context.SaveChanges();
            return RedirectToPage("./Edit", new { id = c.ClassId });
        }

        static bool IsAlphaNumericWithHyphen(string text)
        {
            Regex regex = new Regex("^[A-Z][A-Za-z0-9-]*$");

            return regex.IsMatch(text);
        }

        private ActionResult startUp(int? id)
        {
            var mySystem = new OnlineQuizContext();

            Class = _context.Classes.Include(x => x.Course)
                .Include(x => x.LecturerAccountNavigation).Include(x => x.CoWorkerNavigation).SingleOrDefault(x => x.ClassId == id);
            if (Class != null)
            {
                Lecturers = _context.Lecturers
                .Include(x => x.AccountNavigation).Where(c => c.Account != Class.LecturerAccount && c.Account != Class.CoWorker).ToList();
                Courses = _context.Courses.ToList();
                Students = _context.Students
                    .Include(x => x.AccountNavigation).Where(x=>x.AccountNavigation.RoleId == 4).ToList();
                TakeClass = mySystem.TakeClasses.Include(x => x.StudentAccountNavigation).Where(x => x.ClassId == id).ToList();

                List<Class> listClass = mySystem.Classes.Include(x => x.Course).Where(x => x.EndDate >= DateTime.Today).ToList();
                List<Student> s = new List<Student>();
                foreach (var item in Students)
                {
                    using (var context = new OnlineQuizContext())
                    {
                        int count = 0;
                        TakeClass t1 = new TakeClass();
                        foreach (var c in listClass)
                        {
                            t1 = mySystem.TakeClasses.Include(x => x.StudentAccountNavigation).SingleOrDefault(x => x.StudentAccount == item.Account && x.ClassId == c.ClassId && x.Class.CourseId == Class.CourseId);
                            if (t1 != null)
                            {
                                count++;
                            }
                        }
                        //TakeClass t = mySystem.TakeClasses.Include(x => x.StudentAccountNavigation).SingleOrDefault(x => x.StudentAccount == item.Account && x.ClassId == Class.ClassId);
                        if (count == 0)
                        {
                            s.Add(item);
                        }
                    }
                }
                StudentsNotAttend = s;
                return Page();
            }
            else
            {
                return RedirectToPage("/Courses/ManageCourses");
            }
            
        }
    }
}
