using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using QuizSystem.Models;

namespace QuizSystem.Pages.ClassManagement
{
    public class TakeClassModel : PageModel
    {
        [BindProperty]
        public string StudentAccount {  get; set; }

        [BindProperty]
        public string ClassId { get; set; }

        OnlineQuizContext onlineQuizContext = new OnlineQuizContext();

        public void OnGet()
        {
        }
    }
}
