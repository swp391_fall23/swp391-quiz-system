﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;

namespace QuizSystem.Pages.ClassManagement
{
    public class indexModel : PageModel
    {
        private readonly QuizSystem.Models.OnlineQuizContext _context;

        public indexModel(QuizSystem.Models.OnlineQuizContext context)
        {
            _context = context;
        }

        public IList<Class> Class { get; set; } = default!;
        public IList<Lecturer> Lecturers { get; set; } = default!;
        public int CourseID { get; set; }
        public string CourseCode { get; set; }

        public ActionResult OnGet(int? id)
        {
            return startUp(id);

        }
        public IActionResult OnPostCreate(string name, string course, string startdate, string enddate)
        {
            var mySystem = new OnlineQuizContext();
            var _c = mySystem.Classes.SingleOrDefault(x=>x.ClassName == name);
            if (String.IsNullOrEmpty(name))
            {
                TempData["FailMessage"] = "Class name is only contains alphabet, numbers and hyphen(ABC-123).";
            }
            else if (!IsAlphaNumericWithHyphen(name))
            {
                TempData["FailMessage"] = "Class name is only contains alphabet, numbers and hyphen(ABC-123).";
            }else if (DateTime.Parse(startdate) < DateTime.Today || DateTime.Parse(enddate) < DateTime.Today)
            {
                TempData["FailMessage"] = "Start date and End date must be in the future.";
            }
            else if (DateTime.Parse(startdate) > DateTime.Parse(enddate))
            {
                TempData["FailMessage"] = "Start date cannot be earlier than end date.";
            }
            else if (_c != null)
            {
                TempData["FailMessage"] = "Class already exist!";
            }
            else if(_c == null)
            {
                var c = new Class
                {
                    ClassName = name,
                    CourseId = Int32.Parse(course),
                    StartDate = DateTime.Parse(startdate),
                    EndDate = DateTime.Parse(enddate)
                };
                _context.Classes.Add(c);
                _context.SaveChanges();
                TempData["SuccessMessage"] = "Insert Successfully! Please assign Lecturer and Student for the class.";
                return RedirectToPage("./Edit", new { id = c.ClassId });
            }
            return RedirectToPage("index", new { id = course });
        }

        public IActionResult OnPostDelete(int id, int courseid)
        {
            var c = _context.Classes.Find(id);
            _context.Classes.Remove(c);
            _context.SaveChanges();
            return RedirectToPage("index", new { id = courseid });
        }

        public IActionResult OnGetFind(int id)
        {
            var c = _context.Classes.Find(id);
            return new JsonResult(c);
        }


        static bool IsAlphaNumericWithHyphen(string text)
        {
            Regex regex = new Regex("^[A-Z][A-Za-z0-9-]*$");

            return regex.IsMatch(text);
        }

        private ActionResult startUp(int? id)
        {
            var mySystem = new OnlineQuizContext();
            Course c = mySystem.Courses.FirstOrDefault(x => x.CourseId == id);
            if (c != null)
            {
                Class = mySystem.Classes.Include(x => x.Course)
                    .Include(x => x.LecturerAccountNavigation).Where(x => x.CourseId == id).ToList();
                Lecturers = mySystem.Lecturers
                    .Include(x => x.AccountNavigation).ToList();
                CourseID = (int)id;
                CourseCode = c.CourseCode;

                return Page();
            }
            else
            {
                return RedirectToPage("/Courses/ManageCourses");
            }

        }
    }
}
