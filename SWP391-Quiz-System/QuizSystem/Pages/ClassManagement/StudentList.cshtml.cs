using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using QuizSystem.Models;

namespace QuizSystem.Pages.ClassManagement
{
    public class StudentListModel : PageModel
    {
        [BindProperty]
        public string Account { get; set; }

        [BindProperty]
        public Class Class { get; set; }

        [BindProperty]
        public Course Course { get; set; }

        OnlineQuizContext onlineQuizContext = new OnlineQuizContext();
        [BindProperty]
        public List<Student> students { get; set; }
      
        public void OnGet(int classId)
        {
            Class = onlineQuizContext.Classes.SingleOrDefault(x => x.ClassId == classId);
            Course = onlineQuizContext.Courses.SingleOrDefault(x => x.CourseId == Class.CourseId);
            students = new List<Student>();
            List<String> AccountList = new List<String>();
            List<TakeClass> takeClasses = onlineQuizContext.TakeClasses.Where(x => x.ClassId == classId).ToList();
            foreach (TakeClass takeClass in takeClasses)
            {
                AccountList.Add(takeClass.StudentAccount);
            }
            foreach(String account in AccountList) {
                students.Add(onlineQuizContext.Students.SingleOrDefault(x => x.Account == account));
            }
          
        }
    


    }
}
