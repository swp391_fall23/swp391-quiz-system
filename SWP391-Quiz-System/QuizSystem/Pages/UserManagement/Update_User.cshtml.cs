using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Primitives;
using QuizSystem.Models;
using QuizSystem.Repositories;
using QuizSystem.Services.Encryption;
using QuizSystem.Services.ExtensionMethods;
using System.Drawing;

namespace QuizSystem.Pages.UserManagement {
    public class Update_UserModel : PageModel {
        UserRepository userRepository;
        StudentRepository studentRepository;
        LecturerRepository lecturerRepository;
        ManagerRepository managerRepository;
        AdminRepository adminRepository;
        ClassRepository classRepository;
        
        public List<Role> RoleList { get; set; } = new List<Role>();
        public User UserData { get; set; }
        public Update_UserModel(OnlineQuizContext _context) {
            userRepository = new UserRepository(_context);
            studentRepository = new StudentRepository(_context);
            adminRepository = new AdminRepository(_context);
            lecturerRepository = new LecturerRepository(_context);
            managerRepository = new ManagerRepository(_context);
            classRepository = new ClassRepository(_context);

            _context.Users.Include(e => e.Admin).Load();
            _context.Users.Include(e => e.Manager).Load();
            _context.Users.Include(e => e.Lecturer).Load();
            _context.Users.Include(e => e.Student).Load();
            RoleList = new RoleRepository(_context).GetAll();

            if (RoleList.Any(e => e.Role1 != null && e.Role1.Equals("Admin", StringComparison.InvariantCultureIgnoreCase))) {
                RoleList.RemoveAll(e => e.Role1.Equals("Admin", StringComparison.InvariantCultureIgnoreCase));
            }
        }
        public async Task<IActionResult> OnGet(string u) {
            if (u != null && u.Length > 0) {
                string? acc = u.Decrypt();

                if (acc != null && await userRepository.Find(acc) != null) {
                    UserData = await userRepository.Find(acc);
                    return Page();
                }
            }
            TempData["Message"] = "Cannot get user.";
            TempData["Color"] = "danger";
            return RedirectToPage("Index");
        }

        public async Task<IActionResult> OnPostAsync(
            [Bind("Account", "Email", "Role_Role1", Prefix = "UserData")] User? user
            , [Bind("Mssv", "StudentName", "Dob", "Phone", "Major", Prefix = "UserData.Student")] Student? student
            , [Bind("LecturerName", "Phone", Prefix = "UserData.Lecturer")] Lecturer? lecturer
            , [Bind("AdminName", "Phone", Prefix = "UserData.Admin")] Admin? admin
            , [Bind("ManagerName", "Phone", Prefix = "UserData.Manager")] Manager? manager
            , string? role) {
            TempData["Color"] = "danger";

            var lastPage = "../";
            var referer = Request.Headers["Referer"];
			if (!StringValues.IsNullOrEmpty(referer)) lastPage = referer.Last() ?? "../";



            if (IsUserNull(user) || user?.Account == null || role == null) {
                return Redirect(lastPage);
            }
            Services.ValidationService.ValidationService validation = new Services.ValidationService.ValidationService() {
                Account = user.Account,
                Email = user.Email
            };
            user.Role = RoleList.FirstOrDefault(e => e.Role1.Equals(role, StringComparison.InvariantCultureIgnoreCase));
            user.RoleId = user.Role.RoleId;
            switch (role) {
                case "Student":
                    if (IsUserNull(student)) return Redirect(lastPage);
                    student.Account = user.Account;
                    validation.Name = student.StudentName;
                    validation.Phone = student.Phone;
                    validation.Major = student?.Major;
                    validation.DoB = student?.Dob;
                    break;
                case "Lecturer":
                    if (IsUserNull(lecturer)) return Redirect(lastPage);
                    lecturer.Account = user.Account;
                    validation.Name = lecturer?.LecturerName;
                    validation.Phone = lecturer?.Phone;
                    break;
                case "Manager":
                    if (IsUserNull(manager)) return Redirect(lastPage);
                    manager.Account = user.Account;
                    validation.Name = manager?.ManagerName;
                    validation.Phone = manager?.Phone;
                    break;
                case "Admin":
                    if (IsUserNull(admin)) return Redirect(lastPage);
                    admin.Account = user.Account;
                    validation.Name = admin?.AdminName;
                    validation.Phone = admin?.Phone;
                    break;
                default:
                    break;
            }

            var result = await validation.ValidateAll(true,false);
            if (!result.IsPassed() || (!validation.ValidateMssv(student.Mssv, out string? error) && role.Equals("Student"))) {
                List<string> errorList = result.Errors;
                if (role.Equals("Student") && !validation.ValidateMssv(student.Mssv, out string? e)) errorList.Add(e);
                if (result.Errors.Count == 1) {
                    TempData["Message"] = result.Errors[0];
                }
                else {
                    TempData["Message"] = "Encountering following issues:";
                    foreach (var item in result.Errors) {
                        TempData["Message"] += "<br/>  -  " + item;
                    }
                }
                UserData = user;
                UserData.Role = RoleList.FirstOrDefault(e => e.Role1.Equals(role, StringComparison.InvariantCultureIgnoreCase));
                UserData.Student = student;
                UserData.Lecturer = lecturer;
                UserData.Manager = manager;
                UserData.Admin = admin;
                return Redirect(lastPage);
            }
            try {
                User? u = await userRepository.Find(user.Account);
                if (!(u?.Role?.Role1 ?? role).Equals(role)) {
                    if (u.Role.Role1.Equals("Lecturer", StringComparison.InvariantCultureIgnoreCase)) {
                        Lecturer? l = await lecturerRepository.Find(user.Account);
                        if (await IsLecturerAttendOngoingClass(l)) return Redirect(lastPage);
                    }
                    if (u.Role.Role1.Equals("Student", StringComparison.InvariantCultureIgnoreCase)) {
                        Student? s = await studentRepository.Find(user.Account);
                        if (await IsStudentAttendOngoingClass(s)) return Redirect(lastPage);
                    }
                }

                Merge(ref u, user);

                switch (role) {
                    case "Student":
                        Student? s = await studentRepository.Find(user.Account);
                        if (s == null) await studentRepository.Add(student);
                        else {
                            Merge(ref s, student);
                            await studentRepository.Update(s);
                        }
                        break;
                    case "Lecturer":
                        Lecturer? l = await lecturerRepository.Find(user.Account);
                        if (l == null) await lecturerRepository.Add(lecturer);
                        else {
                            Merge(ref l, lecturer);
                            await lecturerRepository.Update(l);
                        }
                        break;
                    case "Manager":
                        Manager? m = await managerRepository.Find(user.Account);
                        if (m == null) await managerRepository.Add(manager);
                        else {
                            Merge(ref m, manager);
                            await managerRepository.Update(m);
                        }
                        break;
                    case "Admin":
                        Admin? a = await adminRepository.Find(user.Account);
                        if (a == null) u.Admin = admin;
                        else {
                            Merge(ref a, admin);
                            await adminRepository.Update(a);
                        }
                        break;
                    default:
                        break;
                }
                await userRepository.Update(u);
            }
            catch (Exception e) {
                await Console.Out.WriteLineAsync(e.Message + "\n" + e.StackTrace);

                TempData["Message"] = "Update user failed.";
                return Redirect(lastPage);
            }

            TempData["Color"] = "success";
            TempData["Message"] = "Update user successfully";
            return Redirect("Index");

            bool IsUserNull<T> (T? data) {
                if (data == null) {
                    ViewData["Message"] = "Cannot find user.";
                    return true;
                }
                return false;
            }
            async Task<bool> IsStudentAttendOngoingClass(Student? student) {
                if (student == null) return false;

                foreach (var c in student.TakeClasses.Select(e => e.ClassId)) {
                    if (c == null) continue;
                    Console.WriteLine("___ " + c);

                    Class? @class = await classRepository.Find((int)c);
                    if (@class == null) continue;
                    if (@class.StartDate >= DateTime.Now || @class.EndDate >= DateTime.Now) {
                        TempData["Message"] = "Student is assigned to upcoming or ongoing class(es). Contact a Manager to remove the user from said class(es) and try again!";
                        return true;
                    }
                }
                return false;
            }
            async Task<bool> IsLecturerAttendOngoingClass(Lecturer? lecturer) {
                if (student == null) return false;
                return false;
            }
            static void Merge<T> (ref T target, T data) where T : class {
                var excluded = new List<string>() { "Account", "Password" };
                foreach (var p in target.GetType().GetProperties()) {
                    if (p.CanWrite ) {
                        if (excluded.Contains(p.Name) || data.GetType().GetProperty(p.Name)?.GetValue(data) is null) continue;
                        if (data.GetType().GetProperty(p.Name)?.GetValue(data)?.GetType().IsGenericType ?? false) {
                            if (data.GetType().GetProperty(p.Name)?.GetValue(data)?.GetType().GetGenericTypeDefinition() == typeof(List<>)) continue;
                        }
                        p.SetValue(target, data.GetType().GetProperty(p.Name).GetValue(data));
                    }
                }
            }
        }
    }
}
