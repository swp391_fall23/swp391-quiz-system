using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Primitives;
using QuizSystem.Models;
using QuizSystem.Repositories;
using QuizSystem.Services.Encryption;

namespace QuizSystem.Pages.UserManagement {
    public class Delete_AccountModel : PageModel {
        UserRepository userRepository;
        LecturerRepository lecturerRepository;

        public Delete_AccountModel(OnlineQuizContext _context) {
            userRepository = new UserRepository(_context);
            lecturerRepository = new LecturerRepository(_context);
        }
        public IActionResult OnGet() {
            var lastPage = "../";
            var referer = Request.Headers["Referer"];
            if (!StringValues.IsNullOrEmpty(referer)) lastPage = referer.Last() ?? "../";
            return Redirect(lastPage);
        }

        public async Task<IActionResult> OnPostAsync(string? DeleteU) {
            var lastPage = "../";
            var referer = Request.Headers["Referer"];
			if (!StringValues.IsNullOrEmpty(referer)) lastPage = referer.Last() ?? "../";



			TempData["Color"] = "danger";
            TempData["Message"] = "Cannot get user data";
            string? username = DeleteU.Decrypt();

            if (username == null) return Redirect(lastPage);
            User? u = await userRepository.Find(username);
            if (u == null) return Redirect(lastPage);

            if (u.Role?.Role1 != null && u.Role.Role1.Equals("Lecturer", StringComparison.InvariantCultureIgnoreCase)) {
                var l = (await lecturerRepository.Find(u.Account));
                if (l == null) return Redirect(lastPage);

                if (l.ClassCoWorkerNavigations != null) {
                    var classes = l.ClassCoWorkerNavigations.ToList();
                    foreach (var c in classes) {
                        if (c.StartDate >= DateTime.Now || c.EndDate >= DateTime.Now) {
                            TempData["Message"] = "Lecturer is attending upcoming or on-going class(es). Remove them from said class(es) and try again.";
                            return Redirect(lastPage);
                        }
                    }
                }
                if (l.ClassLecturerAccountNavigations != null) {
                    var classes = l.ClassLecturerAccountNavigations.ToList();
                    foreach (var c in classes) {
                        if (c.StartDate >= DateTime.Now || c.EndDate >= DateTime.Now) {
                            TempData["Message"] = "Lecturer is attending upcoming or on-going class(es). Remove them from said class(es) and try again.";
                            return Redirect(lastPage);
                        }
                    }
                }
            }

            try {
                await userRepository.Delete(username);
            }
            catch (Exception ex) {
                TempData["Message"] = "Cannot delete account";
                await Console.Out.WriteLineAsync(ex.Message);

                return Redirect(lastPage);
            }

            TempData["Color"] = "success";
            TempData["Message"] = "Account deleted";
            return Redirect(lastPage);
        }
    }
}
