using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using QuizSystem.Models;
using QuizSystem.Repositories;
using QuizSystem.Services.Encryption;

namespace QuizSystem.Pages.UserManagement {
    public class View_InfoModel : PageModel {
        public User UserData { get; set; }
        public List<Role> RoleList { get; set; }
        UserRepository userRepository;
        public View_InfoModel(OnlineQuizContext _context) {
            userRepository = new UserRepository(_context);
            RoleList = new Repositories.RoleRepository(_context).GetAll();

        }
        public async Task<IActionResult> OnGetAsync(string? u) {
            var lastPage = "../";
            var referer = Request.Headers["Referer"];
			if (!StringValues.IsNullOrEmpty(referer)) lastPage = referer.Last() ?? "../";



			string? username = u.Decrypt();
            if (username == null) {
                TempData["Color"] = "danger";
                TempData["Message"] = "Can't get user data.";
            }

            User? user = await userRepository.Find(username);
            if (user == null) {
                TempData["Color"] = "danger";
                TempData["Message"] = "Can't get user data.";
                return Redirect(lastPage);
            }

            UserData = user;

            return Page();
        }
    }
}
