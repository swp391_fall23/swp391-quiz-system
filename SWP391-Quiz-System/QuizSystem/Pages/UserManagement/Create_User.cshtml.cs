using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;
using QuizSystem.Repositories;
using QuizSystem.Services.ExtensionMethods;
using QuizSystem.Services.MailingService;
using QuizSystem.Services.ValidationService;

namespace QuizSystem.Pages.UserManagement {

    public class Create_UserModel : PageModel {
        UserRepository userRepository;
        AdminRepository adminRepository;
        ManagerRepository managerRepository;
        LecturerRepository lecturerRepository;
        StudentRepository studentRepository;

        ISendMailService mailService;

        public List<Role> RoleList { get; set; } = new List<Role>();
        public string DefaultTemplate { get; set; } = "Student";
        public Create_UserModel(OnlineQuizContext _context, ISendMailService _mailService) {
            RoleList = new Repositories.RoleRepository(_context).GetAll();

            if (RoleList.Any(e => e.Role1 != null && e.Role1.Equals("Admin", StringComparison.InvariantCultureIgnoreCase))) {
                RoleList.RemoveAll(e => e.Role1.Equals("Admin", StringComparison.InvariantCultureIgnoreCase));
            }

            mailService = _mailService;

            adminRepository = new AdminRepository(_context);
            managerRepository = new ManagerRepository(_context);
            lecturerRepository = new LecturerRepository(_context);
            studentRepository = new StudentRepository(_context);
            userRepository = new UserRepository(_context);
        }

        public void OnGet() {
        }
        public async Task<IActionResult> OnPostAsync(ValidationService validation, string? role) {
            TempData["Color"] = "danger";
            if (role == null) {
                TempData["Message"] = "Cannot retrieve selected role";
                return Page();
            }

            if (role != "Student") {
                validation.Mssv = null;
                validation.DoB = null;
                validation.Major = null;
            }

            var result = await validation.ValidateAll(isExist: false, isFU: role.Equals("Student", StringComparison.InvariantCultureIgnoreCase) ? true : false, ignoreNull: false);


            result.Password = ValidResult.Success;
            result.ConfirmPassword = ValidResult.Success;
            if (result.Errors.Any(e => e.Contains("Password", StringComparison.InvariantCultureIgnoreCase)))
                result.Errors.Remove(result.Errors.First(e => e.Contains("Password", StringComparison.InvariantCultureIgnoreCase)));

            if (!role.Equals("Student")) {
                result.Mssv = ValidResult.Success;
                if (result.Errors.Any(e => e.Contains("Mssv", StringComparison.InvariantCultureIgnoreCase))) 
                    result.Errors.Remove(result.Errors.First(e => e.Contains("Mssv", StringComparison.InvariantCultureIgnoreCase)));
                result.Major = ValidResult.Success;
                if (result.Errors.Any(e => e.Contains("Major", StringComparison.InvariantCultureIgnoreCase))) 
                    result.Errors.Remove(result.Errors.First(e => e.Contains("Major", StringComparison.InvariantCultureIgnoreCase)));
                result.DoB = ValidResult.Success;
                if (result.Errors.Any(e => e.Contains("DoB", StringComparison.InvariantCultureIgnoreCase))) 
                    result.Errors.Remove(result.Errors.First(e => e.Contains("DoB", StringComparison.InvariantCultureIgnoreCase)));
            }

            if (!result.IsPassed()) {
                if (result.Errors.Count == 1) {
                    TempData["Message"] = result.Errors[0];
                }
                else {
                    TempData["Message"] = "Encountering following issues:";
                    foreach (var error in result.GetType().GetProperties()) {
                        await Console.Out.WriteLineAsync(error.Name + ": " + error.GetValue(result));
                    }
                    foreach (var item in result.Errors) {
                        TempData["Message"] += "<br/>  -  " + item;
                    }
                }
                return Page();
            }

            string newPassword = userRepository.GenerateRandomPassword(10,10,false);
            try {
                User u = new User() {
                    Account = validation.Account,
                    Email = validation.Email,
                    Password = BCrypt.Net.BCrypt.HashPassword(newPassword)
                };
                u.Role = RoleList.FirstOrDefault(e => e.Role1.Equals(role, StringComparison.InvariantCultureIgnoreCase));
                u.RoleId = u.Role.RoleId;

                await userRepository.Add(u);

                switch (role) {
                    case "Admin":
                        Admin a = new Admin() {
                            AdminName = validation.Name,
                            Phone = validation.Phone,
                            Account = validation.Account,
                            
                        };
                        await adminRepository.Add(a);
                        break;
                    case "Manager":
                        Manager m = new Manager() {
                            ManagerName = validation.Name,
                            Phone = validation.Phone,
                            Account = validation.Account
                        };
                        await managerRepository.Add(m);
                        break;
                    case "Lecturer":
                        Lecturer l = new Lecturer() {
                            LecturerName = validation.Name,
                            Phone = validation.Phone,
                            Account = validation.Account
                        };
                        await lecturerRepository.Add(l);
                        break;
                    case "Student":
                        Student s = new Student() {
                            StudentName = validation.Name,
                            Phone = validation.Phone,
                            Account = validation.Account,
                            Dob = validation.DoB,
                            Mssv = validation.Mssv,
                            Major = validation.Major
                        };
                        await studentRepository.Add(s);
                        break;
                }

                u.Password = newPassword;
                await mailService.SendMailWithTemplateAsync(u, MailObjective.AccountCreateByAdmin);
            }
            catch (Exception ex) {
                await Console.Out.WriteLineAsync(ex.Message + "\n" + ex.StackTrace);
                TempData["Message"] = "Creating user failed.";
                return Page();
            }

            TempData["Color"] = "success";
            TempData["Message"] = "New account successfully created.";
            return RedirectToPage("Index");
        }
    }
}
