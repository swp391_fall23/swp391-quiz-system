using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Primitives;
using QuizSystem.Models;
using QuizSystem.Repositories;
using QuizSystem.Services.Encryption;

namespace QuizSystem.Pages.UserManagement {
    public class SearchModel : PageModel {
        public UserRepository userRepository { get; set; }
        public SearchModel(OnlineQuizContext _context) {
            userRepository = new UserRepository(_context);
        }
        public IActionResult OnGet() {
            var lastPage = "../";
            var referer = Request.Headers["Referer"];
            if (!StringValues.IsNullOrEmpty(referer)) lastPage = referer.Last() ?? "../";
            return Redirect(lastPage);
        }
        public IActionResult OnPost(string search, string? role) {
            Console.WriteLine("ROle: " + role);
            List<User> UserList = userRepository.GetAll();
            UserList = userRepository.GetAll();
            string? email = User.Claims != null ? User.Claims?.ElementAtOrDefault(4)?.Value : null;
            if (email != null && UserList.Any(e => e.Email.Equals(email) || e.Account.Equals(email))) {
                User? u = UserList.FirstOrDefault(e => e.Email.Equals(email) || e.Account.Equals(email));
                if (u != null) {
                    UserList.Remove(u);
                }
            }
            if (search != null && search.Length > 0) {
                UserList = UserList.Where(e => e.Account.Contains(search, StringComparison.InvariantCultureIgnoreCase)
                    || e.Email.Contains(search, StringComparison.InvariantCultureIgnoreCase)
                ).ToList();
            }
            var wrongRoleUsers = new List<User>();
            if (role != null) {
                foreach (var user in UserList) {
                    if (user.Role == null || user.Role.Role1 != role) {
                        wrongRoleUsers.Add(user);
                    }
                }
            }
            foreach (var user in wrongRoleUsers) {
                UserList.Remove(user);
            }
            if (UserList.Count == 1) return RedirectToPage("View_Info", new { u = UserList[0].Account.Encrypt() });
            return RedirectToPage("Index", new { search = search, role = role });
        }
    }
}
