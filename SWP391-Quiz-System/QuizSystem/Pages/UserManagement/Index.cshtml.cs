using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using QuizSystem.Models;
using QuizSystem.Repositories;
using System.Drawing;

namespace QuizSystem.Pages.UserManagement {
    public class IndexModel : PageModel {
        public List<User> UserList { get; set; } = new List<User>();
        public List<string?> RoleList { get; set; }
        UserRepository userRepository;

        public IndexModel(OnlineQuizContext _context) {
            userRepository = new UserRepository(_context);
            RoleList = new RoleRepository(_context).GetAll().ToList().Select(e => e.Role1).ToList();
        }
        public void OnGet(string? search, string? role) {

            //var claims = User.Claims.ToList();
            //int i = 1;
            //foreach (var claim in claims) {
            //    Console.WriteLine(i++ + ": " +claim);
            //    Console.WriteLine("Subject:" + claim.Subject);
            //    Console.WriteLine("Issuer:" + claim.Issuer);
            //    Console.WriteLine("ValueType:" + claim.ValueType);
            //    Console.WriteLine("Properties:" + claim.Properties);
            //    Console.WriteLine("Value:" + claim.Value);
            //    Console.WriteLine();
            //    Console.WriteLine();
            //}

            UserList = userRepository.GetAll();
            string? email = User.Claims != null ? User.Claims?.ElementAtOrDefault(4)?.Value : null;
            if (email != null && UserList.Any(e => e.Email.Equals(email) || e.Account.Equals(email))) {
                User? u = UserList.FirstOrDefault(e => e.Email.Equals(email) || e.Account.Equals(email));
                if (u != null) {
                    UserList.Remove(u);
                }
            }

            if (search != null) {
                UserList = UserList.Where(e => e.Account.Contains(search, StringComparison.InvariantCultureIgnoreCase)
                    || e.Email.Contains(search, StringComparison.InvariantCultureIgnoreCase)
                ).ToList();
            }
            var wrongRoleUsers = new List<User>();
            if (role != null) {
                foreach (var user in UserList) {
                    if (user.Role == null || user.Role.Role1 != role) {
                        wrongRoleUsers.Add(user);
                    }
                }
            }
            foreach (var user in wrongRoleUsers) {
                UserList.Remove(user);
            }
        }
    }
}
