﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;


namespace QuizSystem.Pages.Login
{
    public class ProfileModel : PageModel
    {
        private readonly QuizSystem.Models.OnlineQuizContext _context;
        public string Email { get; set; }
        public ProfileModel(QuizSystem.Models.OnlineQuizContext context)
        {
            _context = context;
        }


        public async Task<IActionResult> OnGetAsync()
        {
            string account = HttpContext.Session.GetString("account");
            if (HttpContext.Session.GetString("account") == null)
            {
                return RedirectToPage("/Login/Login");
            }
            if (HttpContext.Session.GetString("role") == "4")
            {
                var student = await _context.Students.Include(s => s.AccountNavigation).FirstOrDefaultAsync(m => m.Account == account);
                ViewData["SelfProfile"] = student;
                if (student == null)
                {
                    return NotFound();
                }
            }else if (HttpContext.Session.GetString("role") == "3")
            {
                var lecturer = await _context.Lecturers.Include(s => s.AccountNavigation).FirstOrDefaultAsync(m => m.Account == account);
                ViewData["SelfProfile"] = lecturer;
                if (lecturer == null)
                {
                    return NotFound();
                }
            }
            else if (HttpContext.Session.GetString("role") == "2")
            {
                var manager = await _context.Managers.Include(s => s.AccountNavigation).FirstOrDefaultAsync(m => m.Account == account);
                ViewData["SelfProfile"] = manager;
                if (manager == null)
                {
                    return NotFound();
                }
            }
            else if (HttpContext.Session.GetString("role") == "1")
            {
                var admin = await _context.Admins.Include(s => s.AccountNavigation).FirstOrDefaultAsync(m => m.Account == account);
                ViewData["SelfProfile"] = admin;
                if (admin == null)
                {
                    return NotFound();
                }
            }

            var result = await HttpContext.AuthenticateAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            var claims = result.Principal?.Identities.FirstOrDefault()?.Claims.Select(claim => new {
                claim.Issuer,
                claim.OriginalIssuer,
                claim.Type,
                claim.Value
            });
            string? a = claims?.ElementAt(4).Value;

            if (TempData != null)
            {
                ViewData["SuccessMessage"] = TempData["SuccessMessage"];
                ViewData["ErrorMessage"] = TempData["ErrorMessage"];
            }
            var user = await _context.Users.FirstOrDefaultAsync(m => m.Account == account);
            ViewData["User"] = user;
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(string phone)
        {
            string account = HttpContext.Session.GetString("account");
            if (HttpContext.Session.GetString("account") == null)
            {
                return RedirectToPage("/Login/Login");
            }
            if (String.IsNullOrEmpty(phone))
            {
                TempData["ErrorMessage"] = "Phone Number is invalid! Please try again.";
                TempData["SuccessMessage"] = null;
            }
            else if (!IsValidPhoneNumber(phone))
            {
                TempData["ErrorMessage"] = "Phone Number is invalid! Please try again.";
                TempData["SuccessMessage"] = null;
            }
            else
            {
                if (HttpContext.Session.GetString("role") == "4")
                {
                    var student = await _context.Students.Include(s => s.AccountNavigation).FirstOrDefaultAsync(m => m.Account == account);
                    student.Phone = phone;
                    _context.Entry<Student>(student).State =
                                Microsoft.EntityFrameworkCore.EntityState.Modified;
                }
                if (HttpContext.Session.GetString("role") == "3")
                {
                    var lecturer = await _context.Lecturers.Include(s => s.AccountNavigation).FirstOrDefaultAsync(m => m.Account == account);
                    lecturer.Phone = phone;
                    _context.Entry<Lecturer>(lecturer).State =
                                Microsoft.EntityFrameworkCore.EntityState.Modified;
                }
                if (HttpContext.Session.GetString("role") == "2")
                {
                    var manager = await _context.Managers.Include(s => s.AccountNavigation).FirstOrDefaultAsync(m => m.Account == account);
                    manager.Phone = phone;
                    _context.Entry<Manager>(manager).State =
                                Microsoft.EntityFrameworkCore.EntityState.Modified;
                }
                if (HttpContext.Session.GetString("role") == "1")
                {
                    var admin = await _context.Admins.Include(s => s.AccountNavigation).FirstOrDefaultAsync(m => m.Account == account);
                    admin.Phone = phone;
                    _context.Entry<Admin>(admin).State =
                                Microsoft.EntityFrameworkCore.EntityState.Modified;
                }
                _context.SaveChanges();
                TempData["SuccessMessage"] = "Change successfully!";
                TempData["ErrorMessage"] = null;
            }
            return RedirectToPage("/Login/Profile");
        }

        static bool IsValidPhoneNumber(string phoneNumber)
        {
            Regex regex = new Regex(@"^0\d{9}$");

            return regex.IsMatch(phoneNumber);
        }

    }
}
