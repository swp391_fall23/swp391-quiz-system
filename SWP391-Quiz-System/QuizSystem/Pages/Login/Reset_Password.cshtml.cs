using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Primitives;
using QuizSystem.Models;
using QuizSystem.Repositories;
using QuizSystem.Services.Encryption;
using QuizSystem.Services.ExtensionMethods;

namespace QuizSystem.Pages.Login {
    public class Reset_PasswordModel : PageModel {
        public string? Email { get; set; }
        public string? OldPassword { get; set; }
        public UserRepository userRepository { get; set; }

        public Reset_PasswordModel(OnlineQuizContext _context) {
            userRepository = new UserRepository(_context);
        }

        public async Task<IActionResult> OnGet(string? u, string? data) {
            TempData["Message"] = "Cannot get user data";
            TempData["Color"] = "danger";

            var lastPage = "../";
            var referer = Request.Headers["Referer"];
			if (!StringValues.IsNullOrEmpty(referer)) lastPage = referer.Last() ?? "../";



			string? acc = u.Decrypt();
            string? pass = data.Decrypt();
            if (acc == null || acc.Length == 0) {
                return Redirect(lastPage);
            }

            if ((await userRepository.IsEmailAvailable(acc))) {
                TempData["Message"] = "Your e-mail is not registered in our system: " + acc + "!";
                return Redirect(lastPage);
            }
            Email = u;

            if (pass != null && ((await userRepository.FindUserByEmail(acc))?.Password ?? "").VerifyHashedPassword(pass)) {
                OldPassword = pass;
            }

            TempData["Message"] = null;
            TempData["Color"] = null;
            return Page();
        }

        public async Task<IActionResult> OnPost(string? Email, string? OldPassword, string? password, string? confirmPassword) {
            TempData["Color"] = "warning";
            if (password != confirmPassword) {
                TempData["Message"] = "Password and Confirm Password do not match";
                return Page();
            }

            if (Email == null || Email.Length == 0) {
                TempData["Message"] = "Please refresh you page and try again!";
                return Page();
            }
            if (OldPassword == null || OldPassword.Length == 0) {
                TempData["Message"] = "The old password is incorrect!";
                return Page();
            }

            string? acc = Email.Decrypt();
            if (acc == null || !await userRepository.CheckLoginCredentials(acc, OldPassword)) {
                TempData["Message"] = "The old password is incorrect!";
                return Page();
            }

            User? u = await userRepository.Find(acc);
            if (u == null) {
                TempData["Message"] = "Cannot get user profile!";
                return Page();
            }

            u.Password = BCrypt.Net.BCrypt.HashPassword(password);
            await userRepository.Update(u);

            TempData["Message"] = "Update successfully! Please log in to your account using your new password.";
            TempData["Color"] = "success";
            return RedirectToPage("/Login/Login");
        }
    }
}
