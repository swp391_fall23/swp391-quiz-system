using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.Google;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using QuizSystem.Models;

namespace QuizSystem.Pages.Login {
    public class LoginModel : PageModel {
        [BindProperty]
        public string Email { get; set; }

        [BindProperty]
        public string Password { get; set; }

        public string Msg { get; set; }

        OnlineQuizContext onlineQuizContext = new OnlineQuizContext();

        public void OnGet() {

        }

        public IActionResult OnPost() {
            User u = onlineQuizContext.Users.FirstOrDefault(u => u.Email == Email);
            if (u == null) {
                Msg = "Account not found";
                return Page();
            }
            else {
                if (Password == null || Password.Trim() == "")
                {
                    Msg = "Please do not just enter spaces in the field Password!";
                    return Page();
                }

                if (u.IsActive != true)
                {
                    Msg = "Your account is disabled! Please contact the admin for more information!";
                    return Page();
                }
                else
                {
                    if (BCrypt.Net.BCrypt.Verify(Password, u.Password))
                    {
                        HttpContext.Session.SetString("user", u.Email);
                        HttpContext.Session.SetString("account", u.Account);
                        HttpContext.Session.SetString("role", u.RoleId.ToString());
                        var claims = new List<Claim>
                        {
                            new Claim(ClaimTypes.Name, ""),
                            new Claim(ClaimTypes.Country, ""),
                            new Claim(ClaimTypes.DateOfBirth, ""),
                            new Claim(ClaimTypes.Role, ""),
                            new Claim(ClaimTypes.Email, u.Email)
                        };

                        var claimsIdentity = new ClaimsIdentity(
                            claims, CookieAuthenticationDefaults.AuthenticationScheme);

                        var authProperties = new AuthenticationProperties
                        {
                            AllowRefresh = true
                        };
                        HttpContext.SignInAsync(
                            CookieAuthenticationDefaults.AuthenticationScheme,
                            new ClaimsPrincipal(claimsIdentity),
                            authProperties);
                        if (u.RoleId == 4)
                        {
                            return Redirect("/Dashboard/StudentDashboard");
                        }
                        else
                        {
                            return Redirect("/Index");
                        }
                    }
                    else
                    {
                        Msg = "Login Failed";
                        return Page();
                    }
                }
                
            }
        }
    }
}
