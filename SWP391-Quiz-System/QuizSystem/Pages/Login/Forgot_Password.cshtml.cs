using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using QuizSystem.Models;
using QuizSystem.Repositories;
using QuizSystem.Services.Encryption;
using QuizSystem.Services.MailingService;

namespace QuizSystem.Pages.Login {
    public class Forgot_PasswordModel : PageModel {
        private UserRepository userRepository;
        private ISendMailService mailService;


        public Forgot_PasswordModel(ISendMailService _mailService, OnlineQuizContext _context) {
            mailService = _mailService;
            userRepository = new UserRepository(_context);
        }

        public void OnGet() {
            if (TempData["Message"] == null) {
                TempData["Message"] = "Enter your recovery e-mail to create new password.";
                TempData["Color"] = "info";
            }
        }

        public async Task<IActionResult> OnPost(string email) {
            if (await userRepository.IsEmailAvailable(email)) {
                TempData["Message"] = $"Cannot find user's email \"{email}\". Please check your entered e-mail address and try again.";
                TempData["Color"] = "warning";
                return Page();
            }
            else {
                User user = await userRepository.FindUserByEmail(email);
                string password = await userRepository.ResetPassword(user.Account);
                user.Password = password;
                bool isMailSent = await mailService.SendMailWithTemplateAsync(user, MailObjective.ForgotPassword);

                //bool isMailSent = await mailService.SendMailAsync("longdhhe160008@fpt.edu.vn", "Hi", "Hello");
                if (!isMailSent) {
                    TempData["Message"] = "Cannot send recovery e-mail. Please check your entered e-mail address and try again.";
                    TempData["Color"] = "danger";
                    return Page();
                }
                else {
                    TempData["Message"] = "Send email successfully! Please check your email to get your new password.";
                    TempData["Color"] = "success";

                    return RedirectToPage("Reset_Password", new { u = email.Encrypt() });
                }

            }
        }
    }
}
