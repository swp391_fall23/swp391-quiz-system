using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using QuizSystem.Models;
using QuizSystem.Repositories;
using QuizSystem.Services.Encryption;
using QuizSystem.Services.ExtensionMethods;
using QuizSystem.Services.MailingService;
using QuizSystem.Services.ValidationService;
using System.Text;
using System.Web;

namespace QuizSystem.Pages.Login {
    public class Create_AccountModel : PageModel {
        private UserRepository _userRepository;
        private ISendMailService _mailService;
        private StudentRepository _studentRepository;

        public string? Email { get; set; } = null;

        public Create_AccountModel(ISendMailService mailService, OnlineQuizContext context) {
            _studentRepository = new StudentRepository(context);
            _userRepository = new UserRepository(context);
            _mailService = mailService;
        }

        public async void OnGet(string? u) {
            string? acc = u.Decrypt();
            if (acc != null && (new ValidationService().ValidateEmail(acc, out var error))) {
                Email = acc;
            }
        }

        public async Task<IActionResult> OnPost(ValidationService validation) {
            await Console.Out.WriteLineAsync("Email = " + validation.Email + "\nAccount" + validation.Account);
            var result = await validation.ValidateAll(false);
            if (result.IsPassed()) {
                try {
                    User u = new User() {
                        Account = validation.Account,
                        Email = validation.Email,
                        Password = BCrypt.Net.BCrypt.HashPassword(validation.Password),
                        RoleId = 4
                    };

                    Student s = new Student() {
                        Account = validation.Account,
                        Mssv = validation.Account,
                        Dob = validation.DoB,
                        Major = validation.Major,
                        Phone = validation.Phone,
                        StudentName = validation.Name
                    };

                    await _userRepository.Add(u);
                    await _studentRepository.Add(s);
                    await _mailService.SendMailWithTemplateAsync(u, MailObjective.CreateAccount);
                }
                catch (Exception ex) {
                    await Console.Out.WriteLineAsync(ex.Message);
                    TempData["Message"] = "Cannot create user";
                    TempData["Color"] = "danger";
                    return Page();
                }

                TempData["Message"] = "Create account successfully! Please log in to your new account.";
                TempData["Color"] = "success";
                return RedirectToPage("/Login/Login");
            }
            else {
                TempData["Color"] = "danger";
                if (result.Errors.Count > 1) {
                    TempData["Message"] = "Cannot create user:";
                    foreach (var s in result.Errors) {
                        TempData["Message"] += "<br/>  -  " + s;
                    }
                }
                else TempData["Message"] = result.Errors[0];
                return Page();
            }
        }
    }
}
