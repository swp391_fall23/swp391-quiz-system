using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;

namespace QuizSystem.Pages.Login
{
    public class ChangePasswordModel : PageModel
    {
        public string Alert { get; set; }
        public string Notice { get; set; }
        public IActionResult OnGet()
        {
            if (HttpContext.Session.GetString("user") == null)
            {
                return RedirectToPage("/Login/Login");
            }
            return Page();
        }
        public void OnPost(string oldpass, string newpass, string confirmpass)
        {
            User u = null;
            Alert = null;
            Notice = null;
            try
            {
                var mySystem = new OnlineQuizContext();
                string email = HttpContext.Session.GetString("user");
                u = mySystem.Users.SingleOrDefault(u => u.Email == email);
                if (!BCrypt.Net.BCrypt.Verify(oldpass, u.Password))
                {
                    Alert = "Password incorrect! Please try again.";
                    Notice = null;
                }
                else if (newpass == oldpass)
                {
                    Alert = "Current Password and Confirm Password are the same! Please try again.";
                    Notice = null;
                }
                else if (newpass != confirmpass && newpass != oldpass)
                {
                    Alert = "New Password and Confirm Password not match! Please try again.";
                    Notice = null;
                }
                else if (newpass == confirmpass && newpass != oldpass)
                {
                    u.Password = newpass;
                    mySystem.Entry<User>(u).State = EntityState.Modified;
                    mySystem.SaveChanges();
                    Notice = "Change sucessfully!";
                    Alert = null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
