﻿using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;

namespace QuizSystem.Repositories {
    public class TakeExamRepository : OnlineQuizRepository<TakeExam> {
        public TakeExamRepository(OnlineQuizContext _context) : base(_context) {
            _context.TakeExams.Select(e => e.TakeAnswers).Load();
            var onGoingExams = _context.TakeExams.Include(e => e.Exam).Where(e => e.StartDate < DateTime.Now && (e.Status ?? "").Equals("In progress")).ToList();

            foreach (var e in onGoingExams) {
                Console.WriteLine(e.TakeExamId);
                DateTime startDate = e.StartDate ?? DateTime.MaxValue;
                int timer = e.Exam?.Timer ?? -1;
                if (startDate == DateTime.MaxValue || timer == -1) continue;

                if (startDate.AddMinutes(timer) < DateTime.Now) {
                    e.Status = "Finished";
                    e.Score = 0;
                    e.EndDate = startDate.AddMinutes(timer);
                }
                _context.TakeExams.Update(e);
                _context.SaveChanges();
            }
        }

        [Obsolete]
        public override Task<TakeExam?> Find(string key) {
            return base.Find(key);
        }

        public async Task<TakeExam?> Find(int key) => await Set.FindAsync(key);

        public async Task<int> UserTakeExamCount(string userId, int examId) => await  Set.CountAsync(e => e.ExamId == examId && e.StudentAccount != null && e.StudentAccount.Equals(userId.Trim()));

        public async Task<TakeExam?> GetStudentLatestOnGoingExam(string studentAccount, int? examId) => await Set.OrderByDescending(e => e.StartDate).FirstOrDefaultAsync(e => (e.StudentAccount ?? "").Equals(studentAccount.Trim()) && (e.ExamId ?? -1) == examId && (e.Status ?? "").Equals("In progress"));
    }
}
