﻿using QuizSystem.Models;

namespace QuizSystem.Repositories {
    public class RoleRepository : OnlineQuizRepository<Role> {
        public RoleRepository(OnlineQuizContext _context) : base(_context) {
        }
    }
}
