﻿using QuizSystem.Models;

namespace QuizSystem.Repositories {
    public class AdminRepository : OnlineQuizRepository<Admin> {
        public AdminRepository(OnlineQuizContext _context) : base(_context) {
        }
    }
}
