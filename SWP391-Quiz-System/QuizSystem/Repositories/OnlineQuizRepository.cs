﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;

namespace QuizSystem.Repositories {
    public abstract class OnlineQuizRepository <T> where T : class {
        protected OnlineQuizContext Context;
        protected DbSet<T> Set;
        public OnlineQuizRepository(OnlineQuizContext _context) {
            Context = _context;
            Set = Context.Set<T>();
        }

        public async virtual Task<T?> Find(string key) => await Set.FindAsync(key);

        public virtual async Task Add(T entity) {
            Set.Add(entity);
            await Context.SaveChangesAsync();
        }

        public virtual async Task Update(T entity) {
            Set.Update(entity);
            await Context.SaveChangesAsync();
        }

        public virtual async Task Delete(string key) {
            T? entity = await Find(key);
            if (entity != null) Set.Remove(entity);
            else throw new Exception("Cannot delete entity because it does not exist!");
            await Context.SaveChangesAsync();
        }

        public virtual List<T> GetAll() => Set.ToList<T>();
    }
}
