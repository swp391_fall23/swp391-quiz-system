﻿using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;

namespace QuizSystem.Repositories {
    public class ExamRepository : OnlineQuizRepository<Exam> {
        public ExamRepository(OnlineQuizContext _context) : base(_context) {
            _context.Exams.Include(e => e.QuestionExams).ThenInclude(e => e.QuestionExamAnswers).Load();
        }
        [Obsolete]
        public override Task<Exam?> Find(string key) {
            return base.Find(key);
        }

        public async Task<Exam?> Find(int key) => await Set.FindAsync(key);
    }
}
