﻿using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;

namespace QuizSystem.Repositories {
    public class StudentRepository : OnlineQuizRepository<Student> {
        OnlineQuizContext context;
        public StudentRepository(OnlineQuizContext _context) : base(_context) {
            _context.Students.Select(e => e.TakeClasses).Load();
            _context.Students.Select(e => e.TakeExams).Load();
            context = _context;
        }
        public Student? FindByMssv(string key) => Set.FirstOrDefault(e => e.Mssv.ToLower().Equals(key.ToLower()));

        public async Task<List<Class>> GetClassListAsync(string key) {
            List<Class> classList = new List<Class>();

            var s = await Find(key);
            if (s != null) {
                context.Students.Include(e => e.TakeClasses).ThenInclude(e => e.Class).Load();
                context.Classes.Include(e => e.Course).Load();
                foreach (var c in s.TakeClasses) {
                    classList.Add(c.Class);
                }
            }

            return classList;
        }
    }
}
