﻿using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;

namespace QuizSystem.Repositories {
    public class LecturerRepository : OnlineQuizRepository<Lecturer> {
        OnlineQuizContext context;
        public LecturerRepository(OnlineQuizContext _context) : base(_context) {
            _context.Lecturers.Select(e => e.ClassCoWorkerNavigations).Load();
            _context.Lecturers.Select(e => e.ClassLecturerAccountNavigations).Load();
            context = _context;
        }

        public async Task<List<Class>> GetClassListAsync(string key) {
            List<Class> classList = new List<Class>();

            var l = await Find(key);
            if (l != null) {
                context.Classes.Include(e => e.Course).Load();
                foreach (var c in l.ClassLecturerAccountNavigations) {
                    classList.Add(c);
                }
                foreach (var c in l.ClassCoWorkerNavigations) {
                    if (!classList.Contains(c))classList.Add(c);
                }
            }

            return classList;   
        }
    }
}
