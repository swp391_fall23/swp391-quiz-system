﻿using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;

namespace QuizSystem.Repositories {
    public class ClassRepository : OnlineQuizRepository<Class> {
        public ClassRepository(OnlineQuizContext _context) : base(_context) {
            _context.Classes.Include(e => e.Course).Load();
            _context.Classes.Include(e => e.LecturerAccountNavigation).Load();
            _context.Classes.Include(e => e.CoWorkerNavigation).Load();
        }

        [Obsolete]
        public override Task<Class?> Find(string key) {
            return base.Find(key);
        }

        public async Task<Class?> Find(int key) => await Set.FindAsync(key);
    }
}
