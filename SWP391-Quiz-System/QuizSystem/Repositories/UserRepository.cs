﻿using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;
using QuizSystem.Services.ExtensionMethods;
using QuizSystem.Services.PasswordGeneration;
using System.CodeDom.Compiler;
using System.Linq.Expressions;

namespace QuizSystem.Repositories {
    public class UserRepository : OnlineQuizRepository<User> {
        public UserRepository(OnlineQuizContext _context) : base(_context) {
            _context.Users.Include(e => e.Role).Load();
            _context.Users.Include(e => e.Student).Load();
            _context.Users.Include(e => e.Manager).Load();
            _context.Users.Include(e => e.Lecturer).Load();
            _context.Users.Include(e => e.Admin).Load();
        }

        public async Task<bool> IsEmailAvailable (string email) => !await Set.AnyAsync(e => e.Email == email);
        public async Task<bool> IsAccountAvailable (string account) => !await Set.AnyAsync(e => e.Account == account);

        public async Task<string> ResetPassword(string key) {
            User? user = await Find(key);
            if (user == null) throw new Exception("Cannnot find User. Account might be deleted or changed.");

            string password = GenerateRandomPassword(10,10,false);
            user.Password = password.HashPassword();
            await Update(user);

            return password;
        }

        public async override Task<User?> Find(string key) {
            if ((await base.Find(key)) != null) return await base.Find(key);
            if ((await FindUserByEmail(key)) != null) return await FindUserByEmail(key);
            return null;
        }

        public async Task<User?> FindUserByEmail(string email) => await Set.FirstOrDefaultAsync(e => e.Email == email);

        public async Task<bool> CheckLoginCredentials (string username, string password) {
            if (username == null || password == null) return false;
            if (username.Length == 0 || password.Length == 0) return false;

            User? u = await Find(username);
            if (u != null 
                && (BCrypt.Net.BCrypt.Verify(password, u.Password) || password.Equals(u.Password))
                ) return true;
                        
            return false;
        }

        public string GenerateRandomPassword(int minLength = 8, int maxLength = 16, bool encryptResult = true) {
            Random rand = new Random();
            return new PasswordGenerate(CharacterSet.LettersOnly).GenerateStringWithRequirement(rand.Next(minLength, maxLength),encryptResult,
                CharacterSet.LowerCaseLetters, CharacterSet.UpperCaseLetters, CharacterSet.Punctuations, CharacterSet.NumbersOnly);
        }
    }
}
