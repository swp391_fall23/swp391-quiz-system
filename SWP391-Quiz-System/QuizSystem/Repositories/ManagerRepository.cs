﻿using QuizSystem.Models;

namespace QuizSystem.Repositories {
    public class ManagerRepository : OnlineQuizRepository<Manager> {
        public ManagerRepository(OnlineQuizContext _context) : base(_context) {
        }
    }
}
