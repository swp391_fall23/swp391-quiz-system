﻿
using QuizSystem.Models;
using QuizSystem.Repositories;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace QuizSystem.Services.ValidationService {
    public class ValidationService {
        #region Private Properties
        private string? email;
        private string? account;
        private string? phone;
        private string? password;
        private string? confirmPassword;
        private string? major;
        private DateTime? dob;
        private string? name;
        private string? mssv;
        #endregion

        #region Public Properties
        public string? Mssv {
            get { return mssv; }
            set { mssv = value; }
        }

        public string? Name {
            get { return name; }
            set { name = value; }
        }

        public string? Email {
            get { return email; }
            set { email = value; }
        }

        public string? Account {
            get { return account; }
            set { account = value; }
        }

        public string? Phone {
            get { return phone; }
            set { phone = value; }
        }

        public string? Password {
            get { return password; }
            set { password = value; }
        }

        public string? ConfirmPassword {
            get { return confirmPassword; }
            set { confirmPassword = value; }
        }

        public string? Major {
            get { return major; }
            set { major = value; }
        }

        public DateTime? DoB {
            get { return dob; }
            set { dob = value; }
        }
        #endregion

        UserRepository users = new UserRepository(new OnlineQuizContext());
        StudentRepository students = new StudentRepository(new OnlineQuizContext());

        public async Task<VResult> ValidateAll(bool isExist = false, bool isFU = true, bool ignoreNull = true) {
            VResult result = new VResult();

            if (!ignoreNull) {
                bool isSet = false;
                foreach (var p in this.GetType().GetProperties()) {
                    if (this.GetType().GetProperty(p.Name) == null || this.GetType().GetProperty(p.Name)?.GetValue(this) != null) {
                        isSet = true;
                        break;
                    }
                }
                if (!isSet) {
                    foreach (var p in this.GetType().GetProperties()) {
                        result.GetType().GetProperty(p.Name)?.SetValue(p.Name, ValidResult.Null);
                    }
                    result.Errors.Add("No properties where set.");
                    return result;
                }
            }

            foreach (var property in this.GetType().GetProperties()) {
                if (this.GetType().GetProperty(property.Name) == null || this.GetType().GetProperty(property.Name)?.GetValue(this) != null) {
                    bool r;

                    switch (property.Name) {
                        case "Email":
                            r = ValidateEmail(out var emailError, isFU);
                            if (emailError != null) result.Errors.Add(emailError);
                            break;
                        case "Account":
                            r = ValidateAccount(out var accountError, isFU);
                            if (accountError != null) result.Errors.Add(accountError);
                            break;
                        case "Mssv":
                            r = ValidateMssv(out var mssvError);
                            if (mssvError != null) result.Errors.Add(mssvError);
                            break;
                        case "Phone":
                            r = ValidatePhone(out var phoneError);
                            if (phoneError != null) result.Errors.Add(phoneError);
                            break;
                        case "Name":
                            r = ValidateName(out var nameError);
                            if (nameError != null) result.Errors.Add(nameError);
                            break;
                        case "Password":
                            r = ValidatePassword(out var passError);
                            if (passError != null) result.Errors.Add(passError);
                            break;
                        case "ConfirmPassword":
                            r = ValidateConfirmPassword();
                            if (!r) result.Errors.Add("Password and Confirm password do not match.");
                            break;
                        case "Major":
                            r = ValidateMajor(out var majorError);
                            if (majorError != null) result.Errors.Add(majorError);
                            break;
                        case "DoB":
                            r = ValidateDoB(out var dobError);
                            if (dobError != null) result.Errors.Add(dobError);
                            break;
                        default:
                            throw new NotImplementedException();
                    }
                    result.GetType().GetProperty(property.Name)?.SetValue(result, r ? ValidResult.Success : ValidResult.Failed);
                }
                else {
                    if (ignoreNull) {
                        result.GetType().GetProperty(property.Name)?.SetValue(result, ValidResult.Success);
                    }
                    else {
                        result.GetType().GetProperty(property.Name)?.SetValue(result, ValidResult.Null);
                    }
                }
            }

            result = await validateDataInDB(result);

            return result;
            async Task<VResult> validateDataInDB(VResult result) {
                User? user = null;
                User? userByEmail = null;

                if (Account != null && !await users.IsAccountAvailable(Account)) user = await users.Find(Account);
                if (Email != null && !await users.IsEmailAvailable(Email)) userByEmail = await users.Find(Email);

                if (user != userByEmail) {
                    result.Email = ValidResult.Failed;
                    result.Errors.Add("Email is already used by another account");
                }

                if (!isExist && user != null) {
                    result.Account = ValidResult.Failed;
                    result.Errors.Add("Account is already used");
                }
                else if (isExist && user == null) {
                    result.Account = ValidResult.Failed;
                    result.Errors.Add("Account is not found");
                }

                if (!isExist && userByEmail != null) {
                    result.Email = ValidResult.Failed;
                    result.Errors.Add("Email is already used");
                }
                else if (isExist && userByEmail == null) {
                    result.Email = ValidResult.Failed;
                    result.Errors.Add("Email is not found");
                }
                if (Mssv != null) {
                    if (isExist && students.FindByMssv(Mssv) == null) {
                        result.Mssv = ValidResult.Failed;
                        result.Errors.Add("Mssv doesn't exist");
                    }
                    else if (!isExist && students.FindByMssv(Mssv) != null) {
                        result.Mssv = ValidResult.Failed;
                        result.Errors.Add("Mssv is already exist");
                    }
                }
                return result;
            }
        }

        public bool ValidateName(out string? error) => ValidateName(Name, out error);

        public bool ValidateDoB(out string? error) => ValidateDoB(DoB, out error);
        public bool ValidateMajor(out string? error) => ValidateMajor(Major, out error);
        public bool ValidateConfirmPassword() => ConfirmPassword != null && Password != null && ConfirmPassword.Equals(Password);
        public bool ValidatePassword(out string? error) => ValidatePassword(Password, out error);
        public bool ValidatePhone(out string? error) => ValidatePhone(Phone, out error);
        public bool ValidateAccount(out string? error, bool isFU = true) => ValidateAccount(Account, out error, isFU);
        public bool ValidateEmail(out string? error, bool isFU = true) => ValidateEmail(Email, out error, isFU);
        public bool ValidateMssv(out string? error) => ValidateMssv(Mssv, out error);

        public bool ValidateDoB(DateTime? dob, out string? Error) {
            Error = "Missing DoB (Date of Birth).";
            if (dob == null) return false;
            DateTime bd = (DateTime)dob;
            DateTime now = DateTime.Now;

            if (bd.Year > now.Year - 18 || bd.Year < now.Year - 100) {
                Error = "Student's age must be between 18 and 100.";
                return false;
            }

            Error = null;
            return true;
        }

        public bool ValidateMajor(string? major, out string? Error) {
            Error = "Missing Major.";
            if (major == null) return false;
            if (!Models.Majors.MajorList.Majors.Contains(major)) {
                Error = "Cannot find major.";
                return false;
            }

            Error = null;
            return true;

        }

        public bool ValidatePassword(string? password, out string? Error) {
            Error = "Missing Password.";
            if (password == null) return false;
            Regex reg = new Regex("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})");
            if (!reg.IsMatch(password)) {
                Error = "Password format is incorrect";
                return false;
            }
            Error = null;

            return true;
        }

        public bool ValidateName(string? name, out string? Error) {
            Error = "Missing Name.";
            if (name == null) return false;
            Regex reg = new Regex("^[a-zA-Z ]+$");
            if (!reg.IsMatch(name)) {
                Error = "Name format is incorrect";
                return false;
            }
            Error = null;
            return true;
        }

        public bool ValidatePhone(string? phone, out string? Error) {
            Error = "Missing Phone number.";
            if (phone == null) return false;
            Regex reg = new Regex("^0[0-9]{9}$");
            if (!reg.IsMatch(phone)) {
                Error = "Phone number format is incorrect";
                return false;
            }
            Error = null;
            return true;
        }

        public bool ValidateAccount(string? account, out string? Error, bool isFU = true) {
            Error = "Missing Account.";
            if (account == null) return false;
            Regex reg = isFU ? new Regex("^[a-zA-Z]+[a-zA-Z]{2}\\d{6}$") : new Regex("^[a-zA-Z]{2,}\\d{0,}$");
            if (!reg.IsMatch(account)) {
                Error = "Username format is incorrect";
                return false;
            }

            //if (await users.Find(account) != null) return (false, "Username is already used.");
            Error = null;
            return true;
        }

        public bool ValidateMssv(string? mssv, out string? Error) {
            Error = "Missing Mssv.";
            if (mssv == null) return false;
            Regex reg = new Regex("^[a-zA-Z]{2}\\d{6}$");
            if (!reg.IsMatch(mssv)) {
                Error = "Mssv format is incorrect";
                return false;
            }

            //if (students.FindByMssv(mssv) != null) return (false, "Mssv is already used.");

            Error = null;
            return true;
        }

        public bool ValidateEmail(string? email, out string? Error, bool isFU = true) {
            Error = "Missing Email.";
            if (email == null) return false;
            Regex reg = isFU ? new Regex("^[a-zA-Z]+(he|hs|se)\\d{6}@fpt[.-]edu[.-]vn$") : new Regex("^\\w+([.-]?\\w+)*@\\w+([.-]?\\w+)*(.\\w{2,3})+$");
            if (!reg.IsMatch(email)) {
                Error = "Email format is incorrect.";
                return false;
            }

            //if (!await users.IsEmailAvailable(email)) return (false, "Email is already used.");
            Error = null;
            return true;
        }
    }
}
