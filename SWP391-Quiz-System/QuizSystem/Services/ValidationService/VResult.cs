﻿namespace QuizSystem.Services.ValidationService {
    public class VResult {
        #region Private Properties
        private ValidResult email;
        private ValidResult account;
        private ValidResult phone;
        private ValidResult password;
        private ValidResult confirmPassword;
        private ValidResult major;
        private ValidResult dob;
        private ValidResult name;
        private ValidResult mssv;
        #endregion

        #region Public Properties

        public ValidResult Mssv {
            get { return mssv; }
            set { mssv = value; }
        }

        public ValidResult Name {
            get { return name; }
            set { name = value; }
        }

        public ValidResult Email {
            get { return email; }
            set { email = value; }
        }

        public ValidResult Account {
            get { return account; }
            set { account = value; }
        }

        public ValidResult Phone {
            get { return phone; }
            set { phone = value; }
        }

        public ValidResult Password {
            get { return password; }
            set { password = value; }
        }

        public ValidResult ConfirmPassword {
            get { return confirmPassword; }
            set { confirmPassword = value; }
        }

        public ValidResult Major {
            get { return major; }
            set { major = value; }
        }

        public ValidResult DoB {
            get { return dob; }
            set { dob = value; }
        }
        #endregion

        public List<string> Errors = new List<string>();
    }
    public enum ValidResult {
        Success,
        Failed,
        Null
    }
}
