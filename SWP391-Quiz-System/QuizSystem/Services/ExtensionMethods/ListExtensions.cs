﻿namespace QuizSystem.Services.ExtensionMethods {
    public static class ListExtensions {
        public static void Shuffle<T> (this IList<T> list) {
            Random rand = new Random();
            int n = list.Count;
            while (n > 1) {
                n--;
                int k = rand.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static List<T> GetPage<T> (this IList<T> list, int page, int pageSize = 10) => list.Skip((page - 1) * pageSize).Take(pageSize).ToList();
    }
}
