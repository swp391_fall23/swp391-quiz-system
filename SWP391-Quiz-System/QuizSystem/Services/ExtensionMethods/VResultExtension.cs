﻿using QuizSystem.Services.ValidationService;

namespace QuizSystem.Services.ExtensionMethods {
    public static class VResultExtension {
        public static bool IsPassed(this VResult result) {
            foreach (var p in typeof(VResult).GetProperties()) {
                if ((ValidResult)result.GetType().GetProperty(p.Name).GetValue(result) != ValidResult.Success) {
                    Console.WriteLine(p.Name );
                    foreach (var s in result.Errors) {
                        Console.WriteLine(s);
                    }
                    return false;
                }
            }
            return true;
        }
    }
}
