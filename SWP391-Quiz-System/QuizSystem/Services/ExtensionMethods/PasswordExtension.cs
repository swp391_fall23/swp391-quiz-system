﻿using BCrypt.Net;

namespace QuizSystem.Services.ExtensionMethods {
    public static class PasswordExtension {
        public static string HashPassword(this string password) => BCrypt.Net.BCrypt.HashPassword(password);

        public static bool VerifyHashedPassword(this string hash, string password) {
            try {
                return BCrypt.Net.BCrypt.Verify(password, hash);
            }
            catch { return false; }
        }
    }
}
