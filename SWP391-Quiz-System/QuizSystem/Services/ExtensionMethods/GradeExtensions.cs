﻿namespace QuizSystem.Services.ExtensionMethods {
    public static class GradeExtensions {
        public static (decimal score, decimal total) ToExamGrade(this (decimal score, decimal total) score, decimal? examTotal) {
            if (examTotal == null) return score;
            decimal examScore = score.score / score.total * (decimal)examTotal;
            examScore *= 100;
            examScore = Math.Round(examScore);
            examScore = decimal.Truncate(examScore);
            examScore /= 100;

            return (examScore, (decimal)examTotal);
        }
    }
}
