﻿namespace QuizSystem.Services.MailingService {
    public enum MailObjective {
        ForgotPassword,
        CreateAccount,
        AccountCreateByAdmin
    }
}
