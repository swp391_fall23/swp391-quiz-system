﻿using Humanizer;
using Microsoft.Extensions.Options;
using MimeKit;
using QuizSystem.Models;
using QuizSystem.Models.Mailing;
using QuizSystem.Services.Encryption;
using System.Web;

namespace QuizSystem.Services.MailingService {
    public partial class SendMailService : ISendMailService {
        private static MailSettings mailSettings;
        private static string filePath = ".\\wwwroot\\MailTemplate.html";
        private static string template = System.IO.File.ReadAllText(filePath);
        private static string projectName = "FPT Quiz";
        private static IHttpContextAccessor httpContext;

        public SendMailService(IOptions<MailSettings> _mailSettings, IHttpContextAccessor _httpContextAccessor) {
            mailSettings = _mailSettings.Value;
            httpContext = _httpContextAccessor;
        }

        public async Task<bool> SendMail(MailContent content) {
            var email = new MimeMessage() {
                Sender = new MailboxAddress(mailSettings.DisplayName, mailSettings.Mail),
                Subject = content.Subject
            };
            email.From.Add(new MailboxAddress(mailSettings.DisplayName, mailSettings.Mail));
            email.To.Add(MailboxAddress.Parse(content.To));

            var builder = new BodyBuilder();
            builder.HtmlBody = content.Body;
            email.Body = builder.ToMessageBody();

            using var smtp = new MailKit.Net.Smtp.SmtpClient();

            try {
                smtp.Connect(mailSettings.Host, mailSettings.Port, MailKit.Security.SecureSocketOptions.StartTls);
                smtp.Authenticate(new System.Net.NetworkCredential(mailSettings.Mail, mailSettings.AppPassword));
                //smtp.Authenticate(mailSettings.Mail, mailSettings.Password);
                await smtp.SendAsync(email);
            }
            catch (Exception ex) {
                await Console.Out.WriteLineAsync(ex.Message);
                return false;
            }
            smtp.Disconnect(true);

            await Console.Out.WriteLineAsync($"Mail sent. - to {content.To}");
            return true;
        }

        public async Task<bool> SendMailAsync(string email, string subject, string htmlMessage) {
            return await SendMail(new MailContent() {
                To = email,
                Subject = subject,
                Body = htmlMessage
            });
        }

        private static Dictionary<MailObjective, (string Subject, string BodyContent)> contentDictionary = new Dictionary<MailObjective, (string Subject, string BodyContent)> {
            {
                MailObjective.ForgotPassword,
                ($"Password Reset for {projectName}", "Your {1}'s password is reset. <br /><br /> Your new password is: <strong>{2}</strong><br /><br />Please navigate to the Reset password page or click on <a href=\"{0}\">this link</a> to update your password.")
            },
            {
                MailObjective.CreateAccount,
                ($"New {projectName} account Created", "A new {0}'s account has been created using {1}. <br/>Thank you for studying with us!")
            },
            {
                MailObjective.AccountCreateByAdmin,
                ($"New {projectName} account Created", "A new {1}'s account has been created for you. You can log in using <a href=\"{3}\">this link</a> or use <a href=\"{0}\">this link</a> to change your password. <br /><br /> Your password is: <strong>{2}</strong>")
            },
        };

        public async Task<bool> SendMailWithTemplateAsync(User user, MailObjective objective) {
            if (contentDictionary.TryGetValue(objective, out var content)) {
                var mail = new MailContent() {
                    To = user.Email,
                    Subject = content.Subject,
                    Body = string.Format(template,
                        user.Account, projectName, GetMailContent(objective, user))
                };
                return await SendMail(mail);
            }
            else throw new NotImplementedException("Objective not implemented.");
        }

        private string GetMailContent(MailObjective objective, User? user = null) {
            string domain = $"{httpContext.HttpContext.Request.Scheme}://{httpContext.HttpContext.Request.Host}";
            switch (objective) {
                case MailObjective.ForgotPassword:
                    return string.Format(contentDictionary[objective].BodyContent, domain + "/Login/Reset_Password?u=" + HttpUtility.UrlEncode(user.Email.Encrypt()) + "&data=" + HttpUtility.UrlEncode(user.Password.Encrypt()), projectName, user.Password);
                case MailObjective.CreateAccount:
                    return string.Format(contentDictionary[objective].BodyContent, projectName, user.Email);
                case MailObjective.AccountCreateByAdmin:
                    return string.Format(contentDictionary[objective].BodyContent, domain + "/Login/Reset_Password?u=" + HttpUtility.UrlEncode(user.Email.Encrypt()) + "&data=" + HttpUtility.UrlEncode(user.Password.Encrypt()), projectName, user.Password, domain + "/Login/Login");
                default:
                    throw new NotImplementedException();
            }
        }

    }
}
