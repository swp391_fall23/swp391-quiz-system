﻿using QuizSystem.Models;
using QuizSystem.Models.Mailing;

namespace QuizSystem.Services.MailingService
{
    public interface ISendMailService {
        Task<bool> SendMail(MailContent content);
        Task<bool> SendMailAsync(string email, string subject, string htmlMessage);
        Task<bool> SendMailWithTemplateAsync(User user, MailObjective objective);
    }
}
