﻿using QuizSystem.Models;
using QuizSystem.Models.ExamQ;
using Microsoft.EntityFrameworkCore;

namespace QuizSystem.Services.ExamGradingService {
    public class GradeExam {
        OnlineQuizContext context;

        public GradeExam(OnlineQuizContext _context) {
            context = _context;
        }

        public async Task<(decimal score, decimal total)> Grade(List<EQuestion> _work, int _examId) {
            List<QuestionExam> questions = context.QuestionExams.Include(e => e.QuestionExamAnswers).Where(e => e.ExamId == _examId).ToList();

            decimal score = 0;
            decimal total = await GetTotalScore(_examId);    


            foreach (var question in questions) {
                if (_work.Any(e => e.QuestionId == question.QuesId)) {
                    if (question.Type ?? true) {
                        score += GradeMultipleChoice(question.Score ?? 0, _work.FirstOrDefault(e => e.QuestionId == question.QuesId), question.QuestionExamAnswers.ToList());
                    }
                    else {
                        score += GradeSelectOne(question.Score ?? 0, _work.FirstOrDefault(e => e.QuestionId == question.QuesId), question.QuestionExamAnswers.ToList());
                    }
                }
            }
            score = Round(score) < total ? Round(score) : total;

            return (score, total);
        }

        public async Task<decimal> GetTotalScore(int examId) {
            List<QuestionExam> questions = await context.QuestionExams.Include(e => e.QuestionExamAnswers).Where(e => e.ExamId == examId).ToListAsync();
            decimal total = decimal.Truncate(questions.Sum(e => e.Score ?? 0));
            return total;
        }

        private decimal Round(decimal score) {
            score = score * 100;
            score = decimal.Truncate(score);
            score = Math.Round(score);
            return score / 100;
        }

        private decimal GradeSelectOne(decimal questionScore, EQuestion eQuestion, List<QuestionExamAnswer> questionExamAnswers) {
            if (eQuestion.Answers.Count() != 1)  return 0;
            if (questionExamAnswers.FirstOrDefault(e => e.Correct == true)?.AnswerId == null) return 0;
            if (questionExamAnswers.FirstOrDefault(e => e.Correct == true).AnswerId == eQuestion.Answers.First().AnswerId) return /* (questionExamAnswers.FirstOrDefault(e => e.Correct == true)?.Percent ?? 0) * */ questionScore /* / 100*/;
            return 0;
        }

        private decimal GradeMultipleChoice(decimal questionScore, EQuestion eQuestion, List<QuestionExamAnswer> questionExamAnswers) {
            decimal score = 0;
            decimal total = 0;
            foreach (var answer in questionExamAnswers) {
                if (answer.Correct ?? false) {
                    if (eQuestion.Answers.Any(e => e.AnswerId == answer.AnswerId)) score += answer.Percent ?? 0;
                    total += answer.Percent ?? 0;
                }
                else {
                    if (eQuestion.Answers.Any(e => e.AnswerId == answer.AnswerId)) return 0;
                }
            }

            return (score / total) * questionScore ;
        }
    }
}
