﻿namespace QuizSystem.Services.PasswordGeneration {
    public enum CharacterSet {
        LowerCaseLetters,
        UpperCaseLetters,
        LettersOnly,
        LettersAndNumbers,
        NumbersOnly, 
        Punctuations,
        Default,
        AllowsAllCharacters
    }
}
