﻿using QuizSystem.Services.ExtensionMethods;

namespace QuizSystem.Services.PasswordGeneration {
    public class PasswordGenerate {

        private int _asciiStart;
        private int _asciiEnd;
        private List<char> _bannedCharacters;
        private Random _rand = new Random();

        private CharacterSet _characterSet;
        public CharacterSet CharacterSet { 
            get => _characterSet;
            private set {
                ChangeCharacterSet(value);
                _characterSet = value;
            }
        }

        public PasswordGenerate() {
            CharacterSet = CharacterSet.Default;
        }

        public PasswordGenerate(CharacterSet specific) {
            CharacterSet = specific;
        }

        private (int asciiStart, int asciiEnd, List<char> bannedCharacters) GetCharacterSet(CharacterSet specific) {
            int start = 33;
            int end = 122;
            List<char> banned = new List<char>() {
                '"', '\'', '(', ')', '[', ']', '{', '}', '>', '<', '/', '\\', '|', ' '
            };
            switch (specific) {
                case CharacterSet.LowerCaseLetters:
                    return (asciiStart: 97, asciiEnd: 122, bannedCharacters: banned);
                case CharacterSet.UpperCaseLetters:
                    return (asciiStart: 65, asciiEnd: 90, bannedCharacters: banned);
                case CharacterSet.LettersOnly:
                    for (int i = start; i <= end; i++) {
                        char c = Convert.ToChar(i);
                        if (!char.IsLetter(c) && !banned.Contains(c)) {
                            banned.Add(Convert.ToChar(i));
                        }
                    }
                    return (asciiStart: start, asciiEnd: end, bannedCharacters: banned);
                case CharacterSet.LettersAndNumbers:
                    for (int i = start; i <= end; i++) {
                        char c = Convert.ToChar(i);
                        if (!char.IsLetterOrDigit(c) && !banned.Contains(c)) {
                            banned.Add(Convert.ToChar(i));
                        }
                    }
                    return (asciiStart: start, asciiEnd: end, bannedCharacters: banned);
                case CharacterSet.NumbersOnly:
                    return (asciiStart: 48, asciiEnd: 57, bannedCharacters: banned);
                case CharacterSet.Punctuations:
                    for (int i = start; i <= end; i++) {
                        char c = Convert.ToChar(i);
                        if (!char.IsPunctuation(c) && !banned.Contains(c)) {
                            banned.Add(Convert.ToChar(i));
                        }
                    }
                    return (asciiStart: start, asciiEnd: end, bannedCharacters: banned);
                case CharacterSet.Default:
                    return (asciiStart: start, asciiEnd: end, bannedCharacters: banned);
                case CharacterSet.AllowsAllCharacters:
                    return (asciiStart: start, asciiEnd: end, bannedCharacters: new List<char>());
                default:
                    throw new NotImplementedException("Characters set not implemented.");
            }
        }

        private void ChangeCharacterSet(CharacterSet specific) {
            var set = GetCharacterSet(specific);
            _asciiStart = set.asciiStart; 
            _asciiEnd = set.asciiEnd;
            _bannedCharacters = set.bannedCharacters;
        }

        /// <summary>
        /// Returns a characters from a specific character set.
        /// <param name="set">A specific set of characters.</param>
        /// </summary>
        /// <returns>A valid character for the character set.</returns>
        public char GenerateCharacter(CharacterSet? set = null) {
            if (set == null) set = _characterSet;
            char gen;
            do {
                gen = Convert.ToChar(_rand.Next(_asciiStart, _asciiEnd));
            }
            while (!HasCharacter(gen.ToString(), (CharacterSet)set));

            return gen;
        }

        /// <summary>
        /// Check if the string contains certain character from a specific character set.
        /// </summary>
        /// <param name="password">Input string to check.</param>
        /// <param name="set">A specific set of characters.</param>
        /// <returns>True if the string contain valid characters type (exclude banned characters: """, "'", "(", ")", "[", "]", "{", "}", "<", ">", "/", "\", "|", " "), and false otherwise.</returns>
        public bool HasCharacter(string password, CharacterSet set) {
            var _set = GetCharacterSet(set);
            foreach (char c in password) {
                int ascii = Convert.ToInt32(c);
                if (ascii >= _set.asciiStart && ascii <= _set.asciiEnd && !_set.bannedCharacters.Contains(c)) return true;
            }
            return false;
        }

        /// <summary>
        /// Check if the string contains certain character from your current character set.
        /// </summary>
        /// <param name="password">Input string to check.</param>
        /// <returns></returns>
        public bool HasCharacter(string password) {
            foreach (char c in password) {
                int ascii = Convert.ToInt32(c);
                if (ascii >= _asciiStart && ascii <= _asciiEnd && !_bannedCharacters.Contains(c)) return true;
            }
            return false;
        }

        /// <summary>
        /// Generate a plain string with all available characters from your current character set
        /// </summary>
        /// <param name="length">The length of the desired string.</param>
        /// <returns>A randomly generated string from all available characters.</returns>
        public string GenerateString(int length, bool encryptResult = false) {
            string s = "";
            while (length > 0) {
                s += GenerateCharacter();
                length--;
            }

            if (encryptResult) return s.HashPassword();
            else return s;
        }

        /// <summary>
        /// Generate a plain string with all available characters from your current character set
        /// </summary>
        /// <param name="length">The length of the desired string.</param>
        /// <param name="set">The set of character in which you wish to generate your string from</param>
        /// <returns>A randomly generated string from all available characters.</returns>
        public string GenerateString(int length, CharacterSet set, bool encryptResult = false) => new PasswordGenerate(set).GenerateString(length, encryptResult);

        /// <summary>
        /// Generate a plain string with all available characters from your current character set that also includes characters from your required set.
        /// </summary>
        /// <param name="length">The length of the desired string.</param>
        /// <param name="requirements">Randomly add one character per set for each requirement to your string.</param>
        /// <returns>A randomly generated string from all available characters with all required characters.</returns>
        /// <exception cref="ArgumentException">Throws when the string length is less than the required characters.</exception>
        public string GenerateStringWithRequirement(int length, bool encryptResult = false, params CharacterSet[] requirements) {
            if (length < requirements.Length) throw new ArgumentException("Password string too short to fit all requirement criterias.");

            List<char> password = new List<char>();
            foreach (var req in requirements) {
                password.Add(GenerateCharacter(req));
            }
            for (int i = 0; i < length - requirements.Length; i++) {
                password.Add(GenerateCharacter());
            }

            password.Shuffle();

            if (encryptResult) return new string(password.ToArray()).HashPassword();
            return new string(password.ToArray());
        }
    }
}
