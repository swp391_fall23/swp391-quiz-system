﻿using System.Data;
using System.Security.Claims;
using System.Web;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.Google;
//using Microsoft.AspNetCore.Authentication.Google;
using Microsoft.AspNetCore.Mvc;
using QuizSystem.Models;
using QuizSystem.Services.Encryption;
using QuizSystem.Services.ValidationService;

namespace QuizSystem.Controllers {
    public class LoginController : Controller {
        OnlineQuizContext onlineQuizContext;
        public LoginController(OnlineQuizContext _context) {
            onlineQuizContext = _context;
        }
        public IActionResult Index() {
            return RedirectToPage("/Login/Login");
        }

        public async Task LoginWithGoogle() {
            await HttpContext.ChallengeAsync(GoogleDefaults.AuthenticationScheme, new AuthenticationProperties() {
                RedirectUri = Url.Action("GoogleResponse")
            });

        }

        public async Task<IActionResult> GoogleResponse() {
            var result = await HttpContext.AuthenticateAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            var claims = result.Principal.Identities.FirstOrDefault().Claims.Select(claim => new {
                claim.Issuer,
                claim.OriginalIssuer,
                claim.Type,
                claim.Value
            });
            string a = "";

            foreach (var c in claims)
            {
                if (c.Type.Substring(c.Type.LastIndexOf("/") + 1) == "emailaddress")
                {
                    a = c.Value;
                }
            }

            //return Json(a); //=> dong nay dung de test xem da lay dc email ra chua

            User? user = onlineQuizContext.Users.FirstOrDefault(user => user.Email == a);
            //if (user == null && (new ValidationService().ValidateEmail(a, out var error))) {
            //    await Logout();
            //    return Redirect("~/Login/Create_Account?u=" + HttpUtility.UrlEncode(a.Encrypt()));
            //}

            //check if mail has @fpt.edu.vn
            if (
                //(a.Substring(a.IndexOf("@"), a.Length - a.IndexOf("@")) != "@fpt.edu.vn"
                //&& a.Substring(a.IndexOf("@"), a.Length - a.IndexOf("@")) != "@fe.edu.vn")
                //||
                user == null) {
                await HttpContext.SignOutAsync();
                return Redirect("~/Login/Login");
            }
            //create session
            HttpContext.Session.SetString("user", user.Email);
            HttpContext.Session.SetString("account", user.Account);
            HttpContext.Session.SetString("role", user.RoleId.ToString());

            switch (user.RoleId) {
                case 1:
                    return Redirect("~/Index");
                    break;
                case 2:
                    return Redirect("~/Index");
                    break;
                case 3:
                    return Redirect("~/Index");
                    break;
                case 4:
                    return Redirect("~/Dashboard/StudentDashboard");
                    break;
            }
            return Redirect("~/Login/Login");
        }

        public async Task<IActionResult> Logout() {
            await HttpContext.SignOutAsync();
            HttpContext.Session.Remove("user");
            HttpContext.Session.Remove("role");
            return Redirect("~/Login/Login");
        }
    }
}
