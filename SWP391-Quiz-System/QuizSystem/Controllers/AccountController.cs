﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using QuizSystem.Models;
using QuizSystem.Services.Encryption;
using QuizSystem.Services.ValidationService;
using System.Web;

namespace QuizSystem.Controllers {
    public class AccountController : Controller {
        private OnlineQuizContext context;
        public AccountController(OnlineQuizContext _context) {
            context = _context;
        }
        public async Task<IActionResult> AccessDenied(string returnUrl) {
            TempData["Color"] = "danger";
            if (User.Identity != null && !User.Identity.IsAuthenticated) {
                TempData["Message"] = "You are not logged in. Please log in and try again";
                return RedirectToPage("/Login/Login");
            }
            var lastPage = "../Index";
            //var referer = Request.Headers["Referer"];
            //if (!StringValues.IsNullOrEmpty(referer)) lastPage = referer.Last() ?? "../";

            TempData["Message"] = "You don't have permission to view this content.";


            
            string? a = HttpContext.Session.GetString("account");
             
            User? user = null;
            if (a != null) {
                user = context.Users.FirstOrDefault(user => user.Email.Equals(a) || user.Account.Equals(a));
            }
            if (user == null) {
                return RedirectToAction("Logout", "Login");
            }
            if (!user?.IsActive ?? false) {
                TempData["Message"] = "Your account is disabled! Please contact the admin for more information!";
                return RedirectToAction("Logout", "Login");
            }
            return Redirect(lastPage);
        }
    }
}
