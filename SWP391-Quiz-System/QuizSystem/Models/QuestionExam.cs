﻿using System;
using System.Collections.Generic;

namespace QuizSystem.Models;

public partial class QuestionExam
{
    public int? ExamId { get; set; }

    public int QuesId { get; set; }

    public string? Content { get; set; }

    public bool? Type { get; set; }

    public decimal? Score { get; set; }

    public virtual Exam? Exam { get; set; }

    public virtual ICollection<QuestionExamAnswer> QuestionExamAnswers { get; set; } = new List<QuestionExamAnswer>();

    public virtual ICollection<TakeAnswer> TakeAnswers { get; set; } = new List<TakeAnswer>();
}
