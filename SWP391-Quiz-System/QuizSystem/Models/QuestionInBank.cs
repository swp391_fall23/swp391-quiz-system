﻿using System;
using System.Collections.Generic;

namespace QuizSystem.Models;

public partial class QuestionInBank
{
    public int QuesId { get; set; }

    public string? Content { get; set; }

    public bool? Type { get; set; }

    public int? CourseId { get; set; }

    public virtual Course? Course { get; set; }

    public virtual ICollection<QuestionBankAnswer> QuestionBankAnswers { get; set; } = new List<QuestionBankAnswer>();
}
