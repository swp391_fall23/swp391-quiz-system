﻿using System;
using System.Collections.Generic;

namespace QuizSystem.Models;

public partial class User
{
    public string Account { get; set; } = null!;

    public string Email { get; set; } = null!;

    public string Password { get; set; } = null!;

    public int? RoleId { get; set; }

    public bool? IsActive { get; set; }

    public virtual Admin? Admin { get; set; }

    public virtual Lecturer? Lecturer { get; set; }

    public virtual Manager? Manager { get; set; }

    public virtual ICollection<Notification> Notifications { get; set; } = new List<Notification>();

    public virtual Role? Role { get; set; }

    public virtual Student? Student { get; set; }
}
