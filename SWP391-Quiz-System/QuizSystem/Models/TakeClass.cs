﻿using System;
using System.Collections.Generic;

namespace QuizSystem.Models;

public partial class TakeClass
{
    public int Id { get; set; }

    public string? StudentAccount { get; set; }

    public int? ClassId { get; set; }

    public virtual Class? Class { get; set; }

    public virtual Student? StudentAccountNavigation { get; set; }
}
