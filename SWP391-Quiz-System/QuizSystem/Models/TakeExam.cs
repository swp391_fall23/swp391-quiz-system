﻿using System;
using System.Collections.Generic;

namespace QuizSystem.Models;

public partial class TakeExam
{
    public int TakeExamId { get; set; }

    public string? StudentAccount { get; set; }

    public int? ExamId { get; set; }

    public string? Status { get; set; }

    public decimal? Score { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public virtual Exam? Exam { get; set; }

    public virtual Student? StudentAccountNavigation { get; set; }

    public virtual ICollection<TakeAnswer> TakeAnswers { get; set; } = new List<TakeAnswer>();
}
