﻿using System;
using System.Collections.Generic;

namespace QuizSystem.Models;

public partial class TakeAnswer
{
    public int TakeAnswerId { get; set; }

    public int? TakeExamId { get; set; }

    public int? QuesId { get; set; }

    public int? AnswerId { get; set; }

    public virtual QuestionExamAnswer? Answer { get; set; }

    public virtual QuestionExam? Ques { get; set; }

    public virtual TakeExam? TakeExam { get; set; }
}
