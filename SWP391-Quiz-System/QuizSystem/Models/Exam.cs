﻿using System;
using System.Collections.Generic;

namespace QuizSystem.Models;

public partial class Exam
{
    public int ExamId { get; set; }

    public int? ClassId { get; set; }

    public string? LecturerAccount { get; set; }

    public string Title { get; set; } = null!;

    public string Summary { get; set; } = null!;

    public decimal? Score { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public int? Timer { get; set; }

    public int? TakingTimes { get; set; }

    public bool? Permission { get; set; }

    public virtual Class? Class { get; set; }

    public virtual ICollection<QuestionExam> QuestionExams { get; set; } = new List<QuestionExam>();

    public virtual ICollection<TakeExam> TakeExams { get; set; } = new List<TakeExam>();
}
