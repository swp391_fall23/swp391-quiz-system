﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace QuizSystem.Models;

public partial class OnlineQuizContext : DbContext
{
    public OnlineQuizContext()
    {
    }

    public OnlineQuizContext(DbContextOptions<OnlineQuizContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Admin> Admins { get; set; }

    public virtual DbSet<Class> Classes { get; set; }

    public virtual DbSet<Course> Courses { get; set; }

    public virtual DbSet<Exam> Exams { get; set; }

    public virtual DbSet<Lecturer> Lecturers { get; set; }

    public virtual DbSet<Manager> Managers { get; set; }

    public virtual DbSet<Notification> Notifications { get; set; }

    public virtual DbSet<QuestionBankAnswer> QuestionBankAnswers { get; set; }

    public virtual DbSet<QuestionExam> QuestionExams { get; set; }

    public virtual DbSet<QuestionExamAnswer> QuestionExamAnswers { get; set; }

    public virtual DbSet<QuestionInBank> QuestionInBanks { get; set; }

    public virtual DbSet<Role> Roles { get; set; }

    public virtual DbSet<Student> Students { get; set; }

    public virtual DbSet<TakeAnswer> TakeAnswers { get; set; }

    public virtual DbSet<TakeClass> TakeClasses { get; set; }

    public virtual DbSet<TakeExam> TakeExams { get; set; }

    public virtual DbSet<User> Users { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
        IConfigurationRoot configuration = builder.Build();
        optionsBuilder.UseSqlServer(configuration.GetConnectionString("DefaultString"));
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Admin>(entity =>
        {
            entity.HasKey(e => e.Account).HasName("PK__Admin__B0C3AC47861ED93A");

            entity.ToTable("Admin");

            entity.Property(e => e.Account).HasMaxLength(50);
            entity.Property(e => e.AdminName).HasMaxLength(50);
            entity.Property(e => e.Phone)
                .HasMaxLength(10)
                .IsUnicode(false);

            entity.HasOne(d => d.AccountNavigation).WithOne(p => p.Admin)
                .HasForeignKey<Admin>(d => d.Account)
                .HasConstraintName("FK__Admin__Account__32E0915F");
        });

        modelBuilder.Entity<Class>(entity =>
        {
            entity.HasKey(e => e.ClassId).HasName("PK__Class__CB1927C070DEECA0");

            entity.ToTable("Class");

            entity.Property(e => e.ClassName).HasMaxLength(100);
            entity.Property(e => e.CoWorker).HasMaxLength(50);
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.LecturerAccount).HasMaxLength(50);
            entity.Property(e => e.StartDate).HasColumnType("datetime");

            entity.HasOne(d => d.CoWorkerNavigation).WithMany(p => p.ClassCoWorkerNavigations)
                .HasForeignKey(d => d.CoWorker)
                .HasConstraintName("FK__Class__CoWorker__398D8EEE");

            entity.HasOne(d => d.Course).WithMany(p => p.Classes)
                .HasForeignKey(d => d.CourseId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK__Class__CourseId__37A5467C");

            entity.HasOne(d => d.LecturerAccountNavigation).WithMany(p => p.ClassLecturerAccountNavigations)
                .HasForeignKey(d => d.LecturerAccount)
                .HasConstraintName("FK__Class__LecturerA__38996AB5");
        });

        modelBuilder.Entity<Course>(entity =>
        {
            entity.HasKey(e => e.CourseId).HasName("PK__Course__C92D71A76611D778");

            entity.ToTable("Course");

            entity.Property(e => e.CourseCode)
                .HasMaxLength(10)
                .IsUnicode(false);
            entity.Property(e => e.CourseName).HasMaxLength(100);
        });

        modelBuilder.Entity<Exam>(entity =>
        {
            entity.HasKey(e => e.ExamId).HasName("PK__Exam__297521C7227E4E86");

            entity.ToTable("Exam");

            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.LecturerAccount).HasMaxLength(50);
            entity.Property(e => e.Score).HasColumnType("decimal(5, 2)");
            entity.Property(e => e.StartDate).HasColumnType("datetime");
            entity.Property(e => e.Summary).HasMaxLength(100);
            entity.Property(e => e.Title).HasMaxLength(50);

            entity.HasOne(d => d.Class).WithMany(p => p.Exams)
                .HasForeignKey(d => d.ClassId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK__Exam__ClassId__3C69FB99");
        });

        modelBuilder.Entity<Lecturer>(entity =>
        {
            entity.HasKey(e => e.Account).HasName("PK__Lecturer__B0C3AC4747B9AEC5");

            entity.ToTable("Lecturer");

            entity.Property(e => e.Account).HasMaxLength(50);
            entity.Property(e => e.LecturerName).HasMaxLength(50);
            entity.Property(e => e.Phone)
                .HasMaxLength(10)
                .IsUnicode(false);

            entity.HasOne(d => d.AccountNavigation).WithOne(p => p.Lecturer)
                .HasForeignKey<Lecturer>(d => d.Account)
                .HasConstraintName("FK__Lecturer__Accoun__2D27B809");
        });

        modelBuilder.Entity<Manager>(entity =>
        {
            entity.HasKey(e => e.Account).HasName("PK__Manager__B0C3AC47E3E6F2D6");

            entity.ToTable("Manager");

            entity.Property(e => e.Account).HasMaxLength(50);
            entity.Property(e => e.ManagerName).HasMaxLength(50);
            entity.Property(e => e.Phone)
                .HasMaxLength(10)
                .IsUnicode(false);

            entity.HasOne(d => d.AccountNavigation).WithOne(p => p.Manager)
                .HasForeignKey<Manager>(d => d.Account)
                .HasConstraintName("FK__Manager__Account__300424B4");
        });

        modelBuilder.Entity<Notification>(entity =>
        {
            entity.HasKey(e => e.NotiId).HasName("PK__Notifica__EDC08E92DABFFC72");

            entity.ToTable("Notification");

            entity.Property(e => e.Account).HasMaxLength(50);
            entity.Property(e => e.SendDate).HasColumnType("datetime");

            entity.HasOne(d => d.AccountNavigation).WithMany(p => p.Notifications)
                .HasForeignKey(d => d.Account)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK__Notificat__Accou__5812160E");
        });

        modelBuilder.Entity<QuestionBankAnswer>(entity =>
        {
            entity.HasKey(e => e.AnswerId).HasName("PK__Question__D48250044C1C87EE");

            entity.ToTable("QuestionBankAnswer");

            entity.Property(e => e.Percent).HasColumnType("decimal(5, 2)");

            entity.HasOne(d => d.Ques).WithMany(p => p.QuestionBankAnswers)
                .HasForeignKey(d => d.QuesId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK__QuestionB__QuesI__48CFD27E");
        });

        modelBuilder.Entity<QuestionExam>(entity =>
        {
            entity.HasKey(e => e.QuesId).HasName("PK__Question__5F3F5FF48F818AD9");

            entity.ToTable("QuestionExam");

            entity.Property(e => e.Score)
                .HasDefaultValueSql("((1))")
                .HasColumnType("decimal(5, 2)");

            entity.HasOne(d => d.Exam).WithMany(p => p.QuestionExams)
                .HasForeignKey(d => d.ExamId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK__QuestionE__ExamI__4222D4EF");
        });

        modelBuilder.Entity<QuestionExamAnswer>(entity =>
        {
            entity.HasKey(e => e.AnswerId).HasName("PK__Question__D4825004F411153C");

            entity.ToTable("QuestionExamAnswer");

            entity.Property(e => e.Percent).HasColumnType("decimal(5, 2)");

            entity.HasOne(d => d.Ques).WithMany(p => p.QuestionExamAnswers)
                .HasForeignKey(d => d.QuesId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK__QuestionE__QuesI__45F365D3");
        });

        modelBuilder.Entity<QuestionInBank>(entity =>
        {
            entity.HasKey(e => e.QuesId).HasName("PK__Question__5F3F5FF4735C2007");

            entity.ToTable("QuestionInBank");

            entity.HasOne(d => d.Course).WithMany(p => p.QuestionInBanks)
                .HasForeignKey(d => d.CourseId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK__QuestionI__Cours__3F466844");
        });

        modelBuilder.Entity<Role>(entity =>
        {
            entity.HasKey(e => e.RoleId).HasName("PK__Role__8AFACE1AF55ED8D7");

            entity.ToTable("Role");

            entity.Property(e => e.RoleId).ValueGeneratedNever();
            entity.Property(e => e.Role1)
                .HasMaxLength(20)
                .HasColumnName("Role");
        });

        modelBuilder.Entity<Student>(entity =>
        {
            entity.HasKey(e => e.Account).HasName("PK__Student__B0C3AC475E7A152B");

            entity.ToTable("Student");

            entity.Property(e => e.Account).HasMaxLength(50);
            entity.Property(e => e.Dob)
                .HasColumnType("date")
                .HasColumnName("DOB");
            entity.Property(e => e.Major).HasMaxLength(40);
            entity.Property(e => e.Mssv)
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasColumnName("MSSV");
            entity.Property(e => e.Phone)
                .HasMaxLength(10)
                .IsUnicode(false);
            entity.Property(e => e.StudentName).HasMaxLength(50);

            entity.HasOne(d => d.AccountNavigation).WithOne(p => p.Student)
                .HasForeignKey<Student>(d => d.Account)
                .HasConstraintName("FK__Student__Account__2A4B4B5E");
        });

        modelBuilder.Entity<TakeAnswer>(entity =>
        {
            entity.HasKey(e => e.TakeAnswerId).HasName("PK__TakeAnsw__5116992CADC3B644");

            entity.ToTable("TakeAnswer");

            entity.HasOne(d => d.Answer).WithMany(p => p.TakeAnswers)
                .HasForeignKey(d => d.AnswerId)
                .HasConstraintName("FK__TakeAnswe__Answe__5535A963");

            entity.HasOne(d => d.Ques).WithMany(p => p.TakeAnswers)
                .HasForeignKey(d => d.QuesId)
                .HasConstraintName("FK__TakeAnswe__QuesI__5441852A");

            entity.HasOne(d => d.TakeExam).WithMany(p => p.TakeAnswers)
                .HasForeignKey(d => d.TakeExamId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK__TakeAnswe__TakeE__534D60F1");
        });

        modelBuilder.Entity<TakeClass>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__TakeClas__3213E83F08FD9149");

            entity.ToTable("TakeClass");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.StudentAccount).HasMaxLength(50);

            entity.HasOne(d => d.Class).WithMany(p => p.TakeClasses)
                .HasForeignKey(d => d.ClassId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK__TakeClass__Class__4CA06362");

            entity.HasOne(d => d.StudentAccountNavigation).WithMany(p => p.TakeClasses)
                .HasForeignKey(d => d.StudentAccount)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK__TakeClass__Stude__4BAC3F29");
        });

        modelBuilder.Entity<TakeExam>(entity =>
        {
            entity.HasKey(e => e.TakeExamId).HasName("PK__TakeExam__BCE136CB30AECF48");

            entity.ToTable("TakeExam");

            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.Score).HasColumnType("decimal(5, 2)");
            entity.Property(e => e.StartDate).HasColumnType("datetime");
            entity.Property(e => e.Status).HasMaxLength(20);
            entity.Property(e => e.StudentAccount).HasMaxLength(50);

            entity.HasOne(d => d.Exam).WithMany(p => p.TakeExams)
                .HasForeignKey(d => d.ExamId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK__TakeExam__ExamId__5070F446");

            entity.HasOne(d => d.StudentAccountNavigation).WithMany(p => p.TakeExams)
                .HasForeignKey(d => d.StudentAccount)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK__TakeExam__Studen__4F7CD00D");
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.HasKey(e => e.Account).HasName("PK__User__B0C3AC471D8E1482");

            entity.ToTable("User");

            entity.Property(e => e.Account).HasMaxLength(50);
            entity.Property(e => e.Email)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");
            entity.Property(e => e.Password).HasMaxLength(100);

            entity.HasOne(d => d.Role).WithMany(p => p.Users)
                .HasForeignKey(d => d.RoleId)
                .HasConstraintName("FK__User__RoleId__267ABA7A");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
