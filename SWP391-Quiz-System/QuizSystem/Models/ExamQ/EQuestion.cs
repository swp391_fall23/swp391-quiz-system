﻿namespace QuizSystem.Models.ExamQ {
    public class EQuestion {
        public int QuestionId { get; set; }
        public string? Content { get; set; }
        public bool IsSingleChoice { get; set; }
        public List<EAnswer> Answers { get; set; } = new List<EAnswer>();
    }
}
