﻿namespace QuizSystem.Models.ExamQ {
    public class EAnswer {
        public int AnswerId { get; set; }
        public string? Content { get; set; }
        public bool IsSelected { get; set; } = false;
    }
}
