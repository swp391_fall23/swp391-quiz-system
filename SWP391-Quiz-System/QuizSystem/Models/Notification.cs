﻿using System;
using System.Collections.Generic;

namespace QuizSystem.Models;

public partial class Notification
{
    public string? Account { get; set; }

    public int NotiId { get; set; }

    public string? Title { get; set; }

    public string? Content { get; set; }

    public DateTime? SendDate { get; set; }

    public virtual User? AccountNavigation { get; set; }
}
