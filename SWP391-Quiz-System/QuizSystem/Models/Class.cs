﻿using System;
using System.Collections.Generic;

namespace QuizSystem.Models;

public partial class Class
{
    public int ClassId { get; set; }

    public string? ClassName { get; set; }

    public int? CourseId { get; set; }

    public string? LecturerAccount { get; set; }

    public string? CoWorker { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public virtual Lecturer? CoWorkerNavigation { get; set; }

    public virtual Course? Course { get; set; }

    public virtual ICollection<Exam> Exams { get; set; } = new List<Exam>();

    public virtual Lecturer? LecturerAccountNavigation { get; set; }

    public virtual ICollection<TakeClass> TakeClasses { get; set; } = new List<TakeClass>();
}
