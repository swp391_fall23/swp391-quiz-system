﻿using System;
using System.Collections.Generic;

namespace QuizSystem.Models;

public partial class Student
{
    public string Account { get; set; } = null!;

    public string Mssv { get; set; } = null!;

    public string StudentName { get; set; } = null!;

    public DateTime? Dob { get; set; }

    public string? Phone { get; set; }

    public string? Major { get; set; }

    public virtual User AccountNavigation { get; set; } = null!;

    public virtual ICollection<TakeClass> TakeClasses { get; set; } = new List<TakeClass>();

    public virtual ICollection<TakeExam> TakeExams { get; set; } = new List<TakeExam>();
}
