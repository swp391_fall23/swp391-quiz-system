﻿using System;
using System.Collections.Generic;

namespace QuizSystem.Models;

public partial class Lecturer
{
    public string Account { get; set; } = null!;

    public string LecturerName { get; set; } = null!;

    public string? Phone { get; set; }

    public virtual User AccountNavigation { get; set; } = null!;

    public virtual ICollection<Class> ClassCoWorkerNavigations { get; set; } = new List<Class>();

    public virtual ICollection<Class> ClassLecturerAccountNavigations { get; set; } = new List<Class>();
}
