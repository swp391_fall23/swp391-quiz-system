﻿using System;
using System.Collections.Generic;

namespace QuizSystem.Models;

public partial class Admin
{
    public string Account { get; set; } = null!;

    public string AdminName { get; set; } = null!;

    public string? Phone { get; set; }

    public virtual User AccountNavigation { get; set; } = null!;
}
