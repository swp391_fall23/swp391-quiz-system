﻿using System;
using System.Collections.Generic;

namespace QuizSystem.Models;

public partial class Course
{
    public int CourseId { get; set; }

    public string? CourseCode { get; set; }

    public string? CourseName { get; set; }

    public string? Description { get; set; }

    public virtual ICollection<Class> Classes { get; set; } = new List<Class>();

    public virtual ICollection<QuestionInBank> QuestionInBanks { get; set; } = new List<QuestionInBank>();
}
