﻿namespace QuizSystem.Models.Majors {
    public static class MajorList {
        public static List<string> Majors { get; set; } = new List<string>() {
            "Software Engineer", "Digital Marketing", "Information Assurance", "Multimedia Communiation",
            "Graphic Design", "Hotel Manager"
        };
    }
}
