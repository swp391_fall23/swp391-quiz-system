﻿using System;
using System.Collections.Generic;

namespace QuizSystem.Models;

public partial class QuestionBankAnswer
{
    public int AnswerId { get; set; }

    public int? QuesId { get; set; }

    public string? Content { get; set; }

    public bool? Correct { get; set; }

    public decimal? Percent { get; set; }

    public virtual QuestionInBank? Ques { get; set; }
}
