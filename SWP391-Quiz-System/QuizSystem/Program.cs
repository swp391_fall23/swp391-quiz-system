using Microsoft.EntityFrameworkCore;
using QuizSystem.Models;
using QuizSystem.Services.MailingService;
using Microsoft.AspNetCore.Authentication.Google;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using QuizSystem.Models.Mailing;
using Microsoft.Extensions.DependencyInjection.Extensions;
using QuizSystem.Policies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages(options => {
    options.Conventions.AuthorizeFolder("/UserManagement", "Admin Only");
    //options.Conventions.AuthorizeFolder("/ClassManagement", "Manager Only");
    options.Conventions.AuthorizeFolder("/Courses", "Manager and Lecturer");
    options.Conventions.AuthorizeFolder("/Exams", "Lecturer Only");
    options.Conventions.AuthorizeFolder("/TakingExams", "Student Only");
    options.Conventions.AuthorizePage("/Dashboard/AdminDashboard", "Admin Only");
    options.Conventions.AuthorizePage("/Dashboard/LecturerDashboard", "Lecturer Only");
    options.Conventions.AuthorizePage("/Dashboard/ManagerDashboard", "Manager Only");
    options.Conventions.AuthorizePage("/Dashboard/StudentDashboard", "Student Only");
    options.Conventions.AuthorizePage("/Courses/ManageCourses", "Manager Only");
    options.Conventions.AuthorizePage("/ClassManagement/StudentList", "Manager and Lecturer");
    options.Conventions.AuthorizePage("/ClassManagement/Profile", "Manager and Lecturer");
    options.Conventions.AuthorizePage("/ClassManagement/index", "Manager Only");
    options.Conventions.AuthorizePage("/ClassManagement/Details", "Manager Only");
    options.Conventions.AuthorizePage("/ClassManagement/CourseList", "Manager Only");
    options.Conventions.AuthorizePage("/ClassManagement/Edit", "Manager Only");
    //
    //
    //
    //options.Conventions.AddFolderApplicationModelConvention
});
builder.Services.AddControllersWithViews();
builder.Services.AddSession();

//Database
builder.Services.AddDbContext<OnlineQuizContext>(options => {
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultString"));
}, ServiceLifetime.Transient);

//Mailing service
builder.Services.AddOptions();
var mailSettings = builder.Configuration.GetSection("MailSettings");
builder.Services.Configure<MailSettings>(mailSettings);

builder.Services.AddHttpContextAccessor();
builder.Services.AddTransient<ISendMailService, SendMailService>();

//Role Authorization
builder.Services.AddAuthorization(options => {
    options.AddPolicy("Admin Only", policy => policy.Requirements.Add(new RoleAuthorizeRequirement("Admin")));
    options.AddPolicy("Manager Only", policy => policy.Requirements.Add(new RoleAuthorizeRequirement("Manager")));
    options.AddPolicy("Lecturer Only", policy => policy.Requirements.Add(new RoleAuthorizeRequirement("Lecturer")));
    options.AddPolicy("Student Only", policy => policy.Requirements.Add(new RoleAuthorizeRequirement("Student")));
    options.AddPolicy("Student and Lecturer", policy => policy.Requirements.Add(new RoleAuthorizeRequirement("Lecturer", "Student")));
    options.AddPolicy("Manager and Lecturer", policy => policy.Requirements.Add(new RoleAuthorizeRequirement("Lecturer", "Manager")));
});
builder.Services.AddTransient<IAuthorizationHandler, RoleBasedAuthorizationHandler>();
builder.Services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();

//gg login service
builder.Services.AddAuthentication(options =>
{
    options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = GoogleDefaults.AuthenticationScheme;
})
    .AddCookie()
    .AddGoogle(GoogleDefaults.AuthenticationScheme, options =>
    {
        options.ClientId = builder.Configuration["Authentication:Google:ClientId"];
        options.ClientSecret = builder.Configuration["Authentication:Google:ClientSecret"];
        options.ClaimActions.MapJsonKey("urn:google:picture","picture", "url");
        options.Events.OnRedirectToAuthorizationEndpoint = context =>
        {
            context.Response.Redirect(context.RedirectUri + "&prompt=consent");
            return Task.CompletedTask;
        };
    });

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment()) {
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseAuthentication();
app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseSession();

app.UseRouting();

app.UseAuthorization();

app.MapRazorPages();
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Login}/{action=Index}");
//app.UseEndpoints(endpoints => {
//    endpoints.MapControllerRoute(name: "default", pattern: "{controller}/{action}/{id?}");
//});

app.Run();
